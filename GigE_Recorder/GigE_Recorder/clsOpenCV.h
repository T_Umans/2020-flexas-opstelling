#pragma once
#pragma once

//opencv
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/videoio.hpp> 

//vimba
#include "VimbaCPP/Include/VimbaCPP.h"

//qt
#include <QImage>
#include <QPixmap>
#include <qlabel.h>
#include <qlist.h>

//system
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <ctime>


class clsOpenCV
{
public:
	clsOpenCV();
	~clsOpenCV();

	void StartRecording(int iWidth, int iHeight, int iFPS);
	void StopRecording();
	void Record(cv::Mat oInput);


	bool IsRecording();

	bool IsTimeStampEnabled();
	void EnableTimeStamp(bool xInput);

	QPixmap ConvertMat2Qimage(const cv::Mat& oInput, QLabel* oOutputLabel);
	cv::Mat ConvertFrame(const AVT::VmbAPI::FramePtr pFrame);
	void AddTimeStamp(cv::Mat& oCurrentFrame);

	cv::Mat clsOpenCV::ConstructView(std::vector<cv::Mat> oImages, bool xTitleBar );
private:

	cv::VideoWriter outputVideo;
	bool xRecord = false;

	std::string GetTimeString();
	std::string GetDateString();

	std::string GetBaseDirectory();
	bool fexists(const char* filename);

	bool xEnableTimeStamp;


};

