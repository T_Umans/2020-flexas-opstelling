#include "clsOpenCV.h"


clsOpenCV::clsOpenCV()
{
}


clsOpenCV::~clsOpenCV()
{
}



QPixmap clsOpenCV::ConvertMat2Qimage(const cv::Mat& oInput, QLabel* oOutputLabel)
{
	cv::Mat oTemp;
	// rescale oInput
	double iRescale = 1 ;
	if (oInput.size().width > oOutputLabel->width() || oInput.size().height > oOutputLabel->height()) {
		if (oInput.size().width > oOutputLabel->width()) {
			iRescale = (double(oInput.size().width) / double(oOutputLabel->width()));
		}
		if (oInput.size().height / iRescale > oOutputLabel->height()) {
			iRescale = (double(oInput.size().height) / double(oOutputLabel->height()));
		}
	}


	cv::Mat oTemp2;
	resize(oInput, oTemp2, cv::Size(oInput.size().width / iRescale, oInput.size().height / iRescale ));

	switch (oTemp2.type()) {
	case CV_8UC1:
		oTemp = cv::Mat::zeros(oTemp2.size(), CV_8UC3);
		cv::cvtColor(oTemp2, oTemp, cv::COLOR_GRAY2RGB);
		break;
	case CV_8UC3:
		oTemp = cv::Mat::zeros(oTemp2.size(), CV_8UC3);
		cv::cvtColor(oTemp2, oTemp, cv::COLOR_BGR2RGB);
		break;
	case CV_16UC3:
		oTemp2.convertTo(oTemp2, CV_8UC3);
		cv::cvtColor(oTemp2, oTemp, cv::COLOR_BGR2RGB);
		break;
	}

	QImage oReturn(oTemp.data, oTemp.cols, oTemp.rows, static_cast<int>(oTemp.step), QImage::Format_RGB888);
	return QPixmap::fromImage(oReturn);

}

cv::Mat clsOpenCV::ConvertFrame(const AVT::VmbAPI::FramePtr pFrame)
{

	VmbUint32_t         iVmbHeight, iVmbWidth;
	VmbUchar_t* oBuffer = NULL;
	VmbUint32_t iVmbBufferSize;
	VmbErrorType Result;
	VmbUint64_t iVmbTimeStamp;
	VmbPixelFormatType  oFormat;
	Result = SP_ACCESS(pFrame)->GetPixelFormat(oFormat);
	Result = SP_ACCESS(pFrame)->GetWidth(iVmbWidth);
	Result = SP_ACCESS(pFrame)->GetHeight(iVmbHeight);
	Result = SP_ACCESS(pFrame)->GetBuffer(oBuffer);
	Result = SP_ACCESS(pFrame)->GetBufferSize(iVmbBufferSize);
	Result = SP_ACCESS(pFrame)->GetTimestamp(iVmbTimeStamp);

	int iHeight, iWidth, iTimeStamp;
	iHeight = iVmbHeight;
	iWidth = iVmbWidth;
	iTimeStamp = iVmbTimeStamp;

	cv::Mat oTemp2;
	cv::Mat oMatOutput;
	cv::Mat oMatInput;

	switch (oFormat) {
	case VmbPixelFormatMono8:
		oMatOutput = cv::Mat(iHeight, iWidth, CV_8UC1, (uchar*)oBuffer);
		break;
	case VmbPixelFormatBgr8:
		oMatOutput = cv::Mat(iHeight, iWidth, CV_8UC3, (uchar*)oBuffer);
		break;
	case VmbPixelFormatRgb8:
		oMatOutput = cv::Mat(iHeight, iWidth, CV_8UC3, (uchar*)oBuffer);
		cv::cvtColor(oMatOutput, oMatOutput, cv::COLOR_RGB2BGR);
		break;
	case VmbPixelFormatBayerRG8:
		oMatOutput = cv::Mat(iHeight, iWidth, CV_8UC1, (uchar*)oBuffer);
		cv::cvtColor(oMatOutput, oMatOutput, cv::COLOR_BayerBG2BGR);
		break;
	case VmbPixelFormatBayerRG12:
		oMatInput = cv::Mat(iHeight, iWidth, CV_16UC1, (uchar*)oBuffer);
		oTemp2 = oMatInput.clone();
		oTemp2.convertTo(oTemp2, CV_16UC1, 16);
		oMatOutput = cv::Mat(iHeight, iWidth, CV_16UC3);
		cv::cvtColor(oTemp2, oMatOutput, cv::COLOR_BayerBG2BGR);
		break;
	case VmbPixelFormatBayerGR12Packed:
		oMatInput = cv::Mat(iHeight, iWidth, CV_16UC1, (uchar*)oBuffer);
		oTemp2 = oMatInput.clone();
		oTemp2.convertTo(oTemp2, CV_16UC1, 16);
		oMatOutput = cv::Mat(iHeight, iWidth, CV_16UC3);
		cv::cvtColor(oTemp2, oMatOutput, cv::COLOR_BayerGR2BGR);
		break;
		//case VmbPixelForcv::MatYuv422:
		//oTemp = cv::Mat(iHeight, iWidth, CV_8UC1, (uchar*)oBuffer);
		//	break;
		//case	 VmbPixelForcv::MatYuv411:
		//	break;
		//case	 VmbPixelForcv::MatYuv444:
		//	break;  
	default:
		oMatOutput = cv::Mat(iHeight, iWidth, CV_16UC3);
		oMatOutput.setTo(cv::Scalar(0, 0, 255));
		oMatInput = oMatOutput.clone();
	}
	return oMatOutput;
}

void clsOpenCV::StartRecording(int iWidth, int iHeight, int iFPS)
{
	xRecord = true;
	cv::Size S = cv::Size(iWidth, iHeight);
	//int fourcc = CV_FOURCC('H', '2', '6', '4');
	int fourcc = cv::VideoWriter::fourcc('D', 'I', 'V', 'X');
	//std::string filename = "D:\\Baxter_Video\\Cam_";
	
	std::string sTemp =  GetDateString() + "_" + GetTimeString() + ".avi";
	std::replace(sTemp.begin(), sTemp.end(), ' ', '_');
	std::replace(sTemp.begin(), sTemp.end(), '/', '-');
	std::replace(sTemp.begin(), sTemp.end(), ':', '-');
	std::string filename = "E:\\FlexAs\\Cam_" + sTemp;
	xRecord = outputVideo.open(filename, fourcc, iFPS, S, true);
}

void clsOpenCV::StopRecording()
{
	if (xRecord) {
		outputVideo.release();
	}
	xRecord = false;
}

void clsOpenCV::Record(cv::Mat oInput)
{
	if (outputVideo.isOpened())
	{

		outputVideo.write(oInput);
	}
}




void clsOpenCV::AddTimeStamp(cv::Mat& oCurrentFrame) {
	cv::putText(oCurrentFrame, GetDateString(), cv::Point2f(0,30), cv::FONT_HERSHEY_TRIPLEX, 1, cv::Scalar(255, 255, 255, 255), 2);
	cv::putText(oCurrentFrame, GetTimeString(), cv::Point2f(250,30), cv::FONT_HERSHEY_TRIPLEX, 1, cv::Scalar(255, 255, 255, 255), 2);
	return;
}

std::string clsOpenCV::GetTimeString() {
	//toevoegen timestamp op oInput
	time_t t = time(0);
	struct tm now;
	localtime_s(&now, &t);
	std::string arrsTime[3];

	arrsTime[0] = std::to_string(now.tm_hour);
	arrsTime[1] = std::to_string(now.tm_min);
	arrsTime[2] = std::to_string(now.tm_sec);
	if (arrsTime[1].length() == 1) {
		arrsTime[1] = "0" + arrsTime[1];
	}
	if (arrsTime[2].length() == 1) {
		arrsTime[2] = "0" + arrsTime[2];
	}
	return arrsTime[0] + ":" + arrsTime[1] + ":" + arrsTime[2];
}


std::string clsOpenCV::GetDateString() {
	time_t t = time(0);
	struct tm now;
	localtime_s(&now, &t);

	return  std::to_string(now.tm_mday) + "/" + std::to_string(now.tm_mon) + "/" + std::to_string(now.tm_year + 1900);
}


bool clsOpenCV::IsRecording()
{
	return xRecord;
}

std::string clsOpenCV::GetBaseDirectory()
{
	char working_directory[MAX_PATH + 1];
	GetCurrentDirectoryA(sizeof(working_directory), working_directory);
	return working_directory;
}

bool clsOpenCV::fexists(const char* filename)
{
	std::ifstream ifile(filename);
	return ifile.is_open();
}

void clsOpenCV::EnableTimeStamp(bool xInput)
{
	xEnableTimeStamp = xInput;
}

bool clsOpenCV::IsTimeStampEnabled()
{
	return xEnableTimeStamp;
}


cv::Mat clsOpenCV::ConstructView(std::vector<cv::Mat> oImages, bool xTitleBar = false)
{
	int iRescaleFactor =1; // reduce width and height by a factor of
	int iMaxImagesPerRow = 2; // max images for each row

	int iOffsetBetweenImages = 2;

	int iMaxHeight = 0;
	int iMaxWidth = 0;
	int iCurrentHeight = iOffsetBetweenImages;
	int iCurrentWidth = iOffsetBetweenImages;
	int iRowCounter = 0;
	if (xTitleBar) {
		iCurrentHeight += 40;
	}
	

	std::vector<int> vecHeight;
	std::vector<int> vecWidth;
	std::vector<cv::Mat> vecProcessedImages;

	for (int i = 0; i < oImages.size(); i++) {
		//// afbeelding correct herschalen
		cv::Size s = oImages[i].size();
		s = cv::Size(s.width / iRescaleFactor, s.height / iRescaleFactor);
		vecProcessedImages.push_back(cv::Mat(s, oImages[i].type()));
		resize(oImages[i], vecProcessedImages[i], vecProcessedImages[i].size());

		//// locatie bepalen van afbeeldingen
		// controle op rij
		if (iRowCounter == iMaxImagesPerRow) {
			// er zijn maximaal aantal afbeeldingen in een nieuwe rij. We starten een nieuwe rij
			iCurrentHeight = iMaxHeight;
			iCurrentWidth = iOffsetBetweenImages;
			iRowCounter = 0;
		}
		// toevoegen afbeelding
		vecHeight.push_back(iCurrentHeight);
		vecWidth.push_back(iCurrentWidth);
		iCurrentWidth = iCurrentWidth+ s.width + iOffsetBetweenImages;
		iMaxWidth = max(iMaxWidth, iCurrentWidth);
		iMaxHeight = max(iMaxHeight, iCurrentHeight + s.height + iOffsetBetweenImages);
		iRowCounter += 1;		
	}


	cv::Mat oResult = cv::Mat(cv::Size(iMaxWidth, iMaxHeight), vecProcessedImages[0].type());
	oResult.setTo(cv::Scalar(153,153,153));
	// opvullen van resultaten afbeelding

	

	for (int i = 0; i < vecProcessedImages.size(); i++) {
		int x = vecWidth[i];
		int y = vecHeight[i];
		cv::Size s = vecProcessedImages[i].size();
		cv::Rect roiDestination = cv::Rect(x, y,s.width , s.height);
		cv::Mat dstROI = oResult(roiDestination);
		vecProcessedImages[i].copyTo(dstROI);
		
	}
	
	return oResult;



	//cv::Size s = oImages[0].size();
	//s = cv::Size(s.width / 3, s.height / 3);
	//cv::Mat oDefault;
	//oDefault.create(s, oImages[0].type());
	//oDefault.setTo(cv::Scalar(150,150,150));
	//std::vector<cv::Mat> ResizedMat;
	//
	//int iSize = oImages.size() + (oImages.size() - (oImages.size() / 2) * 2);
	//// alle afbeeldingen verkleinen
	//for (int a = 0; a < iSize; a++)
	//{
	//	if (a < oImages.size())
	//	{
	//		ResizedMat.insert(ResizedMat.end(), cv::Mat(s, oImages[0].depth()));
	//		resize(oImages[a], ResizedMat[a], ResizedMat[a].size());
	//	}
	//	else
	//	{
	//		ResizedMat.insert(ResizedMat.end(), oDefault);
	//	}
	//}
	//cv::Mat oBoven = ResizedMat[0];
	//cv::Mat oOnder = ResizedMat[iSize / 2];
	//for (int a = 1; a < (iSize / 2); a++) {
	//	hconcat(oBoven, ResizedMat[a], oBoven);
	//	hconcat(oOnder, ResizedMat[a + iSize / 2], oOnder);
	//}
	//cv::Mat output;
	//vconcat(oBoven, oOnder, output);
	//return output;
}
