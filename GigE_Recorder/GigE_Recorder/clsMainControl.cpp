#include "clsMainControl.h"

clsMainControl::clsMainControl(QObject *parent)
	: QObject(parent)
{
	connect(_tmrGetImages, &QTimer::timeout, this, &clsMainControl::tmrGetImages_elapsed);
}

clsMainControl::~clsMainControl()
{
	_vimbaSys.Shutdown();
}


bool clsMainControl::VimbaStartup(QString *sError) {
	VmbErrorType err = _vimbaSys.Startup();
	if (VmbErrorSuccess == err)
	{
		return true;
	}
	*sError = VimbaErrorToString(err);
	return false;
}

QString clsMainControl::VimbaErrorToString(VmbErrorType err) {
	switch (err)
	{
	case VmbErrorSuccess:           return  "Success.";
	case VmbErrorInternalFault:     return  "Unexpected fault in VmbApi or driver. (VmbErrorInternalFault)";
	case VmbErrorApiNotStarted:     return  "API not started. (VmbErrorApiNotStarted)";
	case VmbErrorNotFound:          return  "Not found. (VmbErrorNotFound)";
	case VmbErrorBadHandle:         return  "Invalid handle. (VmbErrorBadHandle) ";
	case VmbErrorDeviceNotOpen:     return  "Device not open. (VmbErrorDeviceNotOpen)";
	case VmbErrorInvalidAccess:     return  "Invalid access. (VmbErrorInvalidAccess)";
	case VmbErrorBadParameter:      return  "Bad parameter. (VmbErrorBadParameter)";
	case VmbErrorStructSize:        return  "Wrong DLL version. (VmbErrorStructSize)";
	case VmbErrorMoreData:          return  "More data returned than memory provided. (VmbErrorMoreData)";
	case VmbErrorWrongType:         return  "Wrong type. (VmbErrorWrongType)";
	case VmbErrorInvalidValue:      return  "Invalid value. (VmbErrorInvalidValue)";
	case VmbErrorTimeout:           return  "Timeout.(VmbErrorTimeout)";
	case VmbErrorOther:             return  "TL error. (VmbErrorOther)";
	case VmbErrorResources:         return  "Resource not available. (VmbErrorResources)";
	case VmbErrorInvalidCall:       return  "Invalid call. (VmbErrorInvalidCall)";
	case VmbErrorNoTL:              return  "TL not loaded. (VmbErrorNoTL)";
	case VmbErrorNotImplemented:    return  "Not implemented. (VmbErrorNotImplemented)";
	case VmbErrorNotSupported:      return  "Not supported. (VmbErrorNotSupported)";
	default:                        return  "Unknown";
	}
}



void clsMainControl::OpenAllCameras(QLabel* oOutputLabel) {
	if (!_xCamerasAreOpen) {
		foreach(clsCamera* oCam, _lstCameras) {
			oCam->StartAcquisition();
		}
		_tmrGetImages->start(FRAMEFREQUENCY);
		_xCamerasAreOpen = true;
		_qlblOutput = oOutputLabel;
		//Debug 
		iChronoTimerValue = std::chrono::high_resolution_clock::now();

		_lstCameras[0]->SetRotateFactor(90);
		_lstCameras[1]->SetRotateFactor(-90);
		_lstCameras[2]->SetRotateFactor(180);
	}
}


void clsMainControl::CloseAllCameras() {
	if (_xCamerasAreOpen) {
		foreach(clsCamera * oCam, _lstCameras) {
			oCam->StopAcquisition();
		}
		_tmrGetImages->stop();
		_xCamerasAreOpen = false;
	}
}


void clsMainControl::StartRecording() {
	_xIsRecording = true;
}

void clsMainControl::StopRecording() {
	_xIsRecording = false;
	_oRecordingLock.lock();
	_oOpenCVworker.StopRecording();
	_oRecordingLock.unlock();
	_xShowOutput = true;
}

void clsMainControl::tmrGetImages_elapsed() {
	
	//_tmrGetImages->stop();
	//verzamelen van alle afbeeldingen
	_oLock.lock();
	//iChronoTimerValue = std::chrono::high_resolution_clock::now();
	std::vector<cv::Mat> vAllimages;
	bool xValidImages = true;

	for (int i = 0; i < _lstCameras.count(); i++) {
		vAllimages.insert(vAllimages.end(), _lstCameras[i]->GetLatestFrame());
		xValidImages = xValidImages & !vAllimages.back().empty();
	}
	if (xValidImages) {
		cv::Mat oResult = _oOpenCVworker.ConstructView(vAllimages, true);
		_oOpenCVworker.AddTimeStamp(oResult);		
		
		std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - iChronoTimerValue).count();
		duration =1000/( duration / 1000);
		iChronoTimerValue = t2;
		cv::putText(oResult, std::to_string(duration), cv::Point2f(600,30), cv::FONT_HERSHEY_TRIPLEX, 1, cv::Scalar(255, 255, 255, 255), 2);

		if (_xIsRecording) {
			_oRecordingLock.lock();
			if (_oOpenCVworker.IsRecording()) {
				// recording is al bezig
				_oOpenCVworker.Record(oResult);
				
			}
			else {
				//recording moet nog opgezet worden. Volgende cylcus starten we dan wel met het recorden
				_oOpenCVworker.StartRecording(oResult.size().width, oResult.size().height, RECORD_FPS);
				_xShowOutput = false;
			}
			_oRecordingLock.unlock();
		}
		//debug
		//cv::imshow("debug", oResult);

		//// afbeelding naar form sturen
		if (_xShowOutput) {
			_qlblOutput->setPixmap(_oOpenCVworker.ConvertMat2Qimage(oResult, _qlblOutput));
		}
		


	
		oResult.release();
	}
	_oLock.unlock();
	//_tmrGetImages->start(FRAMEFREQUENCY);
}




#pragma region AuxilaryFunctions

QString clsMainControl::GetOpenCVVersion() {
	try {
		return "OpenCV Version : " + QString::fromStdString(CV_VERSION);
	}
	catch (...) {
		return "Error : No OpenCV Detected";
	}
}

QString clsMainControl::GetVimbaVersion()
{
	try
	{
		VmbVersionInfo_t oVersion;
		_vimbaSys.QueryVersion(oVersion);
		return "Vimba C++ API version : " + QString::number(oVersion.major) + "." + QString::number(oVersion.minor);
	}
	catch (...)
	{
		return ("Error : No Vimba Detected");
	}
}

QList<clsCamera*> clsMainControl::SearchForCameras() {
	// Get all cameras currently connected to Vimba
	CameraPtrVector cameras;
	_vimbaSys.GetCameras(cameras);
	for (int i = 0; i < _lstCameras.count(); i++) {
		_lstCameras[i]->SetVerified(false);
	}

	for (CameraPtrVector::const_iterator iter = cameras.begin(); cameras.end() != iter; ++iter)
	{
		QString sID;
		QString sName;
		std::string sTemp;
		if (VmbErrorSuccess != (*iter)->GetName(sTemp))
		{
			// de naam kan niet opgevraagd 
			sTemp = "[NoName]";
		}
		sName = QString::fromStdString(sTemp);
		// If for any reason we cannot get the ID of a camera we skip it
		if (VmbErrorSuccess == (*iter)->GetID(sTemp))
		{
			// de ID van de camera is gekend
			sID = QString::fromStdString(sTemp);
			if (!CheckIfExists(sID)) {
				clsCamera* o =new clsCamera(sID, sName,this);
				_lstCameras.append(o);
			}
		}
	}
	// verwijder camera's die niet meer gezien werden
	int i = 0;
	while (i < _lstCameras.count()) {
		if (!_lstCameras[i]->IsVerified())
		{
			_lstCameras[i]->~clsCamera();
			_lstCameras.removeAt(i);
		}
		else { i++; }
	}
	return _lstCameras;
}

bool clsMainControl::CheckIfExists(QString sID) {
	for (int i = 0; i < _lstCameras.count(); i++) {
		if (_lstCameras[i]->GetID() == sID) {
			// een camera met hetzelfde ID is gevonden
			_lstCameras[i]->SetVerified(true);
			return true;
		}
	}
	return false;
}

#pragma endregion AuxilaryFunctions
