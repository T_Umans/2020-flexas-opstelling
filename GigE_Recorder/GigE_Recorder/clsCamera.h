#pragma once


//vimba
#include "VimbaCPP/Include/VimbaCPP.h"


//qt
#include <QObject>
#include "QString"
#include "qlist.h"

//proj
#include "clsFrameAux.h"
#include "clsOpenCV.h"

//opencv
#include <opencv2/core/core.hpp>

//system
#include <chrono>


enum { NUM_FRAMES = 3, };

class clsCamera : public QObject
{
	Q_OBJECT
public:
	//constructor
	clsCamera::clsCamera(QString sID, QString sName, QObject* parent);
	~clsCamera();





	//properties
	bool IsVerified() {return _xVerified;};
	void SetVerified(bool x) { _xVerified = x; };

	QString GetID() { return _sID; };
	QString GetName() { return _sName; };

	//
	void StartStopAcquisistion();
	void StartAcquisition();
	void StopAcquisition();

	//
	cv::Mat GetLatestFrame();
	int GetRotateFactor() { return _iRotateFactor; }
	void SetRotateFactor(int iDegrees) { _iRotateFactor = iDegrees; xNewRotationFactor = true; }
private:
	//var
	AVT::VmbAPI::VimbaSystem& _vimbaSys = AVT::VmbAPI::VimbaSystem::GetInstance();
	AVT::VmbAPI::CameraPtr _ptrCamera;
	
	std::mutex _oLock;

	//image properties
	int iWidth;
	int iHeight;
	bool _xIsCamActive = false;
	//image rotation
	int _iRotateFactor = 0;
	bool xNewRotationFactor = false;
	cv::Mat rot_mat;
	cv::Size rot_size;

	// images
	std::vector<cv::Mat> _oReceivedFrames;
	cv::Mat _oBackupMat;

	// main var
	QString _sID;
	QString _sName;
	bool _xVerified = false; // deze variabelen wordt gebruikt om tijdelijk bij te houden als deze camera nog in de lijst staat.
	clsFrameAux* _oFrameAux;
	clsOpenCV _oOpenCVworker = clsOpenCV();
	
	// func
	VmbErrorType  PrepareCamera();
	VmbErrorType SetIntFeatureValueModulo2(const AVT::VmbAPI::CameraPtr& pCamera, const char* const& Name);

private slots:
	
	void CatchNewFrame(int status);


};



