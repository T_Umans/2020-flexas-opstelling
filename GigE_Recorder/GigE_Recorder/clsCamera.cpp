#include "clsCamera.h"

//
//clsCamera::clsCamera(QString sID, QString sName) {
//	_sID = sID;
//	_sName = sName;
//	_xVerified = true;
//	_vimbaSys.GetCameraByID(_sID.toStdString().c_str(), _ptrCamera);
//
//}
//
//
//
//
//clsCamera::~clsCamera() 
//{
//
//}

clsCamera::clsCamera(QString sID, QString sName, QObject* parent)
	: QObject(parent)
{
	_sID = sID;
	_sName = sName;
	_xVerified = true;
	
}

clsCamera::~clsCamera()
{
}





void clsCamera::StartStopAcquisistion() {
	if (_xIsCamActive) {
		StopAcquisition();
	}
	else {
		StartAcquisition();
	}
}

void clsCamera::StartAcquisition() {
	// open camera met juiste rechten
	_vimbaSys.GetCameraByID(_sID.toStdString().c_str(), _ptrCamera);
	VmbErrorType res = _vimbaSys.OpenCameraByID(_sID.toStdString().c_str(), VmbAccessModeFull, _ptrCamera);
	if (VmbErrorSuccess == res)
	{
		// Set the GeV packet size to the highest possible value
		// (In this example we do not test whether this cam actually is a GigE cam)
		AVT::VmbAPI::FeaturePtr pCommandFeature;
		if (VmbErrorSuccess == _ptrCamera->GetFeatureByName("GVSPAdjustPacketSize", pCommandFeature))
		{
			if (VmbErrorSuccess == pCommandFeature->RunCommand())
			{
				bool bIsCommandDone = false;
				do
				{
					if (VmbErrorSuccess != pCommandFeature->IsCommandDone(bIsCommandDone))
					{
						break;
					}
				} while (false == bIsCommandDone);
			}
		}
		if (VmbErrorSuccess == res)
		{
			// set camera so that transform algorithms will never fail
			res = PrepareCamera();
			// get height, width
		

			if (VmbErrorSuccess == res)
			{
				// Create a frame observer for this camera (This will be wrapped in a shared_ptr so we don't delete it)
				_oFrameAux = new clsFrameAux(_ptrCamera);
				// Start streaming
				res = _ptrCamera->StartContinuousImageAcquisition(NUM_FRAMES, IFrameObserverPtr(_oFrameAux));
		
				QObject::connect(_oFrameAux, &clsFrameAux::FrameReceivedSignal, this, &clsCamera::CatchNewFrame);
				_xIsCamActive = true;
			}
		}
		if (VmbErrorSuccess != res)
		{
			// If anything fails after opening the camera we close it
			_ptrCamera->Close();
		}
	}	
}

void clsCamera::StopAcquisition() {
	
	QObject::disconnect(_oFrameAux, &clsFrameAux::FrameReceivedSignal, this, &clsCamera::CatchNewFrame);
	_ptrCamera->StopContinuousImageAcquisition();
	_ptrCamera->Close();
	_xIsCamActive = false;
}




void clsCamera::CatchNewFrame(int status) {
	
	if (true == _xIsCamActive)
	{
		// Pick up frame
		FramePtr pFrame = _oFrameAux->GetFrame();
		if (SP_ISNULL(pFrame))
		{
			return;
		}
		// See if it is not corrupt
		if (VmbFrameStatusComplete == status)
		{		
			cv::Mat oTemp;		
			oTemp = _oOpenCVworker.ConvertFrame(pFrame);		
			if (_iRotateFactor != 0) {

				// Deze code hieronder is volledig juist en laat toe om een afbeelding onder ELKE hoek te verschuiven.
				// voor de huidige toepassing is enkel een 90 verschuiving nodig. En dus de onderstaande code.
				//
				//if (xNewRotationFactor) {
				//	cv::Point2f center((oTemp.cols - 1) / 2.0, (oTemp.rows - 1) / 2.0);
				//	rot_mat = cv::getRotationMatrix2D(center, _iRotateFactor, 1.0);
				//	cv::Rect2f bbox = cv::RotatedRect(cv::Point2f(), oTemp.size(), _iRotateFactor).boundingRect2f();
				//	rot_size = bbox.size();
				//	// adjust transformation matrix
				//	rot_mat.at<double>(0, 2) += bbox.width / 2.0 - oTemp.cols / 2.0;
				//	rot_mat.at<double>(1, 2) += bbox.height / 2.0 - oTemp.rows / 2.0;
				//	xNewRotationFactor = false;
				//}
				//
				//cv::warpAffine(oTemp, oTemp, rot_mat, rot_size);

				switch (_iRotateFactor) {
				case 90:
					cv::rotate(oTemp, oTemp, cv::ROTATE_90_COUNTERCLOCKWISE);
					break;
				case -90:
					cv::rotate(oTemp, oTemp, cv::ROTATE_90_CLOCKWISE);
					break;
				case 180:
					cv::rotate(oTemp, oTemp, cv::ROTATE_180);
					break;
				}
				
			}

			//_oLock.lock();
			// put new frame here
			if (_oReceivedFrames.size() > 2) {
				_oReceivedFrames.back().release();
				_oReceivedFrames.pop_back();
			}
			_oReceivedFrames.push_back(oTemp);
			if (_oBackupMat.empty() && _oBackupMat.size() == oTemp.size()) {
				cv::Size s = oTemp.size();
				_oBackupMat.create(s, oTemp.type());
				_oBackupMat.setTo(cv::Scalar(255,51,51));
			}
			

			//_oLock.unlock();		
		}
		SP_ACCESS(_ptrCamera)->QueueFrame(pFrame);
	}
}




cv::Mat clsCamera::GetLatestFrame() {
	cv::Mat oTemp;
	//_oLock.lock();
	if (_oReceivedFrames.size() >= 1) {
		oTemp = _oReceivedFrames.front();
		_oReceivedFrames.erase(_oReceivedFrames.begin());
	}
	else {
		oTemp = _oBackupMat.clone();
	}

	//_oLock.unlock();
	return oTemp;
}












VmbErrorType clsCamera::PrepareCamera() {
	VmbErrorType result;
	result = SetIntFeatureValueModulo2(_ptrCamera, "Width");
	if (VmbErrorSuccess != result)
	{
		return result;
	}
	result = SetIntFeatureValueModulo2(_ptrCamera, "Height");
	if (VmbErrorSuccess != result)
	{
		return result;
	}
	return result;
}


VmbErrorType clsCamera::SetIntFeatureValueModulo2(const AVT::VmbAPI::CameraPtr& pCamera, const char* const& Name) {
	VmbErrorType					result;
	AVT::VmbAPI::FeaturePtr			feature;
	VmbInt64_t						value_min = 0;
	VmbInt64_t						value_max = 0;
	VmbInt64_t						value_increment = 0;

	result = SP_ACCESS(pCamera)->GetFeatureByName(Name, feature);
	if (VmbErrorSuccess != result)
	{
		return result;
	}

	result = SP_ACCESS(feature)->GetRange(value_min, value_max);
	if (VmbErrorSuccess != result)
	{
		return result;
	}

	result = SP_ACCESS(feature)->GetIncrement(value_increment);
	if (VmbErrorSuccess != result)
	{
		return result;
	}

	value_max = value_max - (value_max % value_increment);
	if (value_max % 2 != 0)
	{
		value_max -= value_increment;
	}

	result = SP_ACCESS(feature)->SetValue(value_max);
	return result;
}
