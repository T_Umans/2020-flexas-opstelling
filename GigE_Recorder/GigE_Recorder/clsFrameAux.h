#include <queue>

#include <QObject>
#include <QMutex>

#include <VimbaCPP/Include/VimbaCPP.h>

using namespace AVT;
using namespace VmbAPI;

class clsFrameAux : public QObject, virtual public IFrameObserver
{
    Q_OBJECT

public:
    // We pass the camera that will deliver the frames to the constructor
    clsFrameAux(CameraPtr pCamera) : IFrameObserver(pCamera) { ; }

    //
    // This is our callback routine that will be executed on every received frame.
    // Triggered by the API.
    //
    // Parameters:
    //  [in]    pFrame          The frame returned from the API
    //
    virtual void FrameReceived(const FramePtr pFrame);

    //
    // After the view has been notified about a new frame it can pick it up.
    // It is then removed from the internal queue
    //
    // Returns:
    //  A shared pointer to the latest frame
    //
    FramePtr GetFrame();

    //
    // Clears the internal (double buffering) frame queue
    //
    void ClearFrameQueue();

private:
    // Since a Qt signal cannot contain a whole frame
    // the frame observer stores all FramePtr
    std::queue<FramePtr> m_Frames;
    QMutex m_FramesMutex;

signals:
    //
    // The frame received event (Qt signal) that notifies about a new incoming frame
    //
    // Parameters:
    //  [out]   status          The frame receive status
    //
    void FrameReceivedSignal(int status);

};



