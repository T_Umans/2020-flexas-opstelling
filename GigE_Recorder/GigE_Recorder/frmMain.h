#pragma once

//qt
#include <QtWidgets/QMainWindow>
#include "ui_frmMain.h"

//opencv
#include <opencv2/opencv.hpp>
//vimba
#include "VimbaCPP/Include/VimbaCPP.h"

//project
#include "clsCamera.h"
#include "clsMainControl.h"



class frmMain : public QMainWindow
{
    Q_OBJECT

public:
    frmMain(QWidget *parent = Q_NULLPTR);
    ~frmMain();


private:
    Ui::frmMainClass ui;

    //func
    void StartupSequence();
    //auxilary functions
    void Log(QString strMsg);
  

    //buttons
    void btnSearchCameras_clicked();
    void btnStartStopAll_clicked();
    void btnStartStopRecording_clicked();

    //vars
    clsMainControl *_oMainLogic = new clsMainControl(this);

protected:
    void resizeEvent(QResizeEvent* event) override;
};
