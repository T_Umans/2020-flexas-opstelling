#include "frmMain.h"


#define NUM_COLORS 3
#define BIT_DEPTH 8


using AVT::VmbAPI::FramePtr;
using AVT::VmbAPI::CameraPtrVector;

frmMain::frmMain(QWidget *parent) : QMainWindow(parent)
{
    ui.setupUi(this);

    //
    StartupSequence();

    // events
    QObject::connect(ui.btnSearchCameras, &QPushButton::clicked, this, &frmMain::btnSearchCameras_clicked);
    QObject::connect(ui.btnStartStopAll, &QPushButton::clicked, this, &frmMain::btnStartStopAll_clicked);
    QObject::connect(ui.btnStartStopRecording, &QPushButton::clicked, this, &frmMain::btnStartStopRecording_clicked);
   
}


frmMain::~frmMain()
{
    // Before we close the application we stop Vimba 
    _oMainLogic->~clsMainControl();
}

void frmMain::resizeEvent(QResizeEvent* event) {
    ui.lblOutput->setFixedHeight(ui.centralWidget->height() - 20);
    ui.lblOutput->setFixedWidth(ui.centralWidget->width() - 370);   
}



void frmMain::StartupSequence() {
    Log(_oMainLogic->GetOpenCVVersion());
    Log(_oMainLogic->GetVimbaVersion());
    QString sError = "";
    if (_oMainLogic->VimbaStartup(&sError)) {
        Log("Vimba service started successful");
    }
    else {
        Log("Vimba service failed to start up");
        Log("Error : " + sError);
    }
  
    Log("Recording folder : E:\\FlexAs\\");
}



void frmMain::btnSearchCameras_clicked() {
    QList<clsCamera*> oListcam =  _oMainLogic->SearchForCameras();
    ui.lbCameras->clear();
    for (int i = 0;i < oListcam.count() ; i++) {
        ui.lbCameras->addItem(oListcam[i]->GetName() + "  [" + oListcam[i]->GetID() + "]");
    }
}

void frmMain::btnStartStopAll_clicked() {
    if (_oMainLogic->IsCamOpened()) {
        //shit doen om alles te stoppen
        _oMainLogic->CloseAllCameras();
        ui.btnStartStopAll->setText("Open ALL cameras detected");
        ui.btnStartStopRecording->setEnabled(false);
    }
    else {
        // open all the cameras
        _oMainLogic->OpenAllCameras(ui.lblOutput);
        ui.btnStartStopAll->setText("Close ALL cameras detected");
        ui.btnStartStopRecording->setEnabled(true);
    }
}

void frmMain::btnStartStopRecording_clicked() {
    if (_oMainLogic->IsRecording()) {
        ui.btnStartStopRecording->setText("Start Recording");
        _oMainLogic->StopRecording();
        Log("Stop Recording");
    }
    else {
        ui.btnStartStopRecording->setText("Stop Recording");
        _oMainLogic->StartRecording();
        Log("Start Recording");
        Log("Disabling live view for performance gain");
    }
}


//
// Prints out a given logging string
//
// Parameters:
//  [in]    strMsg          A given message to be printed out
//
void frmMain::Log(QString strMsg)
{
    ui.m_ListLog->insertItem(0,strMsg);
}
