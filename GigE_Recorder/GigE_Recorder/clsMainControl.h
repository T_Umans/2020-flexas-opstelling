#pragma once

//openCV includes
#include <opencv2/opencv.hpp>

//vimba
#include <VimbaCPP/Include/VimbaCPP.h>

//project
#include "clsCamera.h"
#include "clsOpenCV.h"

//qt
#include "qstring.h"
#include <QObject>
#include <qtimer.h>
#include <QLabel>

//system
#include <iostream>


using AVT::VmbAPI::FramePtr;
using AVT::VmbAPI::CameraPtrVector;



class clsMainControl : public QObject
{
	Q_OBJECT

public:
	clsMainControl(QObject *parent);
	~clsMainControl();

	//mainfunction
	bool VimbaStartup(QString* sError);

	void OpenAllCameras(QLabel* oOutputLabel);
	void CloseAllCameras();
	void StartRecording();
	void StopRecording();

	// aux functions
	QString GetOpenCVVersion();
	QString GetVimbaVersion();
	QList<clsCamera*> SearchForCameras();


	//properties
	QList<clsCamera*> GetCurrentCameraList() { return _lstCameras; };
	bool IsRecording() { return _xIsRecording; }
	bool IsCamOpened() { return _xCamerasAreOpen; }

	//
	void SetOutputDisplay(bool x) { _xShowOutput = x; }
	bool IsOutputDisplay() { return _xShowOutput; }

private:
	// Toegang tot Vimba API
	//AVT::VmbAPI::Examples::ApiController m_ApiController;
	AVT::VmbAPI::VimbaSystem& _vimbaSys = AVT::VmbAPI::VimbaSystem::GetInstance();

	//opslaan van variabelen
	QList<clsCamera*> _lstCameras;

	// cyclische logica
	QTimer* _tmrGetImages = new QTimer(this);
	void tmrGetImages_elapsed();
	std::mutex _oLock;
	std::mutex _oRecordingLock;
	int FRAMEFREQUENCY = 90; // ms between each timer elapse to get frames. This determines the FPS togheter with the processing time
	int RECORD_FPS = 10; // FPS for the output movie

	//fps
	std::chrono::high_resolution_clock::time_point iChronoTimerValue;

	//output opslaan
	QLabel* _qlblOutput;
	bool _xShowOutput = true;

	//opencv interface
	clsOpenCV _oOpenCVworker = clsOpenCV();

	//aux
	bool CheckIfExists(QString sID);
	QString VimbaErrorToString(VmbErrorType err);


	bool _xIsRecording = false;
	bool _xCamerasAreOpen = false;
};
