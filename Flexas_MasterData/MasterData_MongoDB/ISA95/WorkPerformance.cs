﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.ISA95
{
    /// <summary>
    /// Core components of ISA95 work Performance objects.
    /// </summary>
    public abstract class PerformanceCore : ISA95object
    {
        /// <summary>
        /// Describes the category of work.
        /// </summary>
        [BsonElement("WorkType", Order = 3)]
        [BsonIgnoreIfNull]
        public WorkType WorkType { get; set; }
        [BsonElement("PublishedDate", Order = 4)]
        [BsonIgnoreIfNull]
        public DateTime PublishedDate { get; set; }
        /// <summary>
        /// The starting time.
        /// </summary>
        [BsonElement("StartTime", Order = 5)]
        [BsonIgnoreIfNull]
        public DateTime StartTime { get; set; }
        /// <summary>
        /// The ending time.
        /// </summary>
        [BsonElement("EndTime", Order = 6)]
        [BsonIgnoreIfNull]
        public DateTime EndTime { get; set; }
    }

    public class JobResponse : PerformanceCore
    {
        /// <summary>
        /// An identification of the associated job order, if applicable.
        /// </summary>
        [BsonElement("JobOrderId", Order = 5)]
        [BsonIgnoreIfNull]
        public string JobOrderId { get; set; }

        [BsonElement("WorkMasterId", Order = 5)]
        [BsonIgnoreIfNull]
        public string WorkMasterId { get; set; }
        [BsonElement("WorkMasterVersion", Order = 6)]
        [BsonIgnoreIfNull]
        public string WorkMasterVersion { get; set; }
        [BsonElement("WorkDirectiveId", Order = 5)]
        [BsonIgnoreIfNull]
        public string WorkDirectiveId { get; set; }
        [BsonElement("WorkDirectiveVersion", Order = 6)]
        [BsonIgnoreIfNull]
        public string WorkDirectiveVersion { get; set; }
        [BsonElement("WorkflowSpecification", Order = 4)]
        [BsonIgnoreIfNull]
        public string WorkflowSpecificationId { get; set; }
        [BsonElement("NodeId", Order = 4)]
        [BsonIgnoreIfNull]
        public string NodeId { get; set; }
        [BsonElement("WorkInstructionId", Order = 7)]
        [BsonIgnoreIfNull]
        public string WorkInstructionId { get; set; }
        [BsonElement("WorkInstructionVersion", Order = 8)]
        [BsonIgnoreIfNull]
        public string WorkInstructionVersion { get; set; }
        [BsonElement("JobResponseData", Order = 9)]
        [BsonIgnoreIfNull]
        public List<JobResponseData> JobResponseData { get; set; }
        [BsonIgnoreIfNull]
        public List<string> JobResponseId { get; set; }

        [BsonIgnoreIfNull]
        public WorkState JobState { get; set; }

        //FLEXAS DATA
        [BsonIgnoreIfNull]
        public string ResponseType { get; set; }
        //FLEXAS DATA
        [BsonIgnoreIfNull]
        public bool HimSupport { get; set; }
        [BsonIgnoreIfNull]
        public bool PickingSupport { get; set; }
        [BsonIgnoreIfNull]
        public bool CobotSupport { get; set; }

    }



    public class JobResponseData : ISA95property
    {
        [BsonElement("JobResponseData")]
        [BsonIgnoreIfNull]
        public List<JobResponseData> NestedJobResponseData { get; set; }
    }

    public enum WorkState
    {
        Waiting,
        Ready,
        Running,
        Completed,
        Aborted,
        Held,
        Paused,
        Closed
    }

}
