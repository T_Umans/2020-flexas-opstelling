﻿using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.ISA95
{
    /// <summary>
    /// ISA95 work definition.
    /// </summary>
    public abstract class WorkDefinition : ISA95object
    {
        [BsonElement("Version", Order = 2)]
        [BsonIgnoreIfNull]
        public string Version { get; set; }
        [BsonElement("PublishedDate", Order = 3)]
        [BsonIgnoreIfNull]
        public DateTime PublishedDate { get; set; }
        [BsonElement("WorkType", Order = 7)]
        [BsonIgnoreIfNull]
        public WorkType WorkType { get; set; }
        [BsonElement("WorkDefinitionType", Order = 8)]
        [BsonIgnoreIfNull]
        public string WorkDefinitionType { get; set; }
        [BsonElement("Duration", Order = 9)]
        [BsonIgnoreIfNull]
        public Duration Duration { get; set; }
        [BsonElement("Parameter", Order = 10)]
        [BsonIgnoreIfNull]
        public List<Parameter> Parameter { get; set; }
        [BsonElement("PersonnelSpecifcation", Order = 11)]
        [BsonIgnoreIfNull]
        public List<PersonnelSpecifcation> PersonnelSpecifcation { get; set; }
        [BsonElement("EquipmentSpecification", Order = 12)]
        [BsonIgnoreIfNull]
        public List<EquipmentSpecification> EquipmentSpecification { get; set; }
        [BsonElement("MaterialSpecification", Order = 13)]
        [BsonIgnoreIfNull]
        public List<MaterialSpecification> MaterialSpecification { get; set; }
        [BsonElement("WorkflowSpecification", Order = 14)]
        [BsonIgnoreIfNull]
        public List<WorkflowSpecification> WorkflowSpecification { get; set; }

    }

    /// <summary>
    /// ISA95 work master.
    /// </summary>
    public class WorkMaster : WorkDefinition
    {
        public List<string> WorkMasterId
        { 
            get
            {
                if (this.WorkflowSpecification == null) return null;
                if (this.WorkflowSpecification.Count == 0) return null;
                if (this.WorkflowSpecification.First().Node == null) return null;
                return this.WorkflowSpecification.First().Node.Where(n => ! string.IsNullOrEmpty(n.WorkMasterId)).Select(n => n.WorkMasterId).ToList();
            } 
        }

        public WorkDirective GetWorkDirective(string JobOrder)
        {
            var oWorkDirective = new WorkDirective()
            {
                Id = this.Id + "_" + JobOrder,
                WorkMasterId = this.Id,
                Description = this.Description,
                PublishedDate = DateTime.Now,
                HierarchyScope = this.HierarchyScope,
                JobOrderId = JobOrder,
                WorkDefinitionType = this.WorkDefinitionType,
                WorkType = this.WorkType
            };

            if (this.WorkflowSpecification == null) return oWorkDirective;
            if (this.WorkflowSpecification.Count == 0) return oWorkDirective;

            var sWorkflow = Newtonsoft.Json.JsonConvert.SerializeObject(this.WorkflowSpecification.First());
            oWorkDirective.WorkflowSpecification = new List<WorkflowSpecification>() { Newtonsoft.Json.JsonConvert.DeserializeObject<WorkflowSpecification>(sWorkflow) };
            return oWorkDirective;
        }

    }

    /// <summary>
    /// ISA95 work directive.
    /// </summary>
    public class WorkDirective : WorkDefinition
    {
        [BsonElement("WorkMasterId", Order = 4)]
        [BsonRequired]
        public string WorkMasterId { get; set; }
        [BsonElement("WorkMasterVersion", Order = 5)]
        [BsonIgnoreIfNull]
        public string WorkMasterVersion { get; set; }
        [BsonElement("JobOrderId", Order = 6)]
        [BsonRequired]
        public string JobOrderId { get; set; }
        [BsonIgnoreIfNull]
        public List<string> WorkDirectiveId
        {
            get
            {
                if (this.WorkflowSpecification == null) return null;
                if (this.WorkflowSpecification.Count == 0) return null;
                if (this.WorkflowSpecification.First().Node == null) return null;
                if (this.WorkflowSpecification.First().Node.Count == 0) return null;
                return this.WorkflowSpecification.First().Node.Where(n => !string.IsNullOrEmpty(n.WorkDirectiveId)).Select(n => n.WorkDirectiveId).ToList();
            }
        }

        //FLEXAS DATA
        [BsonIgnoreIfNull]
        public bool HimSupport { get; set; }
        [BsonIgnoreIfNull]
        public bool PickingSupport { get; set; }
        [BsonIgnoreIfNull]
        public bool CobotSupport { get; set; }

        public JobResponse GetJobResponse()
        {
            return new JobResponse()
            {
                Id = this.Id,
                JobOrderId = this.JobOrderId,
                WorkDirectiveId = this.Id,
                WorkMasterId = this.WorkMasterId,
                JobState = WorkState.Ready,
                ResponseType = this.WorkDefinitionType,
                HimSupport = this.HimSupport,
                PickingSupport = this.PickingSupport,
                CobotSupport = this.CobotSupport,
                PublishedDate = DateTime.Now
            };
        }

    }

    public class Parameter : ISA95property
    {
        [BsonElement("Parameter")]
        [BsonIgnoreIfNull]
        public List<Parameter> NestedParameter { get; set; }
    }

    public enum WorkType
    {
        Production,
        Maintenance,
        Quality,
        Inventory,
        Mixed,
        Other
    }

}
