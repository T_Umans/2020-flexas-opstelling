﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.ISA95
{
    /// <summary>
    /// ISA95 base object class.
    /// </summary>
    [BsonIgnoreExtraElements]
    public abstract class ISA95object
    {
        //[BsonId]
        //public ObjectId MongoId { get; set; }
        /// <summary>
        /// A unique identification in the scope of the model.
        /// </summary>
        [BsonElement("Id",Order = 1)]
        [BsonRequired]
        public string Id { get; set; }

        /// <summary>
        /// Contains additional information and descriptions (language, text).
        /// </summary>
        [BsonElement("Description", Order = 2)]
        [BsonIgnoreIfNull]
        public Dictionary<string,string> Description { get; set; }

        [BsonElement("HierarchyScope", Order = 2)]
        [BsonIgnoreIfNull]
        public string HierarchyScope { get; set; }

        //public ISA95object()
        //{
        //    MongoId = ObjectId.GenerateNewId();
        //}

        public override string ToString()
        {
            return Id;
        }
    }

    /// <summary>
    /// ISA95 base property class.
    /// </summary>
    public abstract class ISA95property : ISA95object
    {
        [BsonElement(Order = 3)]
        public Value[] Value { get; set; }
    }

    /// <summary>
    /// ISA95 base specification class
    /// </summary>
    public abstract class ISA95specification : ISA95object
    {
        [BsonElement(Order = 3)]
        public Quantity[] Quantity { get; set; }
    }

    public class Value
    {
        [BsonElement("Key")]
        public string Key { get; set; }
        [BsonElement("ValueString")]
        public string ValueString { get; set; }
        [BsonElement("ValueType")]
        public ValueType ValueType { get; set; }
        [BsonElement("UnitOfMeasure")]
        public string UnitOfMeasure { get; set; }
    }

    public class Quantity : Value
    {

    }

    public class Duration : Value
    {

    }

    public enum ValueType
    {
        Text,
        Numerical,
        Reference
    }

    public class HierarchyScope
    {
        [BsonId]
        public ObjectId MongoId { get; set; }

        [BsonElement("EquipmentId", Order = 1)]
        [BsonRequired]
        public string EquipmentId { get; set; }

        [BsonElement("EquipmentLevel", Order = 2)]
        [BsonRequired]
        public string EquipmentLevel { get; set; }

        [BsonElement("HierarchyScope", Order = 3)]
        [BsonRequired]
        public List<HierarchyScope> ChildHierarchyScope { get; set; }

        public HierarchyScope()
        {
            MongoId = ObjectId.GenerateNewId();
        }

        public override string ToString()
        {
            return EquipmentId;
        }
    }
}
