﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.ISA95
{
    /// <summary>
    /// ISA95 workflow specification.
    /// </summary>
    public class WorkflowSpecification : ISA95object
    {
        [BsonElement("Version", Order = 3)]
        [BsonIgnoreIfNull]
        public string Version { get; set; }
        [BsonElement("Node", Order = 4)]
        public List<Node> Node { get; set; }
        [BsonElement("Connection", Order = 5)]
        public List<Connection> Connection { get; set; }
    }

    /// <summary>
    /// ISA95 workflow specification node.
    /// </summary>
    public class Node : ISA95object
    {
        [BsonElement("NodeType", Order = 3)]
        [BsonRequired]
        public string NodeType { get; set; }
        [BsonElement("WorkflowSpecification", Order = 4)]
        [BsonIgnoreIfNull]
        public WorkflowSpecification WorkflowSpecification { get; set; }
        [BsonElement("WorkMasterId", Order = 5)]
        [BsonIgnoreIfNull]
        public string WorkMasterId { get; set; }
        [BsonElement("WorkMasterVersion", Order = 6)]
        [BsonIgnoreIfNull]
        public string WorkMasterVersion { get; set; }
        [BsonElement("WorkDirectiveId", Order = 5)]
        [BsonIgnoreIfNull]
        public string WorkDirectiveId { get; set; }
        [BsonElement("WorkDirectiveVersion", Order = 6)]
        [BsonIgnoreIfNull]
        public string WorkDirectiveVersion { get; set; }
        [BsonElement("WorkInstructionId", Order = 7)]
        [BsonIgnoreIfNull]
        public string WorkInstructionId { get; set; }
        [BsonElement("WorkInstructionVersion", Order = 8)]
        [BsonIgnoreIfNull]
        public string WorkInstructionVersion { get; set; }
        [BsonElement("NodeProperty", Order = 9)]
        [BsonIgnoreIfNull]
        public List<NodeProperty> NodeProperty { get; set; }


        //ADDED FOR FLAXAS
        [BsonIgnoreIfNull]
        public string TextInstruction { get; set; }
        [BsonIgnoreIfNull]
        public string ImageInstruction { get; set; }
        [BsonIgnoreIfNull]
        public List<PickingAction> PickingActions {get; set; }
        [BsonIgnoreIfNull]
        public string CobotCommand { get; set; }

    }

    public class PickingAction
    {
        public string MaterialDefinitionId { get; set; }
        public int Quantity { get; set; }
    }

    /// <summary>
    /// ISA95 workflow specification node property.
    /// </summary>
    public class NodeProperty : ISA95property
    {
        [BsonElement("NodeProperty")]
        [BsonIgnoreIfNull]
        public List<NodeProperty> NestedNodeProperty { get; set; }
    }

    /// <summary>
    /// ISA95 workflow specification Connection.
    /// </summary>
    public class Connection : ISA95object
    {
        [BsonElement("ConnectionType", Order = 3)]
        [BsonRequired]
        public string ConnectionType { get; set; }
        [BsonElement("FromNodeId", Order = 4)]
        [BsonRequired]
        public List<string> FromNodeId { get; set; }
        [BsonElement("ToNodeId", Order = 5)]
        [BsonRequired]
        public List<string> ToNodeId { get; set; }
        [BsonElement("ConnectionProperty", Order = 5)]
        [BsonIgnoreIfNull]
        public List<ConnectionProperty> WorkflowSpecificationConnectionProperty { get; set; }
    }
 
    public class ConnectionProperty : ISA95property
    {
        [BsonElement("WorkflowSpecificationConnectionProperty")]
        [BsonIgnoreIfNull]
        public List<ConnectionProperty> NestedWorkflowSpecificationConnectionProperty { get; set; }
    }



}
