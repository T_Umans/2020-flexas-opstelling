﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.ISA95
{
    /// <summary>
    /// Core components of ISA95 work Performance objects.
    /// </summary>
    public abstract class ScheduleCore : ISA95object
    {
        /// <summary>
        /// Describes the category of work.
        /// </summary>
        [BsonElement("WorkType", Order = 3)]
        [BsonIgnoreIfNull]
        public WorkType WorkType { get; set; }
        [BsonElement("PublishedDate", Order = 4)]
        [BsonIgnoreIfNull]
        public DateTime PublishedDate { get; set; }
        /// <summary>
        /// The starting time.
        /// </summary>
        [BsonElement("StartTime", Order = 5)]
        [BsonIgnoreIfNull]
        public DateTime StartTime { get; set; }
        /// <summary>
        /// The ending time.
        /// </summary>
        [BsonElement("EndTime", Order = 6)]
        [BsonIgnoreIfNull]
        public DateTime EndTime { get; set; }
    }

    public class JobOrder : ScheduleCore
    {
        [BsonElement("WorkMasterId", Order = 5)]
        [BsonIgnoreIfNull]
        public string WorkMasterId { get; set; }
        [BsonElement("WorkMasterVersion", Order = 6)]
        [BsonIgnoreIfNull]
        public string WorkMasterVersion { get; set; }
    }
}
