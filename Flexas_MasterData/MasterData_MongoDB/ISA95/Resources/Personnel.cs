﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.ISA95
{
    /// <summary>
    /// ISA95 Person object.
    /// </summary>
    public class Person : ISA95object
    {
        /// <summary>
        /// List of linked personnel classes.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<string> PersonnelClassId { get; set; }
        /// <summary>
        /// Person properties.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PersonProperty> PersonProperty { get; set; }
        /// <summary>
        /// Person name.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PersonProperty> Name { get; set; }
    }

    /// <summary>
    /// ISA95 Person property object.
    /// </summary>
    public class PersonProperty : ISA95property
    {
        /// <summary>
        /// Nested material definition properties.
        /// </summary>
        [BsonElement("PersonProperty")]
        [BsonIgnoreIfNull]
        public List<PersonProperty> NestedPersonProperty { get; set; }
    }

    /// <summary>
    /// ISA95 Personnel class object.
    /// </summary>
    public class PersonnelClass : ISA95object
    {
        /// <summary>
        /// Personnel class properties.
        /// </summary>
        [BsonIgnoreIfNull]
        public List<PersonnelClassProperty> PersonnelClassProperty { get; set; }
    }

    /// <summary>
    /// ISA95 Person class property object.
    /// </summary>
    public class PersonnelClassProperty : ISA95property
    {
        /// <summary>
        /// Nested Personnel class properties.
        /// </summary>
        [BsonElement("PersonnelClassProperty")]
        [BsonIgnoreIfNull]
        public List<PersonnelClassProperty> NestedPersonClassProperty { get; set; }

    }

    /// <summary>
    /// ISA95 Personnel specification object.
    /// </summary>
    public class PersonnelSpecifcation : ISA95specification
    {
        [BsonElement(Order = 4)]
        [BsonIgnoreIfNull]
        public List<String> PersonId { get; set; }
        [BsonElement(Order = 5)]
        [BsonIgnoreIfNull]
        public List<String> PersonnelClassId { get; set; }
        [BsonElement(Order = 6)]
        [BsonIgnoreIfNull]
        public List<PersonnelSpecificationProperty> PersonnelSpecificationProperty { get; set; }

    }

    public class PersonnelSpecificationProperty : ISA95property
    {
        /// <summary>
        /// Nested personnel specification properties.
        /// </summary>
        [BsonElement("PersonnelSpecificationProperty")]
        [BsonIgnoreIfNull]
        public List<PersonnelSpecificationProperty> NestedPersonnelSpecificationProperty { get; set; }
    }
}
