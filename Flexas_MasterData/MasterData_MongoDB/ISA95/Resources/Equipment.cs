﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.ISA95
{
    /// <summary>
    /// ISA95 Equipment object.
    /// </summary>
    public class Equipment : ISA95object
    {
        /// <summary>
        /// List of linked equipment classes.
        /// </summary>
        [BsonElement("EquipmentClassId")]
        [BsonIgnoreIfNull]
        public List<string> EquipmentClassId { get; set; }
        /// <summary>
        /// Equipment properties.
        /// </summary>
        [BsonElement("EquipmentProperty")]
        [BsonIgnoreIfNull]
        public List<EquipmentProperty> EquipmentProperty { get; set; }
    }

    /// <summary>
    /// ISA95 Equipment property object.
    /// </summary>
    public class EquipmentProperty : ISA95property
    {
        /// <summary>
        /// Nested equipment properties.
        /// </summary>
        [BsonElement("EquipmentProperty")]
        [BsonIgnoreIfNull]
        public List<EquipmentProperty> NestedEquipmentProperty { get; set; }
    }

    /// <summary>
    /// ISA95 Equipment class object.
    /// </summary>
    public class EquipmentClass : ISA95object
    {
        /// <summary>
        /// Equipment class properties.
        /// </summary>
        [BsonElement("EquipmentClassProperty")]
        [BsonIgnoreIfNull]
        public List<EquipmentClassProperty> EquipmentClassProperty { get; set; }
    }

    /// <summary>
    /// ISA95 Equipment class property object.
    /// </summary>
    public class EquipmentClassProperty : ISA95property
    {
        /// <summary>
        /// Nested equipment class properties.
        /// </summary>
        [BsonElement("EquipmentClassProperty")]
        [BsonIgnoreIfNull]
        public List<EquipmentClassProperty> NestedEquipmentClassProperty { get; set; }
    }

    /// <summary>
    /// ISA95 equipment specification object.
    /// </summary>
    public class EquipmentSpecification : ISA95specification
    {
        [BsonElement(Order = 4)]
        [BsonIgnoreIfNull]
        public List<String> EquipmentId { get; set; }
        [BsonElement(Order = 5)]
        [BsonIgnoreIfNull]
        public List<String> EquipmentClassId { get; set; }
        [BsonElement(Order = 6)]
        [BsonIgnoreIfNull]
        public List<EquipmentSpecificationProperty> EquipmentSpecificationProperty { get; set; }
    }

    /// <summary>
    /// ISA95 equipment specification property object
    /// </summary>
    public class EquipmentSpecificationProperty : ISA95property
    {
        /// <summary>
        /// Nested equipment specification properties.
        /// </summary>
        [BsonElement("EquipmentSpecificationProperty")]
        [BsonIgnoreIfNull]
        public List<EquipmentSpecificationProperty> NestedEquipmentSpecificationProperty { get; set; }
    }

}
