﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.ISA95
{
    /// <summary>
    /// ISA95 material object.
    /// </summary>
    public class MaterialDefinition : ISA95object
    {
        /// <summary>
        /// List of linked material classes.
        /// </summary>
        [BsonElement("MaterialClassId")]
        [BsonIgnoreIfNull]
        public List<string> MaterialClassId { get; set; }
        /// <summary>
        /// Material definition properties.
        /// </summary>
        [BsonElement("MaterialProperty")]
        [BsonIgnoreIfNull]
        public List<MaterialDefinitionProperty> MaterialDefinitionProperty { get; set; }

        //ADDED FOR FLEXAS
        public int PickingLocation { get; set; }
    }

    /// <summary>
    /// ISA95 material property object.
    /// </summary>
    public class MaterialDefinitionProperty : ISA95property
    {
        /// <summary>
        /// Nested material definition properties.
        /// </summary>
        [BsonElement("MaterialDefinitionProperty")]
        [BsonIgnoreIfNull]
        public List<MaterialDefinitionProperty> NestedMaterialDefinitionProperty { get; set; }
    }

    /// <summary>
    /// ISA95 material class object.
    /// </summary>
    public class MaterialClass : ISA95object
    {
        /// <summary>
        /// Material class properties.
        /// </summary>
        [BsonElement("MaterialClassProperty")]
        [BsonIgnoreIfNull]
        public List<MaterialClassProperty> MaterialClassProperty { get; set; }
    }

    /// <summary>
    /// ISA95 material class property object.
    /// </summary>
    public class MaterialClassProperty : ISA95property
    {
        /// <summary>
        /// Nested material class properties.
        /// </summary>
        [BsonElement("MaterialClassProperty")]
        [BsonIgnoreIfNull]
        public List<MaterialClassProperty> NestedMaterialClassProperty { get; set; }

    }

    /// <summary>
    /// ISA95 material specification object.
    /// </summary>
    public class MaterialSpecification : ISA95specification
    {
        [BsonElement(Order = 4)]
        [BsonIgnoreIfNull]
        public List<String> MaterialDefinitionId { get; set; }
        [BsonElement(Order = 5)]
        [BsonIgnoreIfNull]
        public List<String> MaterialClassId { get; set; }
        [BsonElement(Order = 6)]
        [BsonIgnoreIfNull]
        public List<MaterialSpecificationProperty> MaterialSpecificationProperty { get; set; }
    }

    /// <summary>
    /// ISA95 material specification property object
    /// </summary>
    public class MaterialSpecificationProperty : ISA95property
    {
        /// <summary>
        /// Nested material specification properties.
        /// </summary>
        [BsonElement("MaterialSpecificationProperty")]
        [BsonIgnoreIfNull]
        public List<MaterialSpecificationProperty> NestedMaterialSpecificationProperty { get; set; }
    }
}
