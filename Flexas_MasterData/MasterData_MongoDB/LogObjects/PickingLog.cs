﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.LogObjects
{
    public class PickingLog
    {
        [BsonId]
        ObjectId id { get; set; }
        [BsonIgnoreIfNull]
        public DateTime Timestamp { get; set; }
        [BsonIgnoreIfNull]
        public int PickingLight { get; set; }
        [BsonIgnoreIfNull]
        public string MaterialDefinitionId { get; set; }
        [BsonIgnoreIfNull]
        public bool LightActivated { get; set; }
    }
}
