﻿using ISyE.AssemblyInformation.ISA95;
using ISyE.AssemblyInformation.MongoDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValueType = ISyE.AssemblyInformation.ISA95.ValueType;

//Test

namespace ISyE.AssemblyInformation
{
    class Program
    {
        static void Main(string[] args)
        {
            MongoDAL oMongoDB = new MongoDAL("mongodb://FlexAs:FlexAs@172.16.220.101:27017", "FlexAs_MasterData");
            oMongoDB.DropMaterialDefinitionCollection();
            using (var reader = new StreamReader(@"MaterialDefinitionList.csv"))
            {
                bool xHeaders = true;
                while (!reader.EndOfStream)
                {
                    if (xHeaders)
                    {
                        reader.ReadLine();
                        xHeaders = false;
                    }
                    else
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(';');
                        var oMD = new MaterialDefinition() { Id = values[0], PickingLocation = int.Parse(values[1]) };
                        oMongoDB.UpdateMaterialDefinition(oMD);
                    }
                }
            }
        }

        static void ExampleWorkMaster(MongoDAL oMongoDB)
        {
            var WM_C40 = new WorkMaster()
            {
                Id = "C40-D11",
                Description = new Dictionary<string, string>() { { "EN", "Air End C40" } },
                PublishedDate = DateTime.Now,
                WorkDefinitionType = "TASK",
                WorkType = WorkType.Production,
                HierarchyScope = "LINE ASSY C40-C55-KK84"
            };

            WM_C40.MaterialSpecification = new List<MaterialSpecification>()
            {
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "3", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Needle bearing 15 mm" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Needle bearing 20 mm" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Sealbus 15 mm" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "O-Ring set 9, 20, 72 & 85 mm" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Lipseal" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "2", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Locknut 15 mm (KM moer)" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Outlet bearing housing" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Male Rotor C40" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Female Rotor C40" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "2", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Angular contact ball bearing 15 mm" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Inlet rotor housing C40" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "2", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Dowel bolt with lipseal M8 * 40" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Outlet cover" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "4", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Bolt M8 * 70" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "PVC protection cover 20 mm" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Data plate" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Write off label" }},
                new MaterialSpecification(){ Quantity = new Quantity[] { new  Quantity() { ValueString = "1", UnitOfMeasure = "pcs."} }, MaterialDefinitionId =  new List<string>(){ "Airend label" }}
            };

            WM_C40.WorkflowSpecification = new List<WorkflowSpecification>()
            {
                new WorkflowSpecification()
                {
                    Id = "C40-D11",
                    Node = new List<Node>()
                    {
                        new Node(){ Id = "Step 1", NodeType = "STEP", WorkMasterId = "C40-D11.Step1" },
                        new Node(){ Id = "Step 2", NodeType = "STEP", WorkMasterId = "C40-D11.Step2" },
                        new Node(){ Id = "Step 3", NodeType = "STEP", WorkMasterId = "C40-D11.Step3" },
                        new Node(){ Id = "Step 4", NodeType = "STEP", WorkMasterId = "C40-D11.Step4" },
                    }
                }
            };

            var WM_C40_1 = new WorkMaster()
            {
                Id = "C40-D11.Step1",
                PublishedDate = DateTime.Now,
                WorkDefinitionType = "STEP",
                WorkType = WorkType.Production,
                HierarchyScope = "VrijWerkvlak"
            };

            WM_C40_1.WorkflowSpecification = new List<WorkflowSpecification>()
            {
                new WorkflowSpecification()
                {
                    Id = "C40-D11.Step1",
                    Node = new List<Node>()
                }
            };

            //1.1
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "10",
                NodeType = "INSTRUCTION",
                TextInstruction = "Bedek de werktafel met vodden indien dit nog niet eerder gebeurde."
            });

            //1.2
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "20",
                NodeType = "INSTRUCTION",
                TextInstruction = "Doe handschoenen aan."
            });

            //1.3
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "30",
                NodeType = "INSTRUCTION",
                TextInstruction = "Neem een O-ring set voor de C40.",
                PickingActions = new List<PickingAction>()
                {
                    new PickingAction(){ MaterialDefinitionId = "O-Ring set 9, 20, 72 & 85 mm", Quantity = 1 }
                }
            });

            //1.4
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "40",
                NodeType = "INSTRUCTION",
                TextInstruction = "Vet elke O-ring in. (Zonder echt vet, doen alsof)"
            });

            //1.5
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "50",
                NodeType = "INSTRUCTION",
                TextInstruction = "Leg de O-ringen in het O-ringwachtbakje en plaats het bakje op de afwerkpost."
            });

            //1.6
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "60",
                NodeType = "INSTRUCTION",
                TextInstruction = "Neem 3 kleine naaldlagers (15 mm) en 1 grote naaldlager (20 mm)",
                PickingActions = new List<PickingAction>()
                {
                    new PickingAction(){ MaterialDefinitionId = "Needle bearing 15 mm", Quantity = 3 },
                    new PickingAction(){ MaterialDefinitionId = "Needle bearing 20 mm", Quantity = 1 }
                }
            });

            //1.7
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "70",
                NodeType = "INSTRUCTION",
                TextInstruction = "Scheid de binnen- en buitenringen"
            });

            //1.8
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "80",
                NodeType = "INSTRUCTION",
                TextInstruction = "Rijg de buitenringen over de respectievelijke ontvettingstoren."
            });

            //1.9
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "90",
                NodeType = "INSTRUCTION",
                TextInstruction = "Ontvet binnendiameter van binnenringen van al de lagers, buitendiameter van buitenringen van al de lagers op de ontvettingstoren met een papier."
            });

            //1.10
            WM_C40_1.WorkflowSpecification.First().Node.Add(new Node()
            {
                Id = "100",
                NodeType = "INSTRUCTION",
                TextInstruction = "Leg de binnenringen in het lagerwachtbakje en breng het bakje naar de afwerkpost."
            });

            oMongoDB.UpdateWorkMaster(WM_C40);
            oMongoDB.UpdateWorkMaster(WM_C40_1);
        }


    }
}
