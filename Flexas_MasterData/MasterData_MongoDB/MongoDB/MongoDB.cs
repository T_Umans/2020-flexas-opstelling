﻿using ISyE.AssemblyInformation.ISA95;
using ISyE.AssemblyInformation.LogObjects;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISyE.AssemblyInformation.MongoDB
{
    /// <summary>
    /// Class to access the ISA-95 objects in the MongoDB database
    /// </summary>
    public class MongoDAL
    {
        private IMongoClient _oClient;
        private IMongoDatabase _oDb;

        public MongoDAL(string ConnectionString, string DataBase)
        {
            _oClient = new MongoClient(ConnectionString);
            _oDb = _oClient.GetDatabase(DataBase);
        }

        #region Work Master
        /// <summary>
        /// ISA95 work master collection.
        /// </summary>
        private IMongoCollection<WorkMaster> WORKMASTER
        {
            get
            {
                return _oDb.GetCollection<WorkMaster>("WorkMaster");
            }
        }

        /// <summary>
        /// Update or add work master in MongoDB.
        /// </summary>
        public void UpdateWorkMaster(WorkMaster workmaster)
        {
            var fb = new FilterDefinitionBuilder<WorkMaster>();
            var filter = fb.Eq(wm => wm.Id, workmaster.Id);
            if (workmaster.Version != null) filter = filter  & fb.Eq(wm => wm.Version, workmaster.Version);
            var options = new ReplaceOptions();
            options.IsUpsert = true;
            WORKMASTER.ReplaceOne(filter, workmaster, options);
        }

        /// <summary>
        /// Delete work master in MongoDB.
        /// </summary>
        public void DeleteWorkMaster(WorkMaster workmaster)
        {
            var fb = new FilterDefinitionBuilder<WorkMaster>();
            var filter = fb.Eq(wm => wm.Id, workmaster.Id);
            if (workmaster.Version != null) filter = filter & fb.Eq(wm => wm.Version, workmaster.Version);
            var options = new ReplaceOptions();
            options.IsUpsert = true;
            WORKMASTER.DeleteOne(filter);
        }


        /// <summary>
        /// Get all work masters from MongoDB.
        /// </summary>
        /// <returns></returns>
        public List<WorkMaster> GetWorkMasters()
        {
            return WORKMASTER.Find("{}").ToList();
        }

        /// <summary>
        /// Get work master from MongoDB by ID and Version
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Version"></param>
        /// <returns></returns>
        public WorkMaster GetWorkMaster(string ID, string Version = null)
        {
            var fb = new FilterDefinitionBuilder<WorkMaster>();
            var filter = fb.Eq(wm => wm.Id, ID);
            if (Version != null) filter = filter & fb.Eq(wm => wm.Version, Version);
            var result = WORKMASTER.Find(filter).ToList();
            return result.Count > 0 ? result.First() : null;
        }
        #endregion

        #region Work Directive
        /// <summary>
        /// ISA95 work master collection.
        /// </summary>
        private IMongoCollection<WorkDirective> WORKDIRECTIVE
        {
            get
            {
                return _oDb.GetCollection<WorkDirective>("WorkDirective");
            }
        }

        public void InsertWorkDirective(WorkDirective workdirective)
        {
            WORKDIRECTIVE.InsertOneAsync(workdirective);
        }

        public void InsertWorkDirective(List<WorkDirective> workdirectives)
        {
            WORKDIRECTIVE.InsertManyAsync(workdirectives);
        }

        /// <summary>
        /// Update or add work master in MongoDB.
        /// </summary>
        public void UpdateWorkDirective(WorkDirective workdirective)
        {
            var fb = new FilterDefinitionBuilder<WorkDirective>();
            var filter = fb.Eq(wm => wm.Id, workdirective.Id);
            if (workdirective.Version != null) filter = filter & fb.Eq(wm => wm.Version, workdirective.Version);
            var options = new ReplaceOptions();
            options.IsUpsert = true;
            WORKDIRECTIVE.ReplaceOneAsync(filter, workdirective, options);
        }

        /// <summary>
        /// Delete work master in MongoDB.
        /// </summary>
        public void DeleteWorkDirective(WorkDirective workdirective)
        {
            var fb = new FilterDefinitionBuilder<WorkDirective>();
            var filter = fb.Eq(wm => wm.Id, workdirective.Id);
            if (workdirective.Version != null) filter = filter & fb.Eq(wm => wm.Version, workdirective.Version);
            var options = new ReplaceOptions();
            options.IsUpsert = true;
            WORKDIRECTIVE.DeleteOneAsync(filter);
        }

        /// <summary>
        /// Get all work masters from MongoDB.
        /// </summary>
        /// <returns></returns>
        public List<WorkDirective> GetWorkDirectives()
        {
            return WORKDIRECTIVE.Find("{}").ToList();
        }

        /// <summary>
        /// Get work master from MongoDB by ID and Version
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Version"></param>
        /// <returns></returns>
        public WorkDirective GetWorkDirective(string ID, string Version = null)
        {
            var fb = new FilterDefinitionBuilder<WorkDirective>();
            var filter = fb.Eq(wm => wm.Id, ID);
            if (Version != null) filter &= fb.Eq(wm => wm.Version, Version);
            var result = WORKDIRECTIVE.Find(filter).ToList();
            return result.Count > 0 ? result.First() : null;
        }
        #endregion

        #region Material definition
        /// <summary>
        /// ISA95 material definition collection.
        /// </summary>
        private IMongoCollection<MaterialDefinition> MATERIAL_DEFINITION
        {
            get
            {
                return _oDb.GetCollection<MaterialDefinition>("MaterialDefinition");
            }
        }

        public void DropMaterialDefinitionCollection()
        {
            _oDb.DropCollection("MaterialDefinition");
        }
        /// <summary>
        /// Update or add material definition in MongoDB.
        /// </summary>
        public void UpdateMaterialDefinition(MaterialDefinition materialdefinition)
        {
            var fb = new FilterDefinitionBuilder<MaterialDefinition>();
            var filter = fb.Eq(md => md.Id, materialdefinition.Id);
            var options = new ReplaceOptions();
            options.IsUpsert = true;
            MATERIAL_DEFINITION.ReplaceOneAsync(filter, materialdefinition, options);
        }

        /// <summary>
        /// Delete material definition in MongoDB.
        /// </summary>
        public void DeleteMaterialDefinition(MaterialDefinition materialdefinition)
        {
            var fb = new FilterDefinitionBuilder<MaterialDefinition>();
            var filter = fb.Eq(md => md.Id, materialdefinition.Id);
            var options = new ReplaceOptions();
            options.IsUpsert = true;
            MATERIAL_DEFINITION.DeleteOneAsync(filter);
        }


        /// <summary>
        /// Get all material definitions from MongoDB.
        /// </summary>
        /// <returns></returns>
        public List<MaterialDefinition> GetMaterialDefinitions()
        {
            return MATERIAL_DEFINITION.Find("{}").ToList();
        }

        /// <summary>
        /// Get work master from MongoDB by ID and Version
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="Version"></param>
        /// <returns></returns>
        public MaterialDefinition GetMaterialDefinition(string ID)
        {
            var fb = new FilterDefinitionBuilder<MaterialDefinition>();
            var filter = fb.Eq(wm => wm.Id, ID);
            var result = MATERIAL_DEFINITION.Find(filter).ToList();
            return result.Count > 0 ? result.First() : null;
        }
        #endregion

        #region Job Response
        /// <summary>
        /// ISA95 job response collection.
        /// </summary>
        private IMongoCollection<JobResponse> JOBRESPONSE
        {
            get
            {
                return _oDb.GetCollection<JobResponse>("JobResponse");
            }
        }

        /// <summary>
        /// Update or add job response in MongoDB.
        /// </summary>
        public void UpdateJobResponse(JobResponse jobresponse)
        {
            var fb = new FilterDefinitionBuilder<JobResponse>();
            var filter = fb.Eq(jr => jr.Id, jobresponse.Id);
            var options = new ReplaceOptions();
            options.IsUpsert = true;
            JOBRESPONSE.ReplaceOneAsync(filter, jobresponse, options);
        }

        /// <summary>
        /// Delete job response in MongoDB.
        /// </summary>
        public void DeleteJobResponse(JobResponse jobresponse)
        {
            var fb = new FilterDefinitionBuilder<JobResponse>();
            var filter = fb.Eq(jr => jr.Id, jobresponse.Id);
            var options = new ReplaceOptions();
            options.IsUpsert = true;
            JOBRESPONSE.DeleteOneAsync(filter);
        }

        public List<JobResponse> GetJobResponses(WorkState WorkState, string JobOrderId = null, string WorkMasterId = null)
        {
            var fb = new FilterDefinitionBuilder<JobResponse>();
            var filter = fb.Eq(jr => jr.JobState, WorkState);
            if (!string.IsNullOrEmpty(JobOrderId)) filter &= fb.Eq(jr => jr.JobOrderId, JobOrderId);
            if (!string.IsNullOrEmpty(WorkMasterId)) filter &= fb.Eq(jr => jr.WorkMasterId, WorkMasterId);
            var result = JOBRESPONSE.Find(filter).ToList();
            return result.Count > 0 ? result : null;
        }
        public List<JobResponse> GetJobResponses(string JobOrderId = null, string WorkMasterId = null)
        {
            var fb = new FilterDefinitionBuilder<JobResponse>();
            var filter = fb.Empty;
            if (!string.IsNullOrEmpty(JobOrderId)) filter &= fb.Eq(jr => jr.JobOrderId, JobOrderId);
            if (!string.IsNullOrEmpty(WorkMasterId)) filter &= fb.Eq(jr => jr.WorkMasterId, WorkMasterId);
            var result = JOBRESPONSE.Find(filter).ToList();
            return result.Count > 0 ? result : null;
        }
        public List<JobResponse> GetTaskJobResponse(WorkDirective workdirective)
        {
            var fb = new FilterDefinitionBuilder<JobResponse>();
            var filter = fb.Eq(jr => jr.WorkDirectiveId, workdirective.Id);
            filter &= fb.Eq(jr => jr.ResponseType, "TASK");
            var result = JOBRESPONSE.Find(filter).ToList();
            return result.Count > 0 ? result : null;
        }
        public List<JobResponse> GetStepJobResponse(WorkDirective workdirective)
        {
            var fb = new FilterDefinitionBuilder<JobResponse>();
            var filter = fb.Eq(jr => jr.WorkDirectiveId, workdirective.Id);
            filter &= fb.Eq(jr => jr.ResponseType, "STEP");
            var result = JOBRESPONSE.Find(filter).ToList();
            return result.Count > 0 ? result : null;
        }
        public List<JobResponse> GetInstructionJobResponses(WorkDirective workdirective, Node node)
        {
            var fb = new FilterDefinitionBuilder<JobResponse>();
            var filter = fb.Eq(jr => jr.WorkDirectiveId, workdirective.Id);
            filter &= fb.Eq(jr => jr.ResponseType, "INSTRUCTION");
            filter &= fb.Eq(jr => jr.NodeId, node.Id);
            var result = JOBRESPONSE.Find(filter).ToList();
            return result.Count > 0 ? result : null;
        }
        public JobResponse GetJobResponse(string JobResponseId)
        {
            var fb = new FilterDefinitionBuilder<JobResponse>();
            var filter = fb.Eq(jr => jr.Id, JobResponseId);
            var result = JOBRESPONSE.Find(filter).ToList();
            return result.Count > 0 ? result.First() : null;
        }

        public void DeleteJobResponse(WorkDirective workdirective)
        {
            var fb = new FilterDefinitionBuilder<JobResponse>();
            var filter = fb.Eq(jr => jr.WorkDirectiveId, workdirective.Id);
            var options = new ReplaceOptions();
            options.IsUpsert = true;
            JOBRESPONSE.DeleteManyAsync(filter);
        }

        public void InsertJobResponse(JobResponse jobresponse)
        {
            JOBRESPONSE.InsertOneAsync(jobresponse);
        }

        public void InsertJobResponse(List<JobResponse> jobresponse)
        {
            JOBRESPONSE.InsertManyAsync(jobresponse);
        }

        #endregion

        #region Picking log
        /// <summary>
        /// Picking log collection.
        /// </summary>
        private IMongoCollection<PickingLog> PICKINGLOG
        {
            get
            {
                return _oDb.GetCollection<PickingLog>("PickingLog");
            }
        }

        public void InsertPickingLog(PickingLog oPickingLog)
        {
            PICKINGLOG.InsertOneAsync(oPickingLog);
        }

        #endregion


    }
}
