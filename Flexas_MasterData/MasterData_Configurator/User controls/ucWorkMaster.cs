﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ISyE.AssemblyInformation.MongoDB;
using ISyE.AssemblyInformation.ISA95;
using System.IO;
using System.Net;

namespace MasterData_Configurator.User_controls
{
    public partial class ucWorkMaster : UserControl
    {
        #region Parameters
        private MongoDAL _oMongoDB;
        private List<WorkMaster> _oWorkMasterList;
        private WorkMaster _oSelectedWorkMaster;
        private Node _oSelectedNode;
        private List<WorkMaster> _oUnassignedWorkMasters;
        private List<MaterialDefinition> _oMaterialDefinitionList;
        private CredentialCache _oCredentialCache;
        private bool _xAddWorkMaster, _xEditWorkMaster, _xDuplicateWorkMaster, _xAddNode, _xEditNode, _xDuplicateNode;
        #endregion

        #region User control methods
        public ucWorkMaster()
        {
            InitializeComponent();
        }
        private void WorkMaster_Load(object sender, EventArgs e)
        {
            //Add items to work definition type combobox
            cbxWorkDefinitionType.Items.Add("TASK");
            cbxWorkDefinitionType.Items.Add("STEP");
            //Add items to hierarchy scope combobox
            cbxHierarchyScope.Items.Add("AssemblyLine");
            cbxHierarchyScope.Items.Add("VrijWerkvlak");
            cbxHierarchyScope.Items.Add("Perstafel");
            cbxHierarchyScope.Items.Add("Afwerkpost");
            //Add items to node type combobox
            cbxNodeType.Items.Add("STEP");
            cbxNodeType.Items.Add("INSTRUCTION");
        }
        #endregion

        #region GUI Updates

        #region Treeview actions
        /// <summary>
        /// Select work master from treeview
        /// </summary>
        private void SelectWorkMaster()
        {
            if (_oSelectedWorkMaster == null) return;
            var oSelectNode = GetAllTreeNodes(tvWorkMasters.Nodes).Where(n => n.ImageKey == _oSelectedWorkMaster.Id).FirstOrDefault();
            if (oSelectNode != null)
            {
                tvWorkMasters.SelectedNode = oSelectNode;
            }
            SelectedWorkMasterControls();
            SelectNode();
        }
        /// <summary>
        /// Select node from treeview.
        /// </summary>
        private void SelectNode()
        {
            if (_oSelectedNode == null) return;
            var oSelectNode = GetAllTreeNodes(tvWorkflow.Nodes).Where(n => n.ImageKey == _oSelectedNode.Id).FirstOrDefault();
            if (oSelectNode != null)
            {
                tvWorkflow.SelectedNode = oSelectNode;
                tvWorkflow.Select();

            }
            SelectedNodeControls();
        }
        /// <summary>
        /// Get a flat list of nodes from a tree node collection.
        /// </summary>
        /// <param name="oTreeNodeCollection">The tree node colledction.</param>
        /// <returns></returns>
        private List<TreeNode> GetAllTreeNodes(TreeNodeCollection oTreeNodeCollection)
        {
            var oList = new List<TreeNode>();
            foreach (TreeNode oN in oTreeNodeCollection)
            {
                oList.Add(oN);
                oList.AddRange(GetAllTreeNodes(oN));
            }
            return oList;
        }
        /// <summary>
        /// Get a flat lists of nodes from a tree node.
        /// </summary>
        /// <param name="oTreeNode">The tree node.</param>
        /// <returns></returns>
        private List<TreeNode> GetAllTreeNodes(TreeNode oTreeNode)
        {
            var oList = new List<TreeNode>();
            foreach (TreeNode oN in oTreeNode.Nodes)
            {
                oList.Add(oN);
                oList.AddRange(GetAllTreeNodes(oN));
            }
            return oList;
        }
        /// <summary>
        /// Update the work master tree view.
        /// </summary>
        private void UpdateWorkMasterTreeView()
        {
            //Load all workmasters
            var oWorkMasterList = _oWorkMasterList.Where(wm => wm.WorkDefinitionType == "TASK");
            //Filter id
            _oUnassignedWorkMasters = _oWorkMasterList.Where(wm => wm.Id.IndexOf(tbxFilter.Text) >= 0).Where(wm => wm.WorkDefinitionType == "STEP").ToList();
            oWorkMasterList = oWorkMasterList.Where(wm => wm.Id.IndexOf(tbxFilter.Text) >= 0).ToList();
            //Add nodes to tree
            tvWorkMasters.Nodes.Clear();
            foreach (var oWM in oWorkMasterList)
            {
                TreeNode oNode = new TreeNode(oWM.Id) { Tag = oWM, ImageKey = oWM.Id };
                AddWorkMasterChildNodes(oWM.WorkMasterId, oNode);
                tvWorkMasters.Nodes.Add(oNode);
            }
            //Add unassigned nodes
            if (_oUnassignedWorkMasters.Count > 0)
            {
                TreeNode oNode = new TreeNode("[UNASSIGNED STEPS]");
                AddWorkMasterChildNodes(_oUnassignedWorkMasters.Select(wm => wm.Id).ToList(), oNode);
                oNode.ForeColor = Color.Red;
                tvWorkMasters.Nodes.Add(oNode);
            }
            //Work master compobox
            cbxNodeWorkMaster.Items.Clear();
            cbxNodeWorkMaster.Items.AddRange(_oWorkMasterList.Where(wm => wm.WorkDefinitionType == "STEP").Select(wm => wm.Id).ToArray());
        }
        /// <summary>
        /// Add child nodes to the work master tree view nodes.
        /// </summary>
        /// <param name="oWorkMasterList">The work master list.</param>
        /// <param name="oNode">The tree node.</param>
        private void AddWorkMasterChildNodes(List<string> oWorkMasterList, TreeNode oNode)
        {
            if (oWorkMasterList == null) return;
            //Add nodes to node
            foreach (var oWM in oWorkMasterList)
            {
                WorkMaster oWorkMaster = _oWorkMasterList.Where(wm => wm.Id == oWM).FirstOrDefault();
                if (oWorkMaster == null) continue;
                _oUnassignedWorkMasters.Remove(oWorkMaster);
                TreeNode oChildNode = new TreeNode(oWM) { Tag = oWorkMaster, ImageKey = oWorkMaster.Id };
                AddWorkMasterChildNodes(oWorkMaster.WorkMasterId, oChildNode);
                oNode.Nodes.Add(oChildNode);
            }
        }
        /// <summary>
        /// Update the workflow tree view.
        /// </summary>
        /// <param name="oWorkMaster">The work master.</param>
        private void UpdateWorkflowTreeView(WorkMaster oWorkMaster)
        {
            ClearNodeFields();
            tvWorkflow.Nodes.Clear();

            if (oWorkMaster.WorkflowSpecification == null) return;
            var oWorkflow = oWorkMaster.WorkflowSpecification.First();
            if (oWorkflow == null) return;
            if (oWorkflow.Node == null) return;

            foreach (var oWorkflowNode in oWorkflow.Node)
            {
                TreeNode oNode = new TreeNode(oWorkflowNode.Id) { Tag = oWorkflowNode, ImageKey = oWorkflowNode.Id };
                tvWorkflow.Nodes.Add(oNode);
            }


        }

        #endregion

        #region Enable controls

        #region Work master
        /// <summary>
        /// Enable default work master controls.
        /// </summary>
        private void DefaultWorkMasterControls()
        {
            //General
            tvWorkMasters.Enabled = true;
            tvWorkflow.Enabled = false;
            tbxFilter.Enabled = true;

            //Work master fields
            tbxID.Enabled = false;
            tbxDescription.Enabled = false;
            cbxWorkDefinitionType.Enabled = false;
            cbxHierarchyScope.Enabled = false;

            //Workflow fields
            tbxNodeId.Enabled = false;
            tbxNodeDescription.Enabled = false;
            cbxNodeType.Enabled = false;
            cbxNodeWorkMaster.Enabled = false;

            //WorkMaster buttons
            btnWorkMasterAdd.Enabled = true;
            btnWorkMasterEdit.Enabled = false;
            btnWorkMasterDuplicate.Enabled = false;
            btnWorkMasterDelete.Enabled = false;
            btnWorkMasterSave.Enabled = false;
            btnWorkMasterCancel.Enabled = false;
            //Workflow buttons
            btnWorkflowAdd.Enabled = false;
            btnWorkflowEdit.Enabled = false;
            btnWorkflowDuplicate.Enabled = false;
            btnWorkflowDelete.Enabled = false;
            btnWorkflowUp.Enabled = false;
            btnWorkflowDown.Enabled = false;
            btnWorkflowSave.Enabled = false;
            btnWorkflowCancel.Enabled = false;

            //Content fields
            gbxContent.Enabled = false;
        }
        /// <summary>
        /// Enable selected work master controls.
        /// </summary>
        private void SelectedWorkMasterControls()
        {
            DefaultWorkMasterControls();
            //General
            tvWorkflow.Enabled = true;
            //WorkMaster buttons
            btnWorkMasterEdit.Enabled = true;
            btnWorkMasterDuplicate.Enabled = true;
            btnWorkMasterDelete.Enabled = true;
            //Workflow buttons
            btnWorkflowAdd.Enabled = true;

            tvWorkMasters.Select();
        }
        /// <summary>
        /// Enable edit work master controls.
        /// </summary>
        private void EditWorkMasterControls()
        {
            DefaultWorkMasterControls();
            //General
            tvWorkMasters.Enabled = false;
            tbxFilter.Enabled = false;
            //Work master fields
            tbxID.Enabled = false;
            tbxDescription.Enabled = true;
            cbxWorkDefinitionType.Enabled = true;
            cbxHierarchyScope.Enabled = true;
            //WorkMaster buttons
            btnWorkMasterAdd.Enabled = false;
            btnWorkMasterEdit.Enabled = false;
            btnWorkMasterDuplicate.Enabled = false;
            btnWorkMasterDelete.Enabled = false;
            btnWorkMasterSave.Enabled = true;
            btnWorkMasterCancel.Enabled = true;
            //Select save button
            btnWorkMasterSave.Select();
        }
        /// <summary>
        /// Enable add work master controls.
        /// </summary>
        private void AddWorkMasterControls()
        {
            ClearWorkMasterFields();
            EditWorkMasterControls();
            tbxID.Enabled = true;
            _oSelectedWorkMaster = new WorkMaster();
        }
        /// <summary>
        /// Enable duplicate work master controls.
        /// </summary>
        private void DuplicateWorkMasterControls()
        {
            EditWorkMasterControls();
            tbxID.Enabled = true;
            tbxID.Text += " (copy)";
        }
        #endregion

        #region Workflow node
        /// <summary>
        /// Enable selected node controls.
        /// </summary>
        private void SelectedNodeControls()
        {
            SelectedWorkMasterControls();
            //Workflow buttons
            btnWorkflowEdit.Enabled = true;
            btnWorkflowDuplicate.Enabled = true;
            btnWorkflowDelete.Enabled = true;
            btnWorkflowUp.Enabled = true;
            btnWorkflowDown.Enabled = true;

            tvWorkflow.Select();
        }
        /// <summary>
        /// Enable edit node controls.
        /// </summary>
        private void EditNodeControls()
        {
            DefaultWorkMasterControls();
            tvWorkMasters.Enabled = false;
            tbxFilter.Enabled = false;
            //Workflow fields
            tbxNodeId.Enabled = true;
            tbxNodeDescription.Enabled = true;
            cbxNodeType.Enabled = true;
            cbxNodeWorkMaster.Enabled = true;
            //Workflow buttons
            btnWorkflowSave.Enabled = true;
            btnWorkflowCancel.Enabled = true;
            //Content fields
            gbxContent.Enabled = true;
        }
        /// <summary>
        /// Enable add node controls.
        /// </summary>
        private void AddNodeControls()
        {
            ClearNodeFields();
            EditNodeControls();
            if (_oSelectedWorkMaster.WorkDefinitionType == "TASK") cbxNodeType.SelectedItem = "STEP";
            if (_oSelectedWorkMaster.WorkDefinitionType == "STEP") cbxNodeType.SelectedItem = "INSTRUCTION";
        }
        /// <summary>
        /// Enable duplicate node controls.
        /// </summary>
        private void DuplicateNodeControls()
        {
            EditNodeControls();
            tbxNodeId.Text += " (copy)";
        }
        #endregion

        /// <summary>
        /// Reset the selected work master and node.
        /// </summary>
        private void ResetSelection()
        {
            _oSelectedNode = null;
            _oSelectedWorkMaster = null;
            _xAddWorkMaster = false;
            _xAddNode = false;
            _xEditWorkMaster = false;
            _xEditNode = false;
            _xDuplicateWorkMaster = false;
            _xDuplicateNode = false;
        }

        #endregion

        #region Update fields

        /// <summary>
        /// Clear the work master fields.
        /// </summary>
        private void ClearWorkMasterFields()
        {
            //ID
            tbxID.Text = "";
            //Description
            tbxDescription.Text = "";
            //Workdefinitiontype
            cbxWorkDefinitionType.SelectedIndex = -1;
            //Hierarchyscope
            cbxHierarchyScope.SelectedIndex = -1;
            //Workflow
            tvWorkflow.Nodes.Clear();
        }
        /// <summary>
        /// Clear the node fields.
        /// </summary>
        private void ClearNodeFields()
        {
            //ID
            tbxNodeId.Text = "";
            //Description
            tbxNodeDescription.Text = "";
            //Type
            cbxNodeType.SelectedIndex = -1;
            //WorkMaster
            cbxNodeWorkMaster.SelectedIndex = -1;
            //Instruction
            tbxTextInstruction.Text = "";
            //Picking
            lvPickingActions.Items.Clear();
            //MaterialDefinition
            cbxMaterialDefinition.Text = "";
            cbxMaterialDefinition.SelectedIndex = -1;
            nudMaterialQuantity.Value = 1;
            //Image
            pbxImageInstruction.Image = null;
            pbxImageInstruction.ImageLocation = null;
            //Cobot commando
            tbxCobotCommando.Text = "";
        }
        /// <summary>
        /// Update work master fields.
        /// </summary>
        /// <param name="oWM">Work master object.</param>
        private void UpdateWorkMasterFields(WorkMaster oWM)
        {
            ClearWorkMasterFields();
            //ID
            tbxID.Text = oWM.Id;
            //Description
            if (oWM.Description != null)
            {
                if (oWM.Description.Count > 0)
                {
                    tbxDescription.Text = oWM.Description.First().Value;
                }
            }
            //Workdefinitiontype
            cbxWorkDefinitionType.SelectedItem = oWM.WorkDefinitionType != null ? oWM.WorkDefinitionType : "";
            //Hierarchyscope
            cbxHierarchyScope.SelectedItem = oWM.HierarchyScope != null ? oWM.HierarchyScope : "";
            //Workflow
            UpdateWorkflowTreeView(oWM);
        }
        /// <summary>
        /// Update node fields.
        /// </summary>
        /// <param name="oNode">Node object.</param>
        private void UpdateNodeFields(Node oNode)
        {
            ClearNodeFields();
            //ID
            tbxNodeId.Text = oNode.Id;
            //Description
            if (oNode.Description != null)
            {
                if (oNode.Description.Count > 0)
                {
                    tbxNodeDescription.Text = oNode.Description.First().Value;
                }
            }
            //Type
            cbxNodeType.SelectedItem = !string.IsNullOrEmpty(oNode.NodeType) ? oNode.NodeType : "";
            //Work master
            cbxNodeWorkMaster.SelectedItem = !string.IsNullOrEmpty(oNode.WorkMasterId) ? oNode.WorkMasterId : "";
            //TextInstruction
            tbxTextInstruction.Text = !string.IsNullOrEmpty(oNode.TextInstruction) ? oNode.TextInstruction : "";
            //Picking
            if (oNode.PickingActions != null)
            {
                foreach (var oPick in oNode.PickingActions)
                {
                    ListViewItem oItem = new ListViewItem(oPick.MaterialDefinitionId);
                    oItem.SubItems.Add(oPick.Quantity.ToString());
                    lvPickingActions.Items.Add(oItem);
                }
            }
            //Image
            txtImagePath.Text = "/";
            if (!string.IsNullOrEmpty(oNode.ImageInstruction))
            {
                try
                {
                    pbxImageInstruction.Image = Image.FromFile(oNode.ImageInstruction);
                    pbxImageInstruction.ImageLocation = oNode.ImageInstruction;
                    txtImagePath.Text = oNode.ImageInstruction;
                }
                catch
                {
                    MessageBox.Show("Image not found... \r\n\r\n Searching for <" + oNode.ImageInstruction +  ">");
                }
            }
            //Cobot commando
            tbxCobotCommando.Text = !string.IsNullOrEmpty(oNode.CobotCommand) ? oNode.CobotCommand : "";
        }

        #endregion

        /// <summary>
        /// Update the material definitions combobox.
        /// </summary>
        private void UpdateMaterialDefinitionsComboBox()
        {
            cbxMaterialDefinition.Items.Clear();
            cbxMaterialDefinition.Items.AddRange(_oMaterialDefinitionList.Select(md => md.Id).ToArray());
        }

        #endregion

        #region MongoDB methods
        /// <summary>
        /// Load data from MongoDB and update GUI
        /// </summary>
        private void LoadData()
        {
            _oWorkMasterList = _oMongoDB.GetWorkMasters();
            _oMaterialDefinitionList = _oMongoDB.GetMaterialDefinitions().OrderBy(md => md.Id).ToList();
            UpdateWorkMasterTreeView();
            UpdateMaterialDefinitionsComboBox();
            DefaultWorkMasterControls();
        }
        /// <summary>
        /// Set the MongoDB DAL
        /// </summary>
        /// <param name="oMongoDB"></param>
        public void SetMongoDAL(MongoDAL oMongoDB)
        {
            _oMongoDB = oMongoDB;
            LoadData();
        }
        /// <summary>
        /// Update the selected work master.
        /// </summary>
        private void UpdateWorkMaster()
        {
            if (_xDuplicateWorkMaster)
            {
                _oSelectedWorkMaster = new WorkMaster() { WorkflowSpecification = _oSelectedWorkMaster.WorkflowSpecification };
            }
            //ID
            if (!string.IsNullOrEmpty(tbxID.Text))
            {
                _oSelectedWorkMaster.Id = tbxID.Text;
                //Check if ID is unique if work master is added
                if (_xAddWorkMaster | _xDuplicateWorkMaster)
                {
                    var iCount = _oWorkMasterList.Count(wm => wm.Id == _oSelectedWorkMaster.Id);
                    if (iCount > 0)
                    {
                        MessageBox.Show("Give an unique work master ID.");
                        return;
                    }
                }
            }
            //Check if ID is valid
            else
            {
                MessageBox.Show("Give a valid work master ID.");
                return;
            }
            //Description
            if (!string.IsNullOrEmpty(tbxDescription.Text))
            {
                _oSelectedWorkMaster.Description = new Dictionary<string, string>() { { "NL", tbxDescription.Text } };
            }
            //Workdefinitiontype
            if (cbxWorkDefinitionType.SelectedIndex > -1)
            {
                _oSelectedWorkMaster.WorkDefinitionType = cbxWorkDefinitionType.SelectedItem.ToString();
            }
            else
            {
                MessageBox.Show("Select a work definition type.");
                return;
            }
            //Hierarchyscope
            if (cbxHierarchyScope.SelectedIndex > -1)
            {
                _oSelectedWorkMaster.HierarchyScope = cbxHierarchyScope.SelectedItem.ToString();
            }
            else
            {
                MessageBox.Show("Select a hierarchy scope.");
                return;
            }
            //Published date
            _oSelectedWorkMaster.PublishedDate = DateTime.Now;

            _oMongoDB.UpdateWorkMaster(_oSelectedWorkMaster);

            LoadData();

            SelectWorkMaster();
            ResetSelection();
        }
        /// <summary>
        /// Update the selected node.
        /// </summary>
        private void UpdateNode()
        {
            var oNode = new Node();
            //ID
            if (!string.IsNullOrEmpty(tbxNodeId.Text))
            {
                oNode.Id = tbxNodeId.Text;
                //Check if ID is unique if work master is added
                if (_xAddNode | _xDuplicateNode)
                {
                    var iCount = _oSelectedWorkMaster.WorkflowSpecification?.First().Node.Count(n => n.Id == oNode.Id);
                    if (iCount > 0)
                    {
                        MessageBox.Show("Give an unique work master ID.");
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Give a valid ID");
                return;
            }
            //Description
            if (string.IsNullOrEmpty(tbxNodeDescription.Text))
            {
                oNode.Description = null;
            }
            else
            {
                if (oNode.Description != null)
                {
                    var sLanguage = oNode.Description.First().Key;
                    oNode.Description[sLanguage] = tbxDescription.Text;
                }
                else
                {
                    oNode.Description = new Dictionary<string, string>() { { "NL", tbxNodeDescription.Text } };
                }
            }
            //NodeType
            if (cbxNodeType.SelectedIndex > -1)
            {
                oNode.NodeType = cbxNodeType.SelectedItem.ToString();
            }
            //WorkMaster
            if (cbxNodeWorkMaster.SelectedIndex > -1) oNode.WorkMasterId = cbxNodeWorkMaster.SelectedItem.ToString();
            //Instruction
            if (!string.IsNullOrEmpty(tbxTextInstruction.Text)) oNode.TextInstruction = tbxTextInstruction.Text;
            //Picking
            if (lvPickingActions.Items.Count == 0) oNode.PickingActions = null;
            else
            {
                oNode.PickingActions = new List<PickingAction>();
                foreach (ListViewItem oItem in lvPickingActions.Items)
                {
                    oNode.PickingActions.Add(new PickingAction() { MaterialDefinitionId = oItem.SubItems[0].Text, Quantity = int.Parse(oItem.SubItems[1].Text) });
                }
            }
            //Image
            if (!string.IsNullOrEmpty(pbxImageInstruction.ImageLocation)) oNode.ImageInstruction = pbxImageInstruction.ImageLocation;
            //Cobot commando
            if (!string.IsNullOrEmpty(tbxCobotCommando.Text)) oNode.CobotCommand = tbxCobotCommando.Text;

            if (_xEditNode)
            {
                //Find index of selected node
                var iIndex = _oSelectedWorkMaster.WorkflowSpecification.First().Node.IndexOf(_oSelectedNode);
                //Replace node
                _oSelectedWorkMaster.WorkflowSpecification.First().Node[iIndex] = oNode;
            }
            if (_xAddNode)
            {
                if (_oSelectedWorkMaster.WorkflowSpecification == null) _oSelectedWorkMaster.WorkflowSpecification = new List<WorkflowSpecification>() { new WorkflowSpecification() { Node = new List<Node>() } };
                if (_oSelectedWorkMaster.WorkflowSpecification.Count == 0) _oSelectedWorkMaster.WorkflowSpecification.Add(new WorkflowSpecification() { Node = new List<Node>() }); ;
                var oNodeList = _oSelectedWorkMaster.WorkflowSpecification.First().Node;
                var iIndex = oNodeList.IndexOf(_oSelectedNode);
                if (iIndex == -1) oNodeList.Add(oNode);
                else oNodeList.Insert(iIndex + 1, oNode);
                _oSelectedNode = oNode;
            }
            if (_xDuplicateNode)
            {
                //Find index of selected node
                var iIndex = _oSelectedWorkMaster.WorkflowSpecification.First().Node.IndexOf(_oSelectedNode);
                //Insert node
                _oSelectedWorkMaster.WorkflowSpecification.First().Node.Insert(iIndex + 1, oNode);
                _oSelectedNode = oNode;
            }

            _oMongoDB.UpdateWorkMaster(_oSelectedWorkMaster);

            LoadData();

            SelectWorkMaster();
            ResetSelection();
        }
        /// <summary>
        /// Delete the selected workmaster
        /// </summary>
        /// <param name="oWM">Work master object.</param>
        private void DeleteWorkMaster(WorkMaster oWM)
        {
            _oMongoDB.DeleteWorkMaster(oWM);
            LoadData();
        }
        #endregion

        /// <summary>
        /// Move a node up or down in a workflow
        /// </summary>
        /// <param name="xDown">Move the node down.</param>
        private void MoveNode(bool xDown)
        {
            if (tvWorkMasters.SelectedNode != null)
            {
                _oSelectedWorkMaster = (WorkMaster)tvWorkMasters.SelectedNode.Tag;
                if (_oSelectedWorkMaster.WorkflowSpecification == null) { ResetSelection(); return; }
                if (_oSelectedWorkMaster.WorkflowSpecification.Count == 0) { ResetSelection(); return; }
                if (_oSelectedWorkMaster.WorkflowSpecification.First().Node.Count <= 1) { ResetSelection(); return; }
                if (tvWorkflow.SelectedNode != null)
                {
                    _oSelectedNode = (Node)tvWorkflow.SelectedNode.Tag;
                    var oNodelist = _oSelectedWorkMaster.WorkflowSpecification.First().Node;
                    var iIndex = oNodelist.IndexOf(_oSelectedNode);

                    if (xDown)
                    {
                        if (iIndex >= oNodelist.Count) { ResetSelection(); return; }
                        oNodelist.RemoveAt(iIndex);
                        oNodelist.Insert(iIndex + 1, _oSelectedNode);
                    }
                    else
                    {
                        if (iIndex <= 0) { ResetSelection(); return; }
                        oNodelist.RemoveAt(iIndex);
                        oNodelist.Insert(iIndex - 1, _oSelectedNode);
                    }
                    UpdateWorkMaster();
                    ResetSelection();
                }
            }
        }

        #region User control events
        private void cbxWorkDefinitionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateWorkMasterTreeView();
        }
        private void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            UpdateWorkMasterTreeView();
        }
        private void btnSaveWM_Click(object sender, EventArgs e)
        {
            UpdateWorkMaster();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            UpdateWorkMasterFields(_oSelectedWorkMaster);
            SelectWorkMaster();
            //Status
            _xAddWorkMaster = false;
            _xEditWorkMaster = false;
            _xDuplicateWorkMaster = false;
            //Selected
            _oSelectedWorkMaster = null;
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (tvWorkMasters.SelectedNode.Tag != null)
            {
                _xEditWorkMaster = true;
                _oSelectedWorkMaster = (WorkMaster) tvWorkMasters.SelectedNode.Tag;
                EditWorkMasterControls();
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            _xAddWorkMaster = true;
            _oSelectedWorkMaster = new WorkMaster();
            AddWorkMasterControls();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (tvWorkMasters.SelectedNode != null)
            {
                if (tvWorkMasters.SelectedNode.Tag != null)
                {
                    var oWM = (WorkMaster) tvWorkMasters.SelectedNode.Tag;
                    var oResult = MessageBox.Show("Are you sure?", "Delete work master " + oWM.Id, MessageBoxButtons.YesNo);
                    if (oResult == DialogResult.Yes) DeleteWorkMaster(oWM);
                }
            }
        }
        private void btnDuplicate_Click(object sender, EventArgs e)
        {
            if (tvWorkMasters.SelectedNode.Tag != null)
            {
                _xDuplicateWorkMaster = true;
                _oSelectedWorkMaster = (WorkMaster)tvWorkMasters.SelectedNode.Tag;
                DuplicateWorkMasterControls();
            }
        }
        private void tbxNodeDescription_TextChanged(object sender, EventArgs e)
        {

        }
        private void tvWorkMasters_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (tvWorkMasters.SelectedNode.Tag != null)
            {
                UpdateWorkMasterFields((WorkMaster)tvWorkMasters.SelectedNode.Tag);
                SelectedWorkMasterControls();
            }
            else
            {
                ClearWorkMasterFields();
                DefaultWorkMasterControls();
            }

        }
        private void tvWorkflow_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (tvWorkflow.SelectedNode.Tag != null)
            {
                UpdateNodeFields((Node)tvWorkflow.SelectedNode.Tag);
                SelectedNodeControls();
            }
            else
            {
                ClearNodeFields();
                SelectedWorkMasterControls();
            }
        }
        private void btnWorkflowEdit_Click(object sender, EventArgs e)
        {

            if (tvWorkMasters.SelectedNode != null)
            {
                _oSelectedWorkMaster = (WorkMaster)tvWorkMasters.SelectedNode.Tag;
                if (tvWorkflow.SelectedNode != null) _oSelectedNode = (Node)tvWorkflow.SelectedNode.Tag;
                _xEditNode = true;
                EditNodeControls();
            }
        }
        private void btnWorkflowAdd_Click(object sender, EventArgs e)
        {
            if (tvWorkMasters.SelectedNode != null)
            {
                _oSelectedWorkMaster = (WorkMaster)tvWorkMasters.SelectedNode.Tag;
                if (tvWorkflow.SelectedNode != null) _oSelectedNode = (Node)tvWorkflow.SelectedNode.Tag;
                _xAddNode = true;
                AddNodeControls();
            }
        }
        private void btnWorkflowDuplicate_Click(object sender, EventArgs e)
        {
            if (tvWorkMasters.SelectedNode != null)
            {
                _oSelectedWorkMaster = (WorkMaster)tvWorkMasters.SelectedNode.Tag;
                if (tvWorkflow.SelectedNode != null) _oSelectedNode = (Node)tvWorkflow.SelectedNode.Tag;
                _xDuplicateNode = true;
                DuplicateNodeControls();
            }
        }
        private void btnWorkflowSave_Click(object sender, EventArgs e)
        {
            UpdateNode();
        }
        private void btnWorkflowUp_Click(object sender, EventArgs e)
        {
            MoveNode(false);
        }
        private void btnWorkflowDown_Click(object sender, EventArgs e)
        {
            MoveNode(true);
        }

  

        private void btnBrowseImage_Click(object sender, EventArgs e)
        {
            var oFileDialog = new OpenFileDialog();
            var sDir = @"\\172.16.30.100\FlexAs\Images";
            //Get dir of loaded image
            if (!string.IsNullOrEmpty(pbxImageInstruction.ImageLocation))
            {
                sDir = Path.GetDirectoryName(pbxImageInstruction.ImageLocation);
            }
            oFileDialog.Title = "Select image";
            oFileDialog.InitialDirectory = sDir;
            oFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            oFileDialog.FilterIndex = 2;
            oFileDialog.RestoreDirectory = true;

            if (oFileDialog.ShowDialog() == DialogResult.OK)
            {
                pbxImageInstruction.Image = Image.FromFile(oFileDialog.FileName);
                pbxImageInstruction.ImageLocation = oFileDialog.FileName;
            }
        }
        private void btnWorkflowCancel_Click(object sender, EventArgs e)
        {
            if (_oSelectedNode != null)
            {
                UpdateNodeFields(_oSelectedNode);
                SelectedNodeControls();
            }
            else
            {
                ClearNodeFields();
                SelectedWorkMasterControls();
            }
            ResetSelection();
        }
        private void btnPickingDelete_Click(object sender, EventArgs e)
        {
            foreach(ListViewItem oItem in lvPickingActions.SelectedItems)
            {
                lvPickingActions.Items.Remove(oItem);
            }
        }
        private void btnPickingAdd_Click(object sender, EventArgs e)
        {
            if (cbxMaterialDefinition.SelectedIndex > -1)
            {
                ListViewItem oItem = new ListViewItem(cbxMaterialDefinition.SelectedItem.ToString());
                oItem.SubItems.Add(nudMaterialQuantity.Value.ToString());
                lvPickingActions.Items.Add(oItem);
            }
        }
        private void btnWorkflowDelete_Click(object sender, EventArgs e)
        {
            if (tvWorkMasters.SelectedNode != null && tvWorkflow.SelectedNode != null)
            {
                if (tvWorkMasters.SelectedNode.Tag != null)
                {
                    _oSelectedWorkMaster = (WorkMaster)tvWorkMasters.SelectedNode.Tag;
                    if (tvWorkflow.SelectedNode.Tag != null)
                    {
                        var oNode = (Node)tvWorkflow.SelectedNode.Tag;
                        var oResult = MessageBox.Show("Are you sure?", "Delete worknode " + oNode.Id, MessageBoxButtons.YesNo);
                        if (oResult == DialogResult.Yes)
                        {
                            _oSelectedWorkMaster.WorkflowSpecification.First().Node.Remove(oNode);
                            UpdateWorkMaster();
                        }
                    }
                }
            }
            ResetSelection();
        }
        #endregion
    }
}
