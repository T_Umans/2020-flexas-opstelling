﻿namespace MasterData_Configurator.User_controls
{
    partial class ucWorkMaster
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnWorkMasterSave = new System.Windows.Forms.Button();
            this.cbxHierarchyScope = new System.Windows.Forms.ComboBox();
            this.cbxWorkDefinitionType = new System.Windows.Forms.ComboBox();
            this.lblWorkMasterHierarchyScope = new System.Windows.Forms.Label();
            this.lblWorkMasterWorkDefinitionType = new System.Windows.Forms.Label();
            this.tbxDescription = new System.Windows.Forms.TextBox();
            this.tbxID = new System.Windows.Forms.TextBox();
            this.lblWorkMasterDescription = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.gbxInfo = new System.Windows.Forms.GroupBox();
            this.btnWorkMasterCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbxWorkflow = new System.Windows.Forms.GroupBox();
            this.gbxContent = new System.Windows.Forms.GroupBox();
            this.tbxCobotCommando = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pbxImageInstruction = new System.Windows.Forms.PictureBox();
            this.btnBrowseImage = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lvPickingActions = new System.Windows.Forms.ListView();
            this.cMaterialDefinition = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cQuantity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nudMaterialQuantity = new System.Windows.Forms.NumericUpDown();
            this.cbxMaterialDefinition = new System.Windows.Forms.ComboBox();
            this.btnPickingDelete = new System.Windows.Forms.Button();
            this.btnPickingAdd = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxTextInstruction = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnWorkflowCancel = new System.Windows.Forms.Button();
            this.gbxNodeInfo = new System.Windows.Forms.GroupBox();
            this.lblWorkMaster = new System.Windows.Forms.Label();
            this.cbxNodeWorkMaster = new System.Windows.Forms.ComboBox();
            this.cbxNodeType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxNodeId = new System.Windows.Forms.TextBox();
            this.tbxNodeDescription = new System.Windows.Forms.TextBox();
            this.btnWorkflowSave = new System.Windows.Forms.Button();
            this.pnlWorkflowNavigation = new System.Windows.Forms.Panel();
            this.tvWorkflow = new System.Windows.Forms.TreeView();
            this.pnlWorkflowControls = new System.Windows.Forms.Panel();
            this.btnWorkflowEdit = new System.Windows.Forms.Button();
            this.btnWorkflowDelete = new System.Windows.Forms.Button();
            this.btnWorkflowDown = new System.Windows.Forms.Button();
            this.btnWorkflowUp = new System.Windows.Forms.Button();
            this.btnWorkflowDuplicate = new System.Windows.Forms.Button();
            this.btnWorkflowAdd = new System.Windows.Forms.Button();
            this.btnWorkMasterAdd = new System.Windows.Forms.Button();
            this.tbxFilter = new System.Windows.Forms.TextBox();
            this.btnWorkMasterEdit = new System.Windows.Forms.Button();
            this.btnWorkMasterDuplicate = new System.Windows.Forms.Button();
            this.tvWorkMasters = new System.Windows.Forms.TreeView();
            this.btnWorkMasterDelete = new System.Windows.Forms.Button();
            this.pnlWorkMasterNavigation = new System.Windows.Forms.Panel();
            this.txtImagePath = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.gbxInfo.SuspendLayout();
            this.gbxWorkflow.SuspendLayout();
            this.gbxContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImageInstruction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaterialQuantity)).BeginInit();
            this.gbxNodeInfo.SuspendLayout();
            this.pnlWorkflowNavigation.SuspendLayout();
            this.pnlWorkflowControls.SuspendLayout();
            this.pnlWorkMasterNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnWorkMasterSave
            // 
            this.btnWorkMasterSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWorkMasterSave.Location = new System.Drawing.Point(820, 93);
            this.btnWorkMasterSave.Name = "btnWorkMasterSave";
            this.btnWorkMasterSave.Size = new System.Drawing.Size(75, 23);
            this.btnWorkMasterSave.TabIndex = 5;
            this.btnWorkMasterSave.Text = "Save";
            this.btnWorkMasterSave.UseVisualStyleBackColor = true;
            this.btnWorkMasterSave.Click += new System.EventHandler(this.btnSaveWM_Click);
            // 
            // cbxHierarchyScope
            // 
            this.cbxHierarchyScope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxHierarchyScope.FormattingEnabled = true;
            this.cbxHierarchyScope.Location = new System.Drawing.Point(113, 93);
            this.cbxHierarchyScope.Name = "cbxHierarchyScope";
            this.cbxHierarchyScope.Size = new System.Drawing.Size(191, 21);
            this.cbxHierarchyScope.TabIndex = 10;
            // 
            // cbxWorkDefinitionType
            // 
            this.cbxWorkDefinitionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxWorkDefinitionType.FormattingEnabled = true;
            this.cbxWorkDefinitionType.Location = new System.Drawing.Point(113, 67);
            this.cbxWorkDefinitionType.Name = "cbxWorkDefinitionType";
            this.cbxWorkDefinitionType.Size = new System.Drawing.Size(191, 21);
            this.cbxWorkDefinitionType.TabIndex = 9;
            // 
            // lblWorkMasterHierarchyScope
            // 
            this.lblWorkMasterHierarchyScope.AutoSize = true;
            this.lblWorkMasterHierarchyScope.Location = new System.Drawing.Point(6, 96);
            this.lblWorkMasterHierarchyScope.Name = "lblWorkMasterHierarchyScope";
            this.lblWorkMasterHierarchyScope.Size = new System.Drawing.Size(84, 13);
            this.lblWorkMasterHierarchyScope.TabIndex = 8;
            this.lblWorkMasterHierarchyScope.Text = "Hierarchy scope";
            // 
            // lblWorkMasterWorkDefinitionType
            // 
            this.lblWorkMasterWorkDefinitionType.AutoSize = true;
            this.lblWorkMasterWorkDefinitionType.Location = new System.Drawing.Point(6, 70);
            this.lblWorkMasterWorkDefinitionType.Name = "lblWorkMasterWorkDefinitionType";
            this.lblWorkMasterWorkDefinitionType.Size = new System.Drawing.Size(101, 13);
            this.lblWorkMasterWorkDefinitionType.TabIndex = 6;
            this.lblWorkMasterWorkDefinitionType.Text = "Work definition type";
            // 
            // tbxDescription
            // 
            this.tbxDescription.Location = new System.Drawing.Point(113, 41);
            this.tbxDescription.Name = "tbxDescription";
            this.tbxDescription.Size = new System.Drawing.Size(577, 20);
            this.tbxDescription.TabIndex = 3;
            // 
            // tbxID
            // 
            this.tbxID.Location = new System.Drawing.Point(113, 17);
            this.tbxID.Name = "tbxID";
            this.tbxID.Size = new System.Drawing.Size(577, 20);
            this.tbxID.TabIndex = 2;
            // 
            // lblWorkMasterDescription
            // 
            this.lblWorkMasterDescription.AutoSize = true;
            this.lblWorkMasterDescription.Location = new System.Drawing.Point(6, 44);
            this.lblWorkMasterDescription.Name = "lblWorkMasterDescription";
            this.lblWorkMasterDescription.Size = new System.Drawing.Size(60, 13);
            this.lblWorkMasterDescription.TabIndex = 1;
            this.lblWorkMasterDescription.Text = "Description";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(6, 20);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(18, 13);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "ID";
            // 
            // gbxInfo
            // 
            this.gbxInfo.Controls.Add(this.btnWorkMasterCancel);
            this.gbxInfo.Controls.Add(this.btnWorkMasterSave);
            this.gbxInfo.Controls.Add(this.groupBox1);
            this.gbxInfo.Controls.Add(this.lblWorkMasterDescription);
            this.gbxInfo.Controls.Add(this.cbxHierarchyScope);
            this.gbxInfo.Controls.Add(this.lblID);
            this.gbxInfo.Controls.Add(this.cbxWorkDefinitionType);
            this.gbxInfo.Controls.Add(this.tbxID);
            this.gbxInfo.Controls.Add(this.lblWorkMasterHierarchyScope);
            this.gbxInfo.Controls.Add(this.tbxDescription);
            this.gbxInfo.Controls.Add(this.lblWorkMasterWorkDefinitionType);
            this.gbxInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbxInfo.Location = new System.Drawing.Point(326, 0);
            this.gbxInfo.Name = "gbxInfo";
            this.gbxInfo.Size = new System.Drawing.Size(985, 123);
            this.gbxInfo.TabIndex = 6;
            this.gbxInfo.TabStop = false;
            this.gbxInfo.Text = "Info";
            // 
            // btnWorkMasterCancel
            // 
            this.btnWorkMasterCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWorkMasterCancel.Location = new System.Drawing.Point(901, 93);
            this.btnWorkMasterCancel.Name = "btnWorkMasterCancel";
            this.btnWorkMasterCancel.Size = new System.Drawing.Size(75, 23);
            this.btnWorkMasterCancel.TabIndex = 11;
            this.btnWorkMasterCancel.Text = "Cancel";
            this.btnWorkMasterCancel.UseVisualStyleBackColor = true;
            this.btnWorkMasterCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(9, 160);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Workflow";
            // 
            // gbxWorkflow
            // 
            this.gbxWorkflow.Controls.Add(this.gbxContent);
            this.gbxWorkflow.Controls.Add(this.btnWorkflowCancel);
            this.gbxWorkflow.Controls.Add(this.gbxNodeInfo);
            this.gbxWorkflow.Controls.Add(this.btnWorkflowSave);
            this.gbxWorkflow.Controls.Add(this.pnlWorkflowNavigation);
            this.gbxWorkflow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxWorkflow.Location = new System.Drawing.Point(326, 123);
            this.gbxWorkflow.Name = "gbxWorkflow";
            this.gbxWorkflow.Size = new System.Drawing.Size(985, 841);
            this.gbxWorkflow.TabIndex = 8;
            this.gbxWorkflow.TabStop = false;
            this.gbxWorkflow.Text = "Workflow";
            // 
            // gbxContent
            // 
            this.gbxContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbxContent.Controls.Add(this.label8);
            this.gbxContent.Controls.Add(this.txtImagePath);
            this.gbxContent.Controls.Add(this.tbxCobotCommando);
            this.gbxContent.Controls.Add(this.label6);
            this.gbxContent.Controls.Add(this.pbxImageInstruction);
            this.gbxContent.Controls.Add(this.btnBrowseImage);
            this.gbxContent.Controls.Add(this.label4);
            this.gbxContent.Controls.Add(this.lvPickingActions);
            this.gbxContent.Controls.Add(this.nudMaterialQuantity);
            this.gbxContent.Controls.Add(this.cbxMaterialDefinition);
            this.gbxContent.Controls.Add(this.btnPickingDelete);
            this.gbxContent.Controls.Add(this.btnPickingAdd);
            this.gbxContent.Controls.Add(this.label7);
            this.gbxContent.Controls.Add(this.tbxTextInstruction);
            this.gbxContent.Controls.Add(this.label5);
            this.gbxContent.Location = new System.Drawing.Point(330, 143);
            this.gbxContent.Name = "gbxContent";
            this.gbxContent.Size = new System.Drawing.Size(652, 667);
            this.gbxContent.TabIndex = 14;
            this.gbxContent.TabStop = false;
            this.gbxContent.Text = "Content";
            // 
            // tbxCobotCommando
            // 
            this.tbxCobotCommando.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxCobotCommando.Location = new System.Drawing.Point(11, 462);
            this.tbxCobotCommando.Multiline = true;
            this.tbxCobotCommando.Name = "tbxCobotCommando";
            this.tbxCobotCommando.Size = new System.Drawing.Size(632, 44);
            this.tbxCobotCommando.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 446);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Cobot command";
            // 
            // pbxImageInstruction
            // 
            this.pbxImageInstruction.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxImageInstruction.Location = new System.Drawing.Point(11, 95);
            this.pbxImageInstruction.Name = "pbxImageInstruction";
            this.pbxImageInstruction.Size = new System.Drawing.Size(632, 323);
            this.pbxImageInstruction.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxImageInstruction.TabIndex = 29;
            this.pbxImageInstruction.TabStop = false;
            // 
            // btnBrowseImage
            // 
            this.btnBrowseImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseImage.Location = new System.Drawing.Point(568, 420);
            this.btnBrowseImage.Name = "btnBrowseImage";
            this.btnBrowseImage.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseImage.TabIndex = 28;
            this.btnBrowseImage.Text = "Browse...";
            this.btnBrowseImage.UseVisualStyleBackColor = true;
            this.btnBrowseImage.Click += new System.EventHandler(this.btnBrowseImage_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Image instruction";
            // 
            // lvPickingActions
            // 
            this.lvPickingActions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvPickingActions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cMaterialDefinition,
            this.cQuantity});
            this.lvPickingActions.FullRowSelect = true;
            this.lvPickingActions.HideSelection = false;
            this.lvPickingActions.Location = new System.Drawing.Point(11, 525);
            this.lvPickingActions.Name = "lvPickingActions";
            this.lvPickingActions.Size = new System.Drawing.Size(632, 109);
            this.lvPickingActions.TabIndex = 26;
            this.lvPickingActions.UseCompatibleStateImageBehavior = false;
            this.lvPickingActions.View = System.Windows.Forms.View.Details;
            // 
            // cMaterialDefinition
            // 
            this.cMaterialDefinition.Text = "Material definition";
            this.cMaterialDefinition.Width = 472;
            // 
            // cQuantity
            // 
            this.cQuantity.Text = "Quantity";
            this.cQuantity.Width = 156;
            // 
            // nudMaterialQuantity
            // 
            this.nudMaterialQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nudMaterialQuantity.Location = new System.Drawing.Point(414, 639);
            this.nudMaterialQuantity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMaterialQuantity.Name = "nudMaterialQuantity";
            this.nudMaterialQuantity.Size = new System.Drawing.Size(68, 20);
            this.nudMaterialQuantity.TabIndex = 25;
            this.nudMaterialQuantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbxMaterialDefinition
            // 
            this.cbxMaterialDefinition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxMaterialDefinition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbxMaterialDefinition.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxMaterialDefinition.FormattingEnabled = true;
            this.cbxMaterialDefinition.Location = new System.Drawing.Point(11, 638);
            this.cbxMaterialDefinition.Name = "cbxMaterialDefinition";
            this.cbxMaterialDefinition.Size = new System.Drawing.Size(397, 21);
            this.cbxMaterialDefinition.TabIndex = 24;
            // 
            // btnPickingDelete
            // 
            this.btnPickingDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPickingDelete.Location = new System.Drawing.Point(568, 637);
            this.btnPickingDelete.Name = "btnPickingDelete";
            this.btnPickingDelete.Size = new System.Drawing.Size(75, 23);
            this.btnPickingDelete.TabIndex = 23;
            this.btnPickingDelete.Text = "Delete";
            this.btnPickingDelete.UseVisualStyleBackColor = true;
            this.btnPickingDelete.Click += new System.EventHandler(this.btnPickingDelete_Click);
            // 
            // btnPickingAdd
            // 
            this.btnPickingAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPickingAdd.Location = new System.Drawing.Point(487, 638);
            this.btnPickingAdd.Name = "btnPickingAdd";
            this.btnPickingAdd.Size = new System.Drawing.Size(75, 23);
            this.btnPickingAdd.TabIndex = 22;
            this.btnPickingAdd.Text = "Add";
            this.btnPickingAdd.UseVisualStyleBackColor = true;
            this.btnPickingAdd.Click += new System.EventHandler(this.btnPickingAdd_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 509);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Picking actions";
            // 
            // tbxTextInstruction
            // 
            this.tbxTextInstruction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxTextInstruction.Location = new System.Drawing.Point(11, 32);
            this.tbxTextInstruction.Multiline = true;
            this.tbxTextInstruction.Name = "tbxTextInstruction";
            this.tbxTextInstruction.Size = new System.Drawing.Size(632, 44);
            this.tbxTextInstruction.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Text instruction";
            // 
            // btnWorkflowCancel
            // 
            this.btnWorkflowCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWorkflowCancel.Location = new System.Drawing.Point(898, 812);
            this.btnWorkflowCancel.Name = "btnWorkflowCancel";
            this.btnWorkflowCancel.Size = new System.Drawing.Size(75, 23);
            this.btnWorkflowCancel.TabIndex = 13;
            this.btnWorkflowCancel.Text = "Cancel";
            this.btnWorkflowCancel.UseVisualStyleBackColor = true;
            this.btnWorkflowCancel.Click += new System.EventHandler(this.btnWorkflowCancel_Click);
            // 
            // gbxNodeInfo
            // 
            this.gbxNodeInfo.Controls.Add(this.lblWorkMaster);
            this.gbxNodeInfo.Controls.Add(this.cbxNodeWorkMaster);
            this.gbxNodeInfo.Controls.Add(this.cbxNodeType);
            this.gbxNodeInfo.Controls.Add(this.label3);
            this.gbxNodeInfo.Controls.Add(this.label1);
            this.gbxNodeInfo.Controls.Add(this.label2);
            this.gbxNodeInfo.Controls.Add(this.tbxNodeId);
            this.gbxNodeInfo.Controls.Add(this.tbxNodeDescription);
            this.gbxNodeInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbxNodeInfo.Location = new System.Drawing.Point(329, 16);
            this.gbxNodeInfo.Name = "gbxNodeInfo";
            this.gbxNodeInfo.Size = new System.Drawing.Size(653, 121);
            this.gbxNodeInfo.TabIndex = 1;
            this.gbxNodeInfo.TabStop = false;
            this.gbxNodeInfo.Text = "Info";
            // 
            // lblWorkMaster
            // 
            this.lblWorkMaster.AutoSize = true;
            this.lblWorkMaster.Location = new System.Drawing.Point(9, 96);
            this.lblWorkMaster.Name = "lblWorkMaster";
            this.lblWorkMaster.Size = new System.Drawing.Size(65, 13);
            this.lblWorkMaster.TabIndex = 15;
            this.lblWorkMaster.Text = "WorkMaster";
            // 
            // cbxNodeWorkMaster
            // 
            this.cbxNodeWorkMaster.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbxNodeWorkMaster.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxNodeWorkMaster.FormattingEnabled = true;
            this.cbxNodeWorkMaster.Location = new System.Drawing.Point(79, 93);
            this.cbxNodeWorkMaster.Name = "cbxNodeWorkMaster";
            this.cbxNodeWorkMaster.Size = new System.Drawing.Size(191, 21);
            this.cbxNodeWorkMaster.TabIndex = 16;
            // 
            // cbxNodeType
            // 
            this.cbxNodeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxNodeType.FormattingEnabled = true;
            this.cbxNodeType.Location = new System.Drawing.Point(79, 66);
            this.cbxNodeType.Name = "cbxNodeType";
            this.cbxNodeType.Size = new System.Drawing.Size(191, 21);
            this.cbxNodeType.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Type";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "ID";
            // 
            // tbxNodeId
            // 
            this.tbxNodeId.Location = new System.Drawing.Point(79, 17);
            this.tbxNodeId.Name = "tbxNodeId";
            this.tbxNodeId.Size = new System.Drawing.Size(404, 20);
            this.tbxNodeId.TabIndex = 6;
            // 
            // tbxNodeDescription
            // 
            this.tbxNodeDescription.Location = new System.Drawing.Point(79, 41);
            this.tbxNodeDescription.Name = "tbxNodeDescription";
            this.tbxNodeDescription.Size = new System.Drawing.Size(404, 20);
            this.tbxNodeDescription.TabIndex = 7;
            this.tbxNodeDescription.TextChanged += new System.EventHandler(this.tbxNodeDescription_TextChanged);
            // 
            // btnWorkflowSave
            // 
            this.btnWorkflowSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWorkflowSave.Location = new System.Drawing.Point(817, 812);
            this.btnWorkflowSave.Name = "btnWorkflowSave";
            this.btnWorkflowSave.Size = new System.Drawing.Size(75, 23);
            this.btnWorkflowSave.TabIndex = 12;
            this.btnWorkflowSave.Text = "Save";
            this.btnWorkflowSave.UseVisualStyleBackColor = true;
            this.btnWorkflowSave.Click += new System.EventHandler(this.btnWorkflowSave_Click);
            // 
            // pnlWorkflowNavigation
            // 
            this.pnlWorkflowNavigation.Controls.Add(this.tvWorkflow);
            this.pnlWorkflowNavigation.Controls.Add(this.pnlWorkflowControls);
            this.pnlWorkflowNavigation.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlWorkflowNavigation.Location = new System.Drawing.Point(3, 16);
            this.pnlWorkflowNavigation.Name = "pnlWorkflowNavigation";
            this.pnlWorkflowNavigation.Size = new System.Drawing.Size(326, 822);
            this.pnlWorkflowNavigation.TabIndex = 0;
            // 
            // tvWorkflow
            // 
            this.tvWorkflow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tvWorkflow.HideSelection = false;
            this.tvWorkflow.Location = new System.Drawing.Point(0, 0);
            this.tvWorkflow.Name = "tvWorkflow";
            this.tvWorkflow.Size = new System.Drawing.Size(326, 765);
            this.tvWorkflow.TabIndex = 2;
            this.tvWorkflow.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvWorkflow_AfterSelect);
            // 
            // pnlWorkflowControls
            // 
            this.pnlWorkflowControls.Controls.Add(this.btnWorkflowEdit);
            this.pnlWorkflowControls.Controls.Add(this.btnWorkflowDelete);
            this.pnlWorkflowControls.Controls.Add(this.btnWorkflowDown);
            this.pnlWorkflowControls.Controls.Add(this.btnWorkflowUp);
            this.pnlWorkflowControls.Controls.Add(this.btnWorkflowDuplicate);
            this.pnlWorkflowControls.Controls.Add(this.btnWorkflowAdd);
            this.pnlWorkflowControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlWorkflowControls.Location = new System.Drawing.Point(0, 768);
            this.pnlWorkflowControls.Name = "pnlWorkflowControls";
            this.pnlWorkflowControls.Size = new System.Drawing.Size(326, 54);
            this.pnlWorkflowControls.TabIndex = 0;
            // 
            // btnWorkflowEdit
            // 
            this.btnWorkflowEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWorkflowEdit.Location = new System.Drawing.Point(84, 31);
            this.btnWorkflowEdit.Name = "btnWorkflowEdit";
            this.btnWorkflowEdit.Size = new System.Drawing.Size(75, 23);
            this.btnWorkflowEdit.TabIndex = 9;
            this.btnWorkflowEdit.Text = "Edit";
            this.btnWorkflowEdit.UseVisualStyleBackColor = true;
            this.btnWorkflowEdit.Click += new System.EventHandler(this.btnWorkflowEdit_Click);
            // 
            // btnWorkflowDelete
            // 
            this.btnWorkflowDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWorkflowDelete.Location = new System.Drawing.Point(246, 31);
            this.btnWorkflowDelete.Name = "btnWorkflowDelete";
            this.btnWorkflowDelete.Size = new System.Drawing.Size(75, 23);
            this.btnWorkflowDelete.TabIndex = 8;
            this.btnWorkflowDelete.Text = "Delete";
            this.btnWorkflowDelete.UseVisualStyleBackColor = true;
            this.btnWorkflowDelete.Click += new System.EventHandler(this.btnWorkflowDelete_Click);
            // 
            // btnWorkflowDown
            // 
            this.btnWorkflowDown.Location = new System.Drawing.Point(246, 3);
            this.btnWorkflowDown.Name = "btnWorkflowDown";
            this.btnWorkflowDown.Size = new System.Drawing.Size(75, 23);
            this.btnWorkflowDown.TabIndex = 13;
            this.btnWorkflowDown.Text = "Down";
            this.btnWorkflowDown.UseVisualStyleBackColor = true;
            this.btnWorkflowDown.Click += new System.EventHandler(this.btnWorkflowDown_Click);
            // 
            // btnWorkflowUp
            // 
            this.btnWorkflowUp.Location = new System.Drawing.Point(165, 3);
            this.btnWorkflowUp.Name = "btnWorkflowUp";
            this.btnWorkflowUp.Size = new System.Drawing.Size(75, 23);
            this.btnWorkflowUp.TabIndex = 12;
            this.btnWorkflowUp.Text = "Up";
            this.btnWorkflowUp.UseVisualStyleBackColor = true;
            this.btnWorkflowUp.Click += new System.EventHandler(this.btnWorkflowUp_Click);
            // 
            // btnWorkflowDuplicate
            // 
            this.btnWorkflowDuplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWorkflowDuplicate.Location = new System.Drawing.Point(165, 31);
            this.btnWorkflowDuplicate.Name = "btnWorkflowDuplicate";
            this.btnWorkflowDuplicate.Size = new System.Drawing.Size(75, 23);
            this.btnWorkflowDuplicate.TabIndex = 7;
            this.btnWorkflowDuplicate.Text = "Duplicate";
            this.btnWorkflowDuplicate.UseVisualStyleBackColor = true;
            this.btnWorkflowDuplicate.Click += new System.EventHandler(this.btnWorkflowDuplicate_Click);
            // 
            // btnWorkflowAdd
            // 
            this.btnWorkflowAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWorkflowAdd.Location = new System.Drawing.Point(0, 31);
            this.btnWorkflowAdd.Name = "btnWorkflowAdd";
            this.btnWorkflowAdd.Size = new System.Drawing.Size(75, 23);
            this.btnWorkflowAdd.TabIndex = 5;
            this.btnWorkflowAdd.Text = "Add";
            this.btnWorkflowAdd.UseVisualStyleBackColor = true;
            this.btnWorkflowAdd.Click += new System.EventHandler(this.btnWorkflowAdd_Click);
            // 
            // btnWorkMasterAdd
            // 
            this.btnWorkMasterAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWorkMasterAdd.Location = new System.Drawing.Point(3, 938);
            this.btnWorkMasterAdd.Name = "btnWorkMasterAdd";
            this.btnWorkMasterAdd.Size = new System.Drawing.Size(75, 23);
            this.btnWorkMasterAdd.TabIndex = 1;
            this.btnWorkMasterAdd.Text = "Add";
            this.btnWorkMasterAdd.UseVisualStyleBackColor = true;
            this.btnWorkMasterAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tbxFilter
            // 
            this.tbxFilter.Location = new System.Drawing.Point(3, 3);
            this.tbxFilter.Name = "tbxFilter";
            this.tbxFilter.Size = new System.Drawing.Size(317, 20);
            this.tbxFilter.TabIndex = 2;
            this.tbxFilter.TextChanged += new System.EventHandler(this.tbxFilter_TextChanged);
            // 
            // btnWorkMasterEdit
            // 
            this.btnWorkMasterEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWorkMasterEdit.Location = new System.Drawing.Point(84, 938);
            this.btnWorkMasterEdit.Name = "btnWorkMasterEdit";
            this.btnWorkMasterEdit.Size = new System.Drawing.Size(75, 23);
            this.btnWorkMasterEdit.TabIndex = 2;
            this.btnWorkMasterEdit.Text = "Edit";
            this.btnWorkMasterEdit.UseVisualStyleBackColor = true;
            this.btnWorkMasterEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnWorkMasterDuplicate
            // 
            this.btnWorkMasterDuplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWorkMasterDuplicate.Location = new System.Drawing.Point(165, 938);
            this.btnWorkMasterDuplicate.Name = "btnWorkMasterDuplicate";
            this.btnWorkMasterDuplicate.Size = new System.Drawing.Size(75, 23);
            this.btnWorkMasterDuplicate.TabIndex = 3;
            this.btnWorkMasterDuplicate.Text = "Duplicate";
            this.btnWorkMasterDuplicate.UseVisualStyleBackColor = true;
            this.btnWorkMasterDuplicate.Click += new System.EventHandler(this.btnDuplicate_Click);
            // 
            // tvWorkMasters
            // 
            this.tvWorkMasters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tvWorkMasters.HideSelection = false;
            this.tvWorkMasters.Location = new System.Drawing.Point(3, 29);
            this.tvWorkMasters.Name = "tvWorkMasters";
            this.tvWorkMasters.Size = new System.Drawing.Size(317, 903);
            this.tvWorkMasters.TabIndex = 5;
            this.tvWorkMasters.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvWorkMasters_AfterSelect);
            // 
            // btnWorkMasterDelete
            // 
            this.btnWorkMasterDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWorkMasterDelete.Location = new System.Drawing.Point(246, 938);
            this.btnWorkMasterDelete.Name = "btnWorkMasterDelete";
            this.btnWorkMasterDelete.Size = new System.Drawing.Size(75, 23);
            this.btnWorkMasterDelete.TabIndex = 4;
            this.btnWorkMasterDelete.Text = "Delete";
            this.btnWorkMasterDelete.UseVisualStyleBackColor = true;
            this.btnWorkMasterDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // pnlWorkMasterNavigation
            // 
            this.pnlWorkMasterNavigation.Controls.Add(this.tvWorkMasters);
            this.pnlWorkMasterNavigation.Controls.Add(this.btnWorkMasterDelete);
            this.pnlWorkMasterNavigation.Controls.Add(this.tbxFilter);
            this.pnlWorkMasterNavigation.Controls.Add(this.btnWorkMasterEdit);
            this.pnlWorkMasterNavigation.Controls.Add(this.btnWorkMasterDuplicate);
            this.pnlWorkMasterNavigation.Controls.Add(this.btnWorkMasterAdd);
            this.pnlWorkMasterNavigation.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlWorkMasterNavigation.Location = new System.Drawing.Point(0, 0);
            this.pnlWorkMasterNavigation.Name = "pnlWorkMasterNavigation";
            this.pnlWorkMasterNavigation.Size = new System.Drawing.Size(326, 964);
            this.pnlWorkMasterNavigation.TabIndex = 4;
            // 
            // txtImagePath
            // 
            this.txtImagePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImagePath.Location = new System.Drawing.Point(91, 423);
            this.txtImagePath.Name = "txtImagePath";
            this.txtImagePath.ReadOnly = true;
            this.txtImagePath.Size = new System.Drawing.Size(471, 20);
            this.txtImagePath.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 426);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Image Path : ";
            // 
            // ucWorkMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbxWorkflow);
            this.Controls.Add(this.gbxInfo);
            this.Controls.Add(this.pnlWorkMasterNavigation);
            this.Name = "ucWorkMaster";
            this.Size = new System.Drawing.Size(1311, 964);
            this.Load += new System.EventHandler(this.WorkMaster_Load);
            this.gbxInfo.ResumeLayout(false);
            this.gbxInfo.PerformLayout();
            this.gbxWorkflow.ResumeLayout(false);
            this.gbxContent.ResumeLayout(false);
            this.gbxContent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImageInstruction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaterialQuantity)).EndInit();
            this.gbxNodeInfo.ResumeLayout(false);
            this.gbxNodeInfo.PerformLayout();
            this.pnlWorkflowNavigation.ResumeLayout(false);
            this.pnlWorkflowControls.ResumeLayout(false);
            this.pnlWorkMasterNavigation.ResumeLayout(false);
            this.pnlWorkMasterNavigation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox tbxDescription;
        private System.Windows.Forms.TextBox tbxID;
        private System.Windows.Forms.Label lblWorkMasterDescription;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.ComboBox cbxHierarchyScope;
        private System.Windows.Forms.ComboBox cbxWorkDefinitionType;
        private System.Windows.Forms.Label lblWorkMasterHierarchyScope;
        private System.Windows.Forms.Label lblWorkMasterWorkDefinitionType;
        private System.Windows.Forms.Button btnWorkMasterSave;
        private System.Windows.Forms.GroupBox gbxInfo;
        private System.Windows.Forms.Button btnWorkMasterCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbxWorkflow;
        private System.Windows.Forms.Panel pnlWorkflowNavigation;
        private System.Windows.Forms.Panel pnlWorkflowControls;
        private System.Windows.Forms.Button btnWorkflowDelete;
        private System.Windows.Forms.Button btnWorkflowDuplicate;
        private System.Windows.Forms.Button btnWorkflowAdd;
        private System.Windows.Forms.GroupBox gbxNodeInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxNodeId;
        private System.Windows.Forms.TextBox tbxNodeDescription;
        private System.Windows.Forms.TreeView tvWorkflow;
        private System.Windows.Forms.ComboBox cbxNodeType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnWorkflowEdit;
        private System.Windows.Forms.Button btnWorkflowCancel;
        private System.Windows.Forms.Button btnWorkflowSave;
        private System.Windows.Forms.GroupBox gbxContent;
        private System.Windows.Forms.Button btnWorkflowDown;
        private System.Windows.Forms.Button btnWorkflowUp;
        private System.Windows.Forms.NumericUpDown nudMaterialQuantity;
        private System.Windows.Forms.ComboBox cbxMaterialDefinition;
        private System.Windows.Forms.Button btnPickingDelete;
        private System.Windows.Forms.Button btnPickingAdd;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbxTextInstruction;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblWorkMaster;
        private System.Windows.Forms.ComboBox cbxNodeWorkMaster;
        private System.Windows.Forms.ListView lvPickingActions;
        private System.Windows.Forms.ColumnHeader cMaterialDefinition;
        private System.Windows.Forms.ColumnHeader cQuantity;
        private System.Windows.Forms.PictureBox pbxImageInstruction;
        private System.Windows.Forms.Button btnBrowseImage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnWorkMasterDelete;
        private System.Windows.Forms.TreeView tvWorkMasters;
        private System.Windows.Forms.Button btnWorkMasterDuplicate;
        private System.Windows.Forms.Button btnWorkMasterEdit;
        private System.Windows.Forms.TextBox tbxFilter;
        private System.Windows.Forms.Button btnWorkMasterAdd;
        private System.Windows.Forms.Panel pnlWorkMasterNavigation;
        private System.Windows.Forms.TextBox tbxCobotCommando;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtImagePath;
    }
}
