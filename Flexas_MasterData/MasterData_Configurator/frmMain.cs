﻿using ISyE.AssemblyInformation.MongoDB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISyE.AssemblyInformation.Configurator
{
    public partial class frmMain : Form
    {
        private MongoDAL _oMongoDAL;
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            _oMongoDAL = new MongoDAL("mongodb://FlexAs:FlexAs@172.16.220.101:27017", "FlexAs");
            ucWM.SetMongoDAL(_oMongoDAL);         
        }
    }
}
