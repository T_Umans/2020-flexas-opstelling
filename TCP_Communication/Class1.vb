Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Public Class clsTCP
    Private _oTcpListener As TcpListener
    Private _oTcpClientList As New List(Of TcpClient)
    Private _sServerIP As String
    Private _iPort As Integer
    Private WithEvents _oCheckTimer As New System.Timers.Timer
    Private _oLog As clsLog = clsLog.Instance
    Public Event NewMessage(sender As Object, e As MessageEventargs)
    Public Event ClientDisconnected(sender As Object, e As MessageEventargs)
    Public Sub ConnectToServer()
        Dim oRemoteEP As New IPEndPoint(IPAddress.Parse(_sServerIP), _iPort)
        _oTcpClientList = New List(Of TcpClient)
        _oTcpClientList.Add(New TcpClient())
        Try
            _oTcpClientList.Item(0).Connect(oRemoteEP)
        Catch ex As Exception
            CrashMe("could not connect to server")
        End Try
        If CheckConnected() Then
            _oCheckTimer.Start()
        Else
            CrashMe("Server not found")
        End If
    End Sub
    Public Function CheckConnected()
        If _oTcpClientList IsNot Nothing AndAlso _oTcpClientList.Count > 0 AndAlso _oTcpClientList.Item(0) IsNot Nothing Then
            Try
                If _oTcpClientList.Item(0).Connected Then
                    Dim tts1 As Boolean = _oTcpClientList.Item(0).Client.Poll(100, SelectMode.SelectRead)
                    Dim tts2 As Boolean = _oTcpClientList.Item(0).Client.Available
                    If tts1 And Not tts2 Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        Else
            Return False
        End If
    End Function
    Public Sub Disconnect()
        _oCheckTimer.Stop()
        If _oTcpClientList IsNot Nothing AndAlso _oTcpClientList.Count > 0 AndAlso _oTcpClientList.Item(0) IsNot Nothing AndAlso CheckConnected() Then
            Try
                _oTcpClientList.Item(0).Client.Close()
            Catch ex As Exception
                Throw New ApplicationException("Error while trying to disconnect from server")
            End Try
            _oTcpClientList.Item(0) = Nothing
            _oTcpClientList = Nothing
            Dim oMessageEventargs As New MessageEventargs
            oMessageEventargs.sMessage = New List(Of String) From {"Succesfully disconnected from server"}
            RaiseEvent ClientDisconnected(Me, oMessageEventargs)
        Else
            Dim oMessageEventargs As New MessageEventargs
            oMessageEventargs.sMessage = New List(Of String) From {"There was no active connection available"}
            RaiseEvent ClientDisconnected(Me, oMessageEventargs)
        End If
    End Sub
    Private Sub ConnectionLost()
        _oCheckTimer.Stop()
        If _oTcpClientList IsNot Nothing AndAlso _oTcpClientList.Count > 0 Then
            _oTcpClientList.Item(0) = Nothing
        End If
        _oTcpClientList = Nothing
        Dim oMessageEventargs As New MessageEventargs
        oMessageEventargs.sMessage = New List(Of String) From {"Connection lost from server"}
        RaiseEvent ClientDisconnected(Me, oMessageEventargs)
    End Sub
    Private Sub CrashMe(sMessage As String)
        'Throw New ApplicationException(sMessage)
        _oLog.Log(sMessage)
    End Sub
    Public Sub SendMessage(sMessage As String)
        If _oTcpClientList IsNot Nothing AndAlso _oTcpClientList.Count > 0 AndAlso _oTcpClientList.Item(0) IsNot Nothing AndAlso CheckConnected() Then
            Dim oStream As NetworkStream = _oTcpClientList.Item(0).GetStream
            If oStream.CanWrite Then
                Dim arrbSendData() As Byte
                Dim s As String = sMessage + ";"
                '  If _xEncryption Then s = Encryption(sMessage, _iKey)
                arrbSendData = System.Text.Encoding.ASCII.GetBytes(s)
                oStream.Write(arrbSendData, 0, arrbSendData.Length)
            End If
        Else
            ConnectionLost()
        End If
    End Sub
    Public Sub New(sServerIP As String, iPort As Integer, iUpdateTime As Integer)
        _sServerIP = sServerIP
        _iPort = iPort
        _oCheckTimer.Interval = iUpdateTime
    End Sub
    Private Sub _oCheckTimer_Tick(sender As Object, e As EventArgs) Handles _oCheckTimer.Elapsed
        _oCheckTimer.Stop()
        'code voor client
        If CheckConnected() Then
            Dim oStream As NetworkStream = _oTcpClientList.Item(0).GetStream
            If oStream.CanRead And oStream.DataAvailable Then
                Dim arrSendData(_oTcpClientList.Item(0).Available - 1) As Byte
                oStream.Read(arrSendData, 0, arrSendData.Length)
                Dim sMessage As String = System.Text.Encoding.Unicode.GetChars(arrSendData)
                ' If _xEncryption Then sMessage = Encryption(sMessage, _iKey)
                Dim oMessageEventargs As New MessageEventargs
                Dim oReceived As New List(Of String)
                For i As Integer = 0 To sMessage.Split("�").Length() - 2
                    oReceived.Add(sMessage.Split("�")(i))
                Next
                oMessageEventargs.oTCPClient = _oTcpClientList.Item(0)
                oMessageEventargs.sMessage = oReceived
                oMessageEventargs.sExtraInfo = "steek hier extra info in"
                RaiseEvent NewMessage(Me, oMessageEventargs)
            End If
        Else
            ConnectionLost()
            Exit Sub
        End If
        _oCheckTimer.Start()
    End Sub
    Public ReadOnly Property TCPClients As List(Of TcpClient)
        Get
            Return _oTcpClientList
        End Get
    End Property
End Class