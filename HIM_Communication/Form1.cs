﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HIM_Communication
{
    public partial class frmApiTest : Form
    {
        private clsHimCommunication _oHIM;

        public frmApiTest()
        {
            InitializeComponent();
        }

        private void frmApiTest_Load(object sender, EventArgs e)
        {
            _oHIM = new clsHimCommunication();
            _oHIM.StepChanged += StepChanged;
            _oHIM.PickToLightRequest += PickToLight;

            lbxTasks.DataSource = _oHIM.Processes;
        }


        private void StepChanged()
        {
            this.Invoke((MethodInvoker)delegate
            {
                tbxCurrentStep.Text = _oHIM.CurrentStep;
                cbxFinished.Checked = !_oHIM.ProcessRunning;
            });
        }

        private void PickToLight()
        {
            this.Invoke((MethodInvoker)delegate
            {
                tbxPicking.Text = _oHIM.PickMaterial;
            });
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxGetVariable.Text)) return;
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxSetVariable.Text)) return;
            _oHIM.SetVariable(tbxSetVariable.Text, tbxSetValue.Text);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (lbxTasks.SelectedIndex > -1) _oHIM.StartProcess(lbxTasks.SelectedItem.ToString());

        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            _oHIM.PreviousStep();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            _oHIM.NextStep();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            _oHIM.ResetProcess();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            _oHIM.StopProcess();
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            _oHIM.PauseProcess();
        }

    }
}
