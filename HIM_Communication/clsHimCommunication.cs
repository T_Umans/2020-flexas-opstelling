﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HIM_Communication
{
    public delegate void StepChangedHandler();
    public delegate void PickToLightHandler();
    public delegate void ProcessFinishedHandler();
    public class clsHimCommunication
    {
        private string _sBaseUrl = "http://172.16.220.30:7777/api/v1/";
        private string _sUnitId = "50128116154";
        private string _sProjectId = "-1190584166678559720";
        private Dictionary<string, string> _oProcessList;
        private string _sCurrentProcess;
        private Timer _oTimer;
        public event StepChangedHandler StepChanged;
        public event PickToLightHandler PickToLightRequest;
        public event ProcessFinishedHandler ProcessFinished;
        private string _sCurrentStep;
        private bool _xPickToLight = false;
        private bool _xProcessRunning = false;
        private bool _xAvailable = false;
        private bool _xStartingProcess = false;
        private bool _xBrowsing = false;
        public clsHimCommunication()
        { 
            _oTimer = new Timer(TimerTick, null, Timeout.Infinite, Timeout.Infinite);
            if (HimProject != null)
            {
                if (HimProject == "FlexAS")
                {
                    _xAvailable = true;
                    EnablePolling();
                    _oProcessList = GetProcesses();
                    _sCurrentStep = CurrentStep;
                }
            }            
        }

        public static bool PingHost(string IpAddress)
        {
            bool pingable = false;
            Ping pinger = null;
            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(IpAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }
            return pingable;
        }

        public void EnablePolling()
        {
            _oTimer = new Timer(TimerTick, null, 0, 250);
        }
        public void DisablePolling()
        {
            _oTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        public bool Available => _xAvailable;
        public string[] Processes => Available ? _oProcessList.Keys.ToArray() : null;
        public string CurrentStep => Available ? GetVariable("FlexAs_CurrentStep") : null;
        public string PickMaterial => Available ? GetVariable("FlexAs_PickToLight") : null;
        public bool ProcessRunning => Available ? GetVariable("FlexAs_ProcessRunning") == "TRUE" : false;
        public bool Ack => Available ? GetVariable("FlexAs_HimAck") == "TRUE" : false;
        public string HimProject => GetLoadedProject()?.Name;

        public void SetTextInstructon(string TextInstruction)
        {
            SetVariable("FlexAs_TextInstruction", TextInstruction);
        }
        public void SetImageInstruction(string ImageInstruction)
        {
            SetVariable("FlexAs_ImageInstruction", ImageInstruction);
        }

        public HimObject GetLoadedProject()
        {
            try
            {
                var sURL = _sBaseUrl + "units/" + _sUnitId + "/loadedProject/";
                var webClient = new System.Net.WebClient();
                var sResult = webClient.DownloadString(sURL);
                if (sResult == null) return null;
                var oHimObject = JsonConvert.DeserializeObject<HimObject>(sResult);
                return oHimObject;
            }
            catch (System.Net.WebException ex)
            {
                return null;
            }
        }
        public string GetVariable(string Name)
        {
            try
            {
                var sURL = _sBaseUrl + "units/" + _sUnitId + "/variables/" + Name;
                var webClient = new System.Net.WebClient();
                var sResult = webClient.DownloadString(sURL);
                if (sResult == null) return null;
                var oValiable = JsonConvert.DeserializeObject<HimVariable>(sResult);
                return oValiable.CurrentState;
            }
            catch (System.Net.WebException ex)
            {
                return null;
            }
        }

        public void TimerTick(object obj)
        {
            if (!_xBrowsing)
            {
                if (CurrentStep != "" && !string.Equals(_sCurrentStep, CurrentStep))
                {
                    _sCurrentStep = CurrentStep;
                    if (!_xStartingProcess && CurrentStep != "") StepChanged?.Invoke();
                }
            }
            if (string.IsNullOrEmpty(PickMaterial)) _xPickToLight = false;
            else if (!_xPickToLight)
            {
                _xPickToLight = true;
                PickToLightRequest?.Invoke();
            }
            if (ProcessRunning) _xProcessRunning = true;
            if (_xProcessRunning & !ProcessRunning)
            {
                _xProcessRunning = ProcessRunning;
                ProcessFinished?.Invoke();
            }
        }
        public void SetVariable(string Name, string Value)
        {
            try
            {
                var oVariable = new HimVariable[] { new HimVariable() { Name = Name, CurrentState = Value } };
                var sParameters = JsonConvert.SerializeObject(oVariable);
                var webClient = new System.Net.WebClient();                
                var sResult = webClient.UploadString(_sBaseUrl + "units/" + _sUnitId + "/variables/", "PUT", sParameters);
            }
            catch (Exception ex)
            {
                return;
            }
        }
        public void StartProcess(string ProcessId, int StepIndex = 0)
        {
            try
            {
                _xStartingProcess = true;
                _sCurrentStep = StepIndex.ToString();
                if (_oProcessList == null) return;
                if (!_oProcessList.ContainsKey(ProcessId)) return;
                _sCurrentProcess = _oProcessList[ProcessId];
                var oVariable = new HimProcess[] { new HimProcess() { ProcessId = _sCurrentProcess, ControlType = "Play" }};
                var sParameters = JsonConvert.SerializeObject(oVariable);
                var webClient = new System.Net.WebClient();
                var sResult = webClient.UploadString(_sBaseUrl + "units/" + _sUnitId + "/processes/control/", sParameters);
                //while (CurrentStep != "" && int.Parse(CurrentStep) < StepIndex) NextStep();
                GoToStep(StepIndex);
                //while (CurrentStep != "" && int.Parse(CurrentStep) > StepIndex) PreviousStep();
                _xStartingProcess = false;
            }
            catch (Exception ex)
            {
                _xStartingProcess = false;
                return;
            }
        }
        public void ResetProcess()
        {
            try
            {
                if (string.IsNullOrEmpty(_sCurrentProcess)) return;
                var oVariable = new HimProcess[] { new HimProcess() { ProcessId = _sCurrentProcess, ControlType = "Reset" } };
                var sParameters = JsonConvert.SerializeObject(oVariable);
                var webClient = new System.Net.WebClient();
                var sResult = webClient.UploadString(_sBaseUrl + "units/" + _sUnitId + "/processes/control/", sParameters);
            }
            catch (Exception ex)
            {
                return;
            }
        }
        public void NextStep()
        {
            try
            {
                if (string.IsNullOrEmpty(_sCurrentProcess)) return;
                SetVariable("FlexAs_Previous", "FALSE");
                var oVariable = new HimProcess[] { new HimProcess() { ProcessId = _sCurrentProcess, ControlType = "Next" } };
                var sParameters = JsonConvert.SerializeObject(oVariable);
                var webClient = new System.Net.WebClient();
                var sResult = webClient.UploadString(_sBaseUrl + "units/" + _sUnitId + "/processes/control/", sParameters);
            }
            catch (Exception ex)
            {
                return;
            }
        }

        //public void PreviousStep()
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_sCurrentProcess)) return;
        //        SetVariable("FlexAs_Previous", "TRUE");
        //        var oVariable = new HimProcess[] { new HimProcess() { ProcessId = _sCurrentProcess, ControlType = "Previous" } };
        //        var sParameters = JsonConvert.SerializeObject(oVariable);
        //        var webClient = new System.Net.WebClient();
        //        var sResult = webClient.UploadString(_sBaseUrl + "units/" + _sUnitId + "/processes/control/", sParameters);
        //        SetVariable("FlexAs_Previous", "FALSE");
        //    }
        //    catch (Exception ex)
        //    {
        //        return;
        //    }
        //}

        public void PreviousStep()
        {
            GoToStep(int.Parse(CurrentStep) - 1);
        }
        public void GoToStep(int iStep)
        {
            if (iStep < 0) return;
            _xBrowsing = true;
            ResetProcess();
            SetVariable("FlexAs_CurrentStep", "0");
            while (int.Parse(CurrentStep) < iStep)
            {
                if (!Ack) NextStep();
                else SetVariable("FlexAs_HimAck", "FALSE");
            }
            _xBrowsing = false;
        }


        public void PauseProcess()
        {
            try
            {
                if (string.IsNullOrEmpty(_sCurrentProcess)) return;
                var oVariable = new HimProcess[] { new HimProcess() { ProcessId = _sCurrentProcess, ControlType = "Pause" } };
                var sParameters = JsonConvert.SerializeObject(oVariable);
                var webClient = new System.Net.WebClient();
                var sResult = webClient.UploadString(_sBaseUrl + "units/" + _sUnitId + "/processes/control/", sParameters);
            }
            catch (Exception ex)
            {
                return;
            }
        }
        public void StopProcess()
        {
            //PauseProcess();
            //ResetProcess();                    
            StartProcess("RESET");
            _xProcessRunning = false;
            _sCurrentProcess = null;
        }
        public HimObject[] GetActiveSteps()
        {
            try
            {
                var webClient = new System.Net.WebClient();
                var sResult = webClient.DownloadString(_sBaseUrl + "processes/" + _sCurrentProcess + "/activeSteps/");
                var oValiable = JsonConvert.DeserializeObject<HimObject[]>(sResult);
                return oValiable;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private HimObject[] GetHimProcesses()
        {
            try
            {
                var webClient = new System.Net.WebClient();
                var sResult = webClient.DownloadString(_sBaseUrl + "projects/" + _sProjectId + "/processes");
                if (sResult == null) return null;
                var oValiable = JsonConvert.DeserializeObject<HimObject[]>(sResult);
                return oValiable;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private HimObject[] GetHimTasks()
        {
            try
            {
                var webClient = new System.Net.WebClient();
                var sResult = webClient.DownloadString(_sBaseUrl + "projects/" + _sProjectId + "/tasks");
                var oValiable = JsonConvert.DeserializeObject<HimObject[]>(sResult);
                return oValiable;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Dictionary<string, string> GetTasks()
        {
            var oTasks = new Dictionary<string, string>();
            foreach (var oTask in GetHimTasks())
            {
                oTasks.Add(oTask.Name, oTask.Id);
            }
            return oTasks;
        }
        public Dictionary<string,string> GetProcesses()
        {
            var oProcesses = new Dictionary<string, string>();
            foreach(var oProcess in GetHimProcesses())
            {
                oProcesses.Add(oProcess.Name, oProcess.Id);
            }
            return oProcesses;
        }
        public class HimVariable
        {
            public string Name { get; set; }
            public string CurrentState { get; set; }
            public string DefaultWorkflowEvent { get; set; }
            public string Id { get; set; }
        }
        public class HimProcess
        {
            public string ProcessId { get; set; }
            public string ControlType { get; set; }
        }
        public class HimObject
        {
            public string Description { get; set; }
            public string Type { get; set; }
            public string Id { get; set; }
            public string Name { get; set; }
        }
    }
}
