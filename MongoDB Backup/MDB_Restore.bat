@echo off

:: Find most recent file
for /f "tokens=*" %%a in ('dir /A:-D /B /O:-D /S "Backups"') do set NEW=%%a&& goto:n 
:n
:: Get the file name
for %%i in ("%NEW%") do set filename=%%~ni

:: Create directory
echo Restoring from %filename%...
set dirname=Backups
mkdir "%dirname%\%filename%"
:: UNZIP the backup folder
"%ProgramFiles%\WinRAR\Rar.exe" x -ibck -idq "%NEW%" "%dirname%"
pause
:: Restore db
mongorestore --host 172.16.220.101 --port 27017^ --username "FlexAs" --password "FlexAs" --authenticationDatabase "admin" --drop "%dirname%\%filename%"

:: Remove the directory
rmdir "%dirname%\%filename%" /s /q
echo RESTORE COMPLETE
pause



