@echo off
:: Create timestamp
set year=%date:~9,4%
set month=%date:~6,2%
set day=%date:~3,2%
set hour=%time:~0,2%
set min=%time:~3,2%
set sec=%time:~6,2%
set timestamp=%year%%month%%day%_%hour%%min%%sec%
:: Create new directory
set dirname=MDB_Backup_FlexAS_%timestamp%
mkdir "Backups/%dirname%"
:: Export the database
mongodump --host 172.16.220.101 --port 27017^ --username "FlexAs" --password "FlexAs" --authenticationDatabase "admin" --db FlexAs --out "Backups/%dirname%/"

:: ZIP the backup folder
"%ProgramFiles%\WinRAR\Rar.exe" a -ep1 -idq -r -y "Backups/%dirname%" "Backups/%dirname%"
:: Remove the backup folder
rmdir "Backups/%dirname%" /s /q
echo BACKUP COMPLETE

