﻿public struct ST_PickAction
{

    public ST_PickAction(string sLabel, int iQuantity)
    {
        Label = sLabel;
        Quantity = iQuantity;
    }
    public string Label { get;  }
    public int Quantity { get; }
}