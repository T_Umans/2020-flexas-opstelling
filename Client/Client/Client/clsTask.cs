﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class clsTask
    {
        private string _sTaskId;
        private string _sStepId;
        private string _sInstructionId;
        private string _sTextInstruction;
        private string _sImagePath;
        private string _sWorkstationMode;
        private List<ST_PickAction> _oPickingActions;

        public clsTask(string[] arrsData)
        {
            //Message structure: Data|TaskId|StepId|InstructionId|Text|ImagePath|WorkstationMode|Material1§Quantity1§Material2§Quantity2
            if (arrsData.Length == 8)
            {
                _sTaskId = arrsData[1];
                _sStepId = arrsData[2];
                _sInstructionId = arrsData[3];
                _sTextInstruction = arrsData[4];
                _sImagePath = arrsData[5];
                _sWorkstationMode = arrsData[6];

                _oPickingActions = new List<ST_PickAction>();
                if (!(arrsData[7] == null) & !(arrsData[7] == ""))
                {
                    string[] arrsPicking = arrsData[7].Split('§');
                    for (int i = 0; i < arrsPicking.Length; i = i + 2)
                    {
                        _oPickingActions.Add(new ST_PickAction(arrsPicking[i], int.Parse(arrsPicking[i + 1])));
                        
                    }
                }
            }
        }

        public string TaskId { get { return _sTaskId; } }
        public string StepId { get { return _sStepId; } }
        public string InstructionId { get { return _sInstructionId; } }
        public string TextInstruction { get { return _sTextInstruction; } }
        public string ImagePath { get { return _sImagePath; } }
        public string WorkstationMode { get { return _sWorkstationMode; } }
        public string PickingActions
        {
            get
            {
                string s = "";
                foreach (ST_PickAction o in _oPickingActions)
                {
                    s += o.Label + " : " + o.Quantity.ToString() + "\r\n";
                }
                return s;
            }
        }

    }
}
