﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public delegate void NewTaskEventHandler(clsTask o);
    public delegate void KlemStatushandler(int i);
    public delegate void MeasurementStatusHandler(int i);
    public delegate void DubbeleHandbedieningactiefhandler();
    public delegate void DubbeleHandbedieningokhandler();
    public delegate void NextStepCommingHandler();

    public class clsMainLogic
    {
        private TCPC.clsTCPClient _oTcpClient;
        public string _sCurrentWorkstation;
        public clsTask _oCurrentTask;
        public string _sMultiMessageString;

        //event
        public event NewTaskEventHandler NewTaskEvent;
        public event EventHandler Disconnected;
        public event KlemStatushandler KlemStatus;
        public event MeasurementStatusHandler MeasurementStatus;
        public event DubbeleHandbedieningactiefhandler DubbeleHandbedieningactief;
        public event DubbeleHandbedieningokhandler DubbeleHandbedieningok;
        public event NextStepCommingHandler NextStepComming;

        private int _iKlemStatus = 0 ;
        private int _iMeasurementStatus = 0;
        public void TCP_Connect(string sCurrentWorkstation, string sIP)
        {
            try
            {
                _sCurrentWorkstation = sCurrentWorkstation;
                _oTcpClient = new TCPC.clsTCPClient(sIP, 100, 0);
                _oTcpClient.ConnectToServer();
                _oTcpClient.NewMessage += TCP_NewMessage;
                _oTcpClient.ClientDisconnected += TCP_Disconnected;
            }
            catch { }            
        }

        public bool CheckConnected()
        {
            if (_oTcpClient == null)
            {
                return false;
            }
            else
            {
                return (bool)_oTcpClient.CheckConnected();
            }
        }

        private void TCP_NewMessage(object sender, TCPC.MessageEventargs e)
        {
            _sMultiMessageString = _sMultiMessageString + e.sMessage;
            string[] arrMessages = _sMultiMessageString.Split('$');
            for (int i = 0; i < arrMessages.Length-1; i++)
            {
                if (arrMessages.Contains("|"))
                {
                    ProcessNewMessage(arrMessages[i]);
                    
                }
            }
            //als het laatste tekst bevat, was z'n boodschap nog niet volledig en mag het er staan
            //als alles wel verzonden was, is de laatste arrmessage empty
            _sMultiMessageString = arrMessages[arrMessages.Length - 1];
        }

        private void ProcessNewMessage(string sMessage)
        {
            string[] arrsData = sMessage.Split('|');
            string sResponse = "";
            try
            {
                switch (arrsData[0])
                {
                    case "Who are you?":
                        sResponse = "I am|" + _sCurrentWorkstation;
                        break;
                    case "Data":
                        _oCurrentTask = new clsTask(arrsData);
                        RunInNewThread(new Action[] { () => NewTaskEvent?.Invoke(_oCurrentTask) });
                        sResponse = "ACK";
                        break;
                    case "Klem":
                        if (arrsData[1] == null || arrsData[1] == "")
                        {
                            break;
                        }
                        bool x = bool.Parse(arrsData[1]);
                        if (x)
                        {
                            _iKlemStatus = 1;
                        }
                        else
                        {
                            _iKlemStatus = 2;
                        }
                        RunInNewThread(new Action[] { () => KlemStatus?.Invoke(_iKlemStatus) });
                        break;
                    case "Measurement":
                        if (arrsData[1] == null || arrsData[1] == "")
                        {
                            break;
                        }
                        bool x1 = bool.Parse(arrsData[1]);
                        if (x1)
                        {
                            _iMeasurementStatus = 1;
                        }
                        else
                        {
                            _iMeasurementStatus = 2;
                        }
                        RunInNewThread(new Action[] { () => MeasurementStatus?.Invoke(_iMeasurementStatus), () => Thread.Sleep(3000), () => KlemStatus?.Invoke(_iKlemStatus) });
                        break;
                    case "KlemmenBusy":
                        RunInNewThread(new Action[] { () => DubbeleHandbedieningactief?.Invoke() });
                        break;
                    case "KlemmenOK":
                        RunInNewThread(new Action[] { () => DubbeleHandbedieningok?.Invoke() });
                        break;
                    case "NextStepIncomming":

                        if (arrsData[1] == "1")
                        {
                            RunInNewThread(new Action[] { () => NextStepComming?.Invoke() });
                        }
                        else
                        {

                        }

                        break;
                }
                if (sResponse != "") { _oTcpClient.SendMessage(sResponse); }
            }
            catch (Exception ex)
            {
                MessageBox.Show("an error has occured\r\n\r\n" + ex.Message + "\r\n\r\n" + ex.StackTrace);
            }
        }


  
        private void TCP_Disconnected(object sender, EventArgs e)
        {
            Disconnected?.Invoke(this, e);
            //events ontkoppelen
            _oTcpClient.ClientDisconnected -= TCP_Disconnected;
            _oTcpClient.NewMessage -= TCP_NewMessage;
            _oTcpClient = null;
        }

        public void TCP_Disconnect()
        {
          if (_oTcpClient != null){
                _oTcpClient.Disconnect();
                NewTaskEvent?.Invoke(null);
               }
        }

        public int CurrentKlemStatus
        {
            get{
                return _iKlemStatus;
            }
        }

        #region "Auxiliary"
        private void RunInNewThread(Action[] arrActions)
        {
            Thread t = new Thread(() => {
                foreach (Action oAction in arrActions)
                {
                    oAction();
                }
            });
            t.Start();
        }




        #endregion

    }
}
