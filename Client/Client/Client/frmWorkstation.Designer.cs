﻿namespace Client
{
    partial class frmWorkstation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboWorkstation = new System.Windows.Forms.ComboBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.txtIP = new System.Windows.Forms.TextBox();
            this.gbxConfig = new System.Windows.Forms.GroupBox();
            this.btnTCPDisconnect = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTaskId = new System.Windows.Forms.TextBox();
            this.txtStepId = new System.Windows.Forms.TextBox();
            this.txtInstructionId = new System.Windows.Forms.TextBox();
            this.txtTextInstruction = new System.Windows.Forms.TextBox();
            this.txtImagePath = new System.Windows.Forms.TextBox();
            this.txtWorkstationMode = new System.Windows.Forms.TextBox();
            this.txtPickingActions = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.gpbTaskInformation = new System.Windows.Forms.GroupBox();
            this.btnStartOperatorView = new System.Windows.Forms.Button();
            this.btnStopOperatorView = new System.Windows.Forms.Button();
            this.btnCloseRDP = new System.Windows.Forms.Button();
            this.gbxConfig.SuspendLayout();
            this.gpbTaskInformation.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboWorkstation
            // 
            this.cboWorkstation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboWorkstation.FormattingEnabled = true;
            this.cboWorkstation.Items.AddRange(new object[] {
            "Vrijwerkvlak",
            "Perstafel",
            "Afwerkpost"});
            this.cboWorkstation.Location = new System.Drawing.Point(116, 39);
            this.cboWorkstation.Name = "cboWorkstation";
            this.cboWorkstation.Size = new System.Drawing.Size(121, 21);
            this.cboWorkstation.TabIndex = 0;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(50, 66);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 1;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // txtIP
            // 
            this.txtIP.Location = new System.Drawing.Point(116, 13);
            this.txtIP.Name = "txtIP";
            this.txtIP.Size = new System.Drawing.Size(121, 20);
            this.txtIP.TabIndex = 2;
            this.txtIP.Text = "172.16.220.101";
            // 
            // gbxConfig
            // 
            this.gbxConfig.Controls.Add(this.btnTCPDisconnect);
            this.gbxConfig.Controls.Add(this.label2);
            this.gbxConfig.Controls.Add(this.btnConnect);
            this.gbxConfig.Controls.Add(this.cboWorkstation);
            this.gbxConfig.Controls.Add(this.label1);
            this.gbxConfig.Controls.Add(this.txtIP);
            this.gbxConfig.Location = new System.Drawing.Point(12, 12);
            this.gbxConfig.Name = "gbxConfig";
            this.gbxConfig.Size = new System.Drawing.Size(299, 105);
            this.gbxConfig.TabIndex = 3;
            this.gbxConfig.TabStop = false;
            this.gbxConfig.Text = "Configuration";
            // 
            // btnTCPDisconnect
            // 
            this.btnTCPDisconnect.Location = new System.Drawing.Point(131, 66);
            this.btnTCPDisconnect.Name = "btnTCPDisconnect";
            this.btnTCPDisconnect.Size = new System.Drawing.Size(75, 23);
            this.btnTCPDisconnect.TabIndex = 5;
            this.btnTCPDisconnect.Text = "Disconnect";
            this.btnTCPDisconnect.UseVisualStyleBackColor = true;
            this.btnTCPDisconnect.Click += new System.EventHandler(this.btnTCPDisconnect_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Workstation:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Master IP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Task id:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Step id:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Instruction id:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Workstation mode:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Image path:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Text instruction:";
            // 
            // txtTaskId
            // 
            this.txtTaskId.Location = new System.Drawing.Point(117, 22);
            this.txtTaskId.Name = "txtTaskId";
            this.txtTaskId.ReadOnly = true;
            this.txtTaskId.Size = new System.Drawing.Size(167, 20);
            this.txtTaskId.TabIndex = 10;
            // 
            // txtStepId
            // 
            this.txtStepId.Location = new System.Drawing.Point(117, 48);
            this.txtStepId.Name = "txtStepId";
            this.txtStepId.ReadOnly = true;
            this.txtStepId.Size = new System.Drawing.Size(167, 20);
            this.txtStepId.TabIndex = 11;
            // 
            // txtInstructionId
            // 
            this.txtInstructionId.Location = new System.Drawing.Point(117, 74);
            this.txtInstructionId.Name = "txtInstructionId";
            this.txtInstructionId.ReadOnly = true;
            this.txtInstructionId.Size = new System.Drawing.Size(167, 20);
            this.txtInstructionId.TabIndex = 12;
            // 
            // txtTextInstruction
            // 
            this.txtTextInstruction.Location = new System.Drawing.Point(117, 100);
            this.txtTextInstruction.Name = "txtTextInstruction";
            this.txtTextInstruction.ReadOnly = true;
            this.txtTextInstruction.Size = new System.Drawing.Size(167, 20);
            this.txtTextInstruction.TabIndex = 13;
            // 
            // txtImagePath
            // 
            this.txtImagePath.Location = new System.Drawing.Point(117, 126);
            this.txtImagePath.Name = "txtImagePath";
            this.txtImagePath.ReadOnly = true;
            this.txtImagePath.Size = new System.Drawing.Size(167, 20);
            this.txtImagePath.TabIndex = 14;
            // 
            // txtWorkstationMode
            // 
            this.txtWorkstationMode.Location = new System.Drawing.Point(117, 152);
            this.txtWorkstationMode.Name = "txtWorkstationMode";
            this.txtWorkstationMode.ReadOnly = true;
            this.txtWorkstationMode.Size = new System.Drawing.Size(167, 20);
            this.txtWorkstationMode.TabIndex = 15;
            // 
            // txtPickingActions
            // 
            this.txtPickingActions.Location = new System.Drawing.Point(117, 178);
            this.txtPickingActions.Multiline = true;
            this.txtPickingActions.Name = "txtPickingActions";
            this.txtPickingActions.ReadOnly = true;
            this.txtPickingActions.Size = new System.Drawing.Size(167, 99);
            this.txtPickingActions.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 181);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Picking actions:4";
            // 
            // gpbTaskInformation
            // 
            this.gpbTaskInformation.Controls.Add(this.txtPickingActions);
            this.gpbTaskInformation.Controls.Add(this.label10);
            this.gpbTaskInformation.Controls.Add(this.label4);
            this.gpbTaskInformation.Controls.Add(this.label5);
            this.gpbTaskInformation.Controls.Add(this.txtWorkstationMode);
            this.gpbTaskInformation.Controls.Add(this.label6);
            this.gpbTaskInformation.Controls.Add(this.txtImagePath);
            this.gpbTaskInformation.Controls.Add(this.label7);
            this.gpbTaskInformation.Controls.Add(this.txtTextInstruction);
            this.gpbTaskInformation.Controls.Add(this.label8);
            this.gpbTaskInformation.Controls.Add(this.txtInstructionId);
            this.gpbTaskInformation.Controls.Add(this.label9);
            this.gpbTaskInformation.Controls.Add(this.txtStepId);
            this.gpbTaskInformation.Controls.Add(this.txtTaskId);
            this.gpbTaskInformation.Location = new System.Drawing.Point(12, 123);
            this.gpbTaskInformation.Name = "gpbTaskInformation";
            this.gpbTaskInformation.Size = new System.Drawing.Size(299, 288);
            this.gpbTaskInformation.TabIndex = 18;
            this.gpbTaskInformation.TabStop = false;
            this.gpbTaskInformation.Text = "Debug Information";
            // 
            // btnStartOperatorView
            // 
            this.btnStartOperatorView.Location = new System.Drawing.Point(12, 418);
            this.btnStartOperatorView.Name = "btnStartOperatorView";
            this.btnStartOperatorView.Size = new System.Drawing.Size(299, 48);
            this.btnStartOperatorView.TabIndex = 19;
            this.btnStartOperatorView.Text = "Start Operator View";
            this.btnStartOperatorView.UseVisualStyleBackColor = true;
            this.btnStartOperatorView.Click += new System.EventHandler(this.btnStartOperatorView_Click);
            // 
            // btnStopOperatorView
            // 
            this.btnStopOperatorView.Location = new System.Drawing.Point(12, 472);
            this.btnStopOperatorView.Name = "btnStopOperatorView";
            this.btnStopOperatorView.Size = new System.Drawing.Size(299, 48);
            this.btnStopOperatorView.TabIndex = 20;
            this.btnStopOperatorView.Text = "Stop Operator View";
            this.btnStopOperatorView.UseVisualStyleBackColor = true;
            this.btnStopOperatorView.Click += new System.EventHandler(this.btnStopOperatorView_Click);
            // 
            // btnCloseRDP
            // 
            this.btnCloseRDP.Location = new System.Drawing.Point(12, 546);
            this.btnCloseRDP.Name = "btnCloseRDP";
            this.btnCloseRDP.Size = new System.Drawing.Size(299, 48);
            this.btnCloseRDP.TabIndex = 21;
            this.btnCloseRDP.Text = "Close RDP and Start Operator View";
            this.btnCloseRDP.UseVisualStyleBackColor = true;
            this.btnCloseRDP.Click += new System.EventHandler(this.btnCloseRDP_Click);
            // 
            // frmWorkstation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 606);
            this.Controls.Add(this.btnCloseRDP);
            this.Controls.Add(this.btnStopOperatorView);
            this.Controls.Add(this.btnStartOperatorView);
            this.Controls.Add(this.gpbTaskInformation);
            this.Controls.Add(this.gbxConfig);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmWorkstation";
            this.Text = "FlexAs workstation";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbxConfig.ResumeLayout(false);
            this.gbxConfig.PerformLayout();
            this.gpbTaskInformation.ResumeLayout(false);
            this.gpbTaskInformation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cboWorkstation;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.TextBox txtIP;
        private System.Windows.Forms.GroupBox gbxConfig;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTaskId;
        private System.Windows.Forms.TextBox txtStepId;
        private System.Windows.Forms.TextBox txtInstructionId;
        private System.Windows.Forms.TextBox txtTextInstruction;
        private System.Windows.Forms.TextBox txtImagePath;
        private System.Windows.Forms.TextBox txtWorkstationMode;
        private System.Windows.Forms.TextBox txtPickingActions;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gpbTaskInformation;
        private System.Windows.Forms.Button btnTCPDisconnect;
        private System.Windows.Forms.Button btnStartOperatorView;
        private System.Windows.Forms.Button btnStopOperatorView;
        private System.Windows.Forms.Button btnCloseRDP;
    }
}

