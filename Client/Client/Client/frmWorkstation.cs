﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Client
{
    public partial class frmWorkstation : Form
    {

        private clsMainLogic _oMainLogic = new clsMainLogic();
        private frmOperatorView _frmOperatorView;

     

        public frmWorkstation()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnConnect.Click += btnConnect_Click;
            gbxConfig.Visible = true;
            Vis_TCPconnection();
            _oMainLogic.NewTaskEvent += CatchNewTask;
            txtIP.KeyPress += CheckIPbox;
            btnStartOperatorView.Enabled = false;
            btnStopOperatorView.Enabled = false;
            btnCloseRDP.Enabled = false;
        }

        private void CheckIPbox(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        public void Vis_TCPconnection()
        {
            bool x = (bool)_oMainLogic.CheckConnected();
            btnTCPDisconnect.Enabled = x;
            btnConnect.Enabled = !x;
            txtIP.Enabled = !x;
            cboWorkstation.Enabled = !x;
           
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            string s;
            if (cboWorkstation.SelectedIndex == -1) { return; }
            s = cboWorkstation.SelectedItem.ToString();
            this.Text = "FlexAs workstation: " + s;
            _oMainLogic.TCP_Connect(s, txtIP.Text);
            Vis_TCPconnection();

            btnStartOperatorView.Enabled = (bool)_oMainLogic.CheckConnected();
            btnCloseRDP.Enabled = (bool)_oMainLogic.CheckConnected();
        }

        private void btnTCPDisconnect_Click(object sender, EventArgs e)
        {
            _oMainLogic.TCP_Disconnect();
            Vis_TCPconnection();
        }

        private void CatchNewTask(clsTask o)
        {
            this.Invoke((MethodInvoker)delegate
            {
                txtTaskId.Text = o?.TaskId ?? ""; 
                txtStepId.Text = o?.StepId ?? "";
                txtInstructionId.Text = o?.InstructionId ?? "";
                txtTextInstruction.Text = o?.TextInstruction ?? "";
                txtImagePath.Text = o?.ImagePath ?? "";
                txtWorkstationMode.Text = o?.WorkstationMode ?? "";
                txtPickingActions.Text = o?.PickingActions ?? "";
            });
        }

        private void btnStartOperatorView_Click(object sender, EventArgs e)
        {
            // make way for operator view
            if (_frmOperatorView == null)
            {
                _frmOperatorView = new frmOperatorView(this, ref _oMainLogic);
                _frmOperatorView.Show();
                btnStartOperatorView.Enabled = false;
                btnStopOperatorView.Enabled = true;
                btnCloseRDP.Enabled = false;
            }
          
        }

        private void btnStopOperatorView_Click(object sender, EventArgs e)
        {
            _frmOperatorView.Close();
            this.ControlBox = true;
            btnStartOperatorView.Enabled = true;
            btnStopOperatorView.Enabled = false;
            btnCloseRDP.Enabled = true;
            _frmOperatorView = null;
        }


        [DllImport("kernel32.dll", SetLastError = true)] public static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);

        [DllImport("kernel32.dll", SetLastError = true)]  public static extern bool Wow64RevertWow64FsRedirection(IntPtr ptr);



        private void btnCloseRDP_Click(object sender, EventArgs e)
        {
            // eerste command om de sessie te vinden waarin we zitten
            IntPtr ptr = new IntPtr();
            Wow64DisableWow64FsRedirection(ref ptr);
            ProcessStartInfo info = new ProcessStartInfo(@"cmd.exe", "/k qwinsta | find \">\" & exit ");
            Process proc = new Process();
            proc.StartInfo = info;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            Wow64RevertWow64FsRedirection(ptr);

            // uit de resultaten de info halen
            string sCurrentID = "";
            string output = proc.StandardOutput.ReadToEnd();
            while (output.Contains("  ")){
                output = output.Replace("  ", " ");
            }
            string[] sparts = output.Split(' ' );
            for (int i= 0; i< sparts.Length; i++)
            {
                int n;
                if (int.TryParse(sparts[i], out n))
                {
                    sCurrentID = sparts[i];
                }
            }
            // run tweede commando om uit te loggen
            ptr = new IntPtr();
            Wow64DisableWow64FsRedirection(ref ptr);
            info = new ProcessStartInfo(@"cmd.exe", "/k tscon " + sCurrentID  + " /dest:console & exit ");
            proc = new Process();
            proc.StartInfo = info;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.CreateNoWindow = true;
            proc.Start();
            Wow64RevertWow64FsRedirection(ptr);



            // start de sessie
            btnStartOperatorView_Click(null, null);
        }



    }
}
