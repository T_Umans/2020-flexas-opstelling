﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.IO;

namespace Client
{
    public partial class frmOperatorView : Form
    {
        private clsMainLogic _oMainlogic;
        private Form _frmParent;
        private bool _xParentVisible = false;


        private TextBox txtInstruction = new TextBox();
        private TextBox txtPickingAction = new TextBox();
        private Panel pnlImage = new Panel();
        private TextBox txtCurrentState = new TextBox();


        public frmOperatorView( Form frmParent , ref clsMainLogic o)
        {
            InitializeComponent();
            _oMainlogic = o;
            _frmParent = frmParent;
            this.FormClosing += CheckClosing;       
        }

        private void frmOperatorView_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
           //todo this.TopMost = true;
            this.Shown += FormReady;
            this.Resize += FormReady; // should be as effecient as possible
            _oMainlogic.NewTaskEvent += CatchNewTask;
            _oMainlogic.KlemStatus += KlemStatusEvent;
            _oMainlogic.MeasurementStatus += CatchMeasurement;
            _oMainlogic.DubbeleHandbedieningactief += DubbeleHandbedieningactief_handler;
            _oMainlogic.DubbeleHandbedieningok += DubbeleHandbedieningok_handler;
            _oMainlogic.NextStepComming += NextStepComming_Handler;
        }

        private void btnOptions_Click(object sender, EventArgs e)
        {
            _xParentVisible = !_xParentVisible;
            _frmParent.WindowState = FormWindowState.Normal;
            _frmParent.ControlBox = !_xParentVisible;
            if (_xParentVisible)
            {
                btnOptions.BackColor = Color.Yellow;
                
            }
            else
            {
                this.TopMost = true;
                btnOptions.BackColor = SystemColors.Control;
                btnOptions.UseVisualStyleBackColor = true;
            }
            _frmParent.TopMost = _xParentVisible;
        }



        private void CheckClosing(object sender, FormClosingEventArgs e)
        {
            if (!_xParentVisible)
            {
                e.Cancel = true;
                MessageBox.Show("no fuck you \r\n\r\nbad user", "no close" );
            }
            else
            {
                _oMainlogic.NewTaskEvent -= CatchNewTask;
            }
        }

        private void FormReady(object sender, EventArgs e)
        {
            GenerateControls();
            CheckForTask();
            KlemStatusEvent(_oMainlogic.CurrentKlemStatus);
        }


        private void GenerateControls()
        {
            if (this.Controls.Contains(txtComment)) { this.Controls.Remove(txtComment); } // remove comment for designer 

            // add controls if it does not exist (so we can call this method multiple times without creating endless controls)
            if (!this.Controls.Contains(txtInstruction)) { this.Controls.Add(txtInstruction); }; 
            if (!this.Controls.Contains(pnlImage)) { this.Controls.Add(pnlImage); }; 
            if (!this.Controls.Contains(txtPickingAction)) { this.Controls.Add(txtPickingAction); }; 

            //calculations for dynamic form generation
            int iHeight, iWidth, iHeightoffset;
            int iBorderOffset = 10;
            iHeightoffset = 100;
            iHeight = this.Height - iHeightoffset;
            iWidth = this.Width;
            int a, b, c, d;
            a = iWidth * 3/5;   //      width of instruction =3/4*widthmax
            b = iWidth -a;      //      width of image is obiously what rests
            c = iHeight *2/3;  //      height is =1/2 ratio
            d = iHeight - c;
            // instruction
            txtInstruction.Multiline = true;
            txtInstruction.Location = new Point(iBorderOffset, iHeightoffset + iBorderOffset);
            txtInstruction.Size = new Size(a - 2 * iBorderOffset,c -2* iBorderOffset);
            // image
            pnlImage.Location = new Point(a + iBorderOffset, iHeightoffset + iBorderOffset);
            pnlImage.Size = new Size(b -  2 * iBorderOffset, iHeight -  2 * iBorderOffset);
            pnlImage.BackColor = Color.Gray;
            pnlImage.BackgroundImageLayout = ImageLayout.Zoom;
            // picking actions
            txtPickingAction.Multiline = true;
            txtPickingAction.Location = new Point(iBorderOffset, c + iHeightoffset + iBorderOffset);
            txtPickingAction.Size = new Size(a - 2 * iBorderOffset, d - 2 * iBorderOffset);     
            
            if (_oMainlogic._sCurrentWorkstation == "Perstafel")
            {
                this.Controls.Add(txtCurrentState);
                txtCurrentState.Location = new Point(100 + iBorderOffset, 15);
                txtCurrentState.Size = new Size(iWidth - 2 * iBorderOffset - 100, 85);
                txtCurrentState.Multiline = true;
                txtCurrentState.Font = new Font(txtCurrentState.Font, FontStyle.Bold);
            }


        }






        /// <summary>
        /// Needed when starting operatorview, because event is already raised (but only if connection is ok)
        /// </summary>
        private void CheckForTask()
        {
            if (_oMainlogic.CheckConnected() && _oMainlogic._oCurrentTask != null )
            {
                CatchNewTask(_oMainlogic._oCurrentTask);
            }
        }

        private void ScaleTextToMax(TextBox o, string s, int iOverwritesize =-1)
        {
            this.Invoke((MethodInvoker)delegate
            {
                if (s == null || s == "") { o.Text = ""; return; }

                Font KnownGood = o.Font;
                Font TestFont;
                if (iOverwritesize != -1)
                {
                    o.Font = new Font(o.Font.FontFamily, iOverwritesize);
                    o.Text = s;
                    return;
                }
                bool x = true;
                int i = 1;
                Size onewSize;
                while (x)
                {
                    TestFont = new Font(o.Font.FontFamily, i);
                    onewSize = TextRenderer.MeasureText(s, TestFont);
                    x = onewSize.Width < o.Size.Width && onewSize.Height < o.Size.Height;
                    if (x)
                    {
                        KnownGood = TestFont;
                    }
                    i = i + 10;
                }
                o.Font = KnownGood;
                o.Text = s;
            });
        }


        private void CatchNewTask(clsTask o)
        {
            this.Invoke((MethodInvoker)delegate
            {
                ScaleTextToMax(txtInstruction, o?.TextInstruction,36);
                ScaleTextToMax(txtPickingAction, o?.PickingActions,36);
                lblCurrentID.Text = "Step ID  : " + o?.StepId + "   Task ID : " + o?.TaskId;
                if (o?.TaskId == "-1")
                {
                    ScaleTextToMax(txtInstruction,"No more actions here. Move to next station", 36);
                }
                Thread t = new Thread(() => {
                    SetImageOnForm(pnlImage, o?.ImagePath);
                });
                t.Start();
            });

        }


        private void SetImageOnForm(Panel o, string sPath)
        {
            if (sPath == null | sPath == "")
            {
                sPath = @"\\172.16.30.100\FlexAs\Images\no_image.png";
            }
            try
            {
                if (Directory.Exists(@"\\172.16.30.100\FlexAs\Images"))
                {
                    o.Invoke((MethodInvoker)delegate { o.BackgroundImage = Image.FromFile(sPath); });
                }
                else
                {
                    o.Invoke((MethodInvoker)delegate { o.BackgroundImage = Properties.Resources.no_connection; });
                }
               
            }
            catch
            {
                o.Invoke((MethodInvoker)delegate { o.BackgroundImage = Properties.Resources.no_connection; });
                
            }
        }


        private void KlemStatusEvent(int x)
        {
            if (_oMainlogic._sCurrentWorkstation == "Perstafel")
            {
                this.Invoke((MethodInvoker)delegate
                {

                    if (x == 1) // klem is aan 
                    {
                        ScaleTextToMax(txtCurrentState, "Currently Active : Clamps");
                        txtCurrentState.BackColor = Color.Yellow;
                    }
                    else if (x == 2) // hydro is aan
                    {                        
                        ScaleTextToMax(txtCurrentState, "Currently Active : Hydraulics");
                        txtCurrentState.BackColor = Color.IndianRed;
                    }
                    else
                    {
                        ScaleTextToMax(txtCurrentState, "ADS not connected on Executor");
                    }
                
                });
            }
        }

      private void CatchMeasurement(int i)
        {
            this.Invoke((MethodInvoker)delegate
            {

                if (i == 1)
                {
                    ScaleTextToMax(txtCurrentState, "Measurement Successful");
                    txtCurrentState.BackColor = Color.Green;
                }
                else if (i == 2)
                {
                    ScaleTextToMax(txtCurrentState, "Measurement Failed");
                    txtCurrentState.BackColor = Color.Orange;
                }
                else
                {
                    ScaleTextToMax(txtCurrentState, "ADS not connected on Executor");
                }

            });
        }


        private void DubbeleHandbedieningactief_handler()
        {
            this.Invoke((MethodInvoker)delegate
            {
                ScaleTextToMax(txtCurrentState, "Double hand control active (keep active till finished)");
                txtCurrentState.BackColor = Color.LightBlue;
            });
        }

        private void DubbeleHandbedieningok_handler()
        {
            this.Invoke((MethodInvoker)delegate
            {
                ScaleTextToMax(txtCurrentState, "Action complete, release double hand control");
                txtCurrentState.BackColor = Color.GreenYellow;
            });
        }

        private void NextStepComming_Handler()
        {
            this.Invoke((MethodInvoker)delegate
            {
                if (txtPickingAction.Text != "")
                {
                    ScaleTextToMax(txtPickingAction, txtPickingAction.Text + "\r\nNext step imminent", 36);
                }
                
            });
        }

    }

}
