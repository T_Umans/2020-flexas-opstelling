﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace FlexAs_Scheduler
{
    class clsWorkstation
    {
        public bool xWaitingForReply { get; set; }
        private TcpClient _oTcpClient;

        public clsWorkstation(TcpClient oTcpClient) {
            _oTcpClient = oTcpClient;
        }

        public TcpClient oTcpClient { get { return _oTcpClient; } }
    }
}
