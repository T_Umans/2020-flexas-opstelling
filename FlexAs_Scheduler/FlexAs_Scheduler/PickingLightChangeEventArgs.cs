﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace FlexAs_Scheduler
{
    public class PickingLightChangeEventArgs : EventArgs
    {

        public bool xWasGreen { get; set; }
        public int iLightNumber { get; set; }

        public PickingLightChangeEventArgs(bool x , int i)
        {
            xWasGreen = x;
            iLightNumber = i;
        }
    }
}
