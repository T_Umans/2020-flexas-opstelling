﻿namespace FlexAs_Scheduler
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstTasks = new System.Windows.Forms.ListBox();
            this.tvExecuter = new System.Windows.Forms.TreeView();
            this.btnScheduleTask = new System.Windows.Forms.Button();
            this.btnNextInstruction = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkUseRobot = new System.Windows.Forms.CheckBox();
            this.chkUsePTL = new System.Windows.Forms.CheckBox();
            this.chkUseHIM = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtConnectionAP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStatusADS = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtConnectionPT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtConnectionVWV = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPreviousInstruction = new System.Windows.Forms.Button();
            this.gpbVerbinding = new System.Windows.Forms.GroupBox();
            this.txtAMSaddress = new System.Windows.Forms.TextBox();
            this.txtADSPort = new System.Windows.Forms.TextBox();
            this.btnADSdisconnect = new System.Windows.Forms.Button();
            this.btnADSconnect = new System.Windows.Forms.Button();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.gbxExecutor = new System.Windows.Forms.GroupBox();
            this.btnExcecutorStop = new System.Windows.Forms.Button();
            this.gbxScheduler = new System.Windows.Forms.GroupBox();
            this.gbxScheduleInfo = new System.Windows.Forms.GroupBox();
            this.lblScheduleState = new System.Windows.Forms.Label();
            this.cbxScheduleCobot = new System.Windows.Forms.CheckBox();
            this.cbxSchedulePick = new System.Windows.Forms.CheckBox();
            this.cbxScheduleHim = new System.Windows.Forms.CheckBox();
            this.lblSchedulePublishedDate = new System.Windows.Forms.Label();
            this.btnExecute = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.tvSchedule = new System.Windows.Forms.TreeView();
            this.gbxPerformance = new System.Windows.Forms.GroupBox();
            this.gbxPerformanceInfo = new System.Windows.Forms.GroupBox();
            this.lblPerformanceStopTime = new System.Windows.Forms.Label();
            this.lblPerformanceStartTime = new System.Windows.Forms.Label();
            this.lblPerformanceConsultations = new System.Windows.Forms.Label();
            this.lblPerformanceDuration = new System.Windows.Forms.Label();
            this.tvPerformance = new System.Windows.Forms.TreeView();
            this.txtStatusHim = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gpbVerbinding.SuspendLayout();
            this.gbxExecutor.SuspendLayout();
            this.gbxScheduler.SuspendLayout();
            this.gbxScheduleInfo.SuspendLayout();
            this.gbxPerformance.SuspendLayout();
            this.gbxPerformanceInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstTasks
            // 
            this.lstTasks.FormattingEnabled = true;
            this.lstTasks.Location = new System.Drawing.Point(6, 19);
            this.lstTasks.Name = "lstTasks";
            this.lstTasks.Size = new System.Drawing.Size(276, 134);
            this.lstTasks.TabIndex = 1;
            // 
            // tvExecuter
            // 
            this.tvExecuter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvExecuter.Location = new System.Drawing.Point(6, 22);
            this.tvExecuter.Name = "tvExecuter";
            this.tvExecuter.Size = new System.Drawing.Size(500, 604);
            this.tvExecuter.TabIndex = 2;
            // 
            // btnScheduleTask
            // 
            this.btnScheduleTask.Location = new System.Drawing.Point(6, 259);
            this.btnScheduleTask.Name = "btnScheduleTask";
            this.btnScheduleTask.Size = new System.Drawing.Size(276, 23);
            this.btnScheduleTask.TabIndex = 3;
            this.btnScheduleTask.Text = "Schedule";
            this.btnScheduleTask.UseVisualStyleBackColor = true;
            // 
            // btnNextInstruction
            // 
            this.btnNextInstruction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNextInstruction.Location = new System.Drawing.Point(256, 632);
            this.btnNextInstruction.Name = "btnNextInstruction";
            this.btnNextInstruction.Size = new System.Drawing.Size(250, 50);
            this.btnNextInstruction.TabIndex = 6;
            this.btnNextInstruction.Text = ">";
            this.btnNextInstruction.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstTasks);
            this.groupBox1.Controls.Add(this.btnScheduleTask);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Location = new System.Drawing.Point(6, 418);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 289);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add tasks";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkUseRobot);
            this.groupBox3.Controls.Add(this.chkUsePTL);
            this.groupBox3.Controls.Add(this.chkUseHIM);
            this.groupBox3.Location = new System.Drawing.Point(6, 159);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(276, 94);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Scenario configuration";
            // 
            // chkUseRobot
            // 
            this.chkUseRobot.AutoSize = true;
            this.chkUseRobot.Location = new System.Drawing.Point(6, 65);
            this.chkUseRobot.Name = "chkUseRobot";
            this.chkUseRobot.Size = new System.Drawing.Size(72, 17);
            this.chkUseRobot.TabIndex = 2;
            this.chkUseRobot.Text = "Use robot";
            this.chkUseRobot.UseVisualStyleBackColor = true;
            // 
            // chkUsePTL
            // 
            this.chkUsePTL.AutoSize = true;
            this.chkUsePTL.Checked = true;
            this.chkUsePTL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUsePTL.Location = new System.Drawing.Point(6, 42);
            this.chkUsePTL.Name = "chkUsePTL";
            this.chkUsePTL.Size = new System.Drawing.Size(103, 17);
            this.chkUsePTL.TabIndex = 1;
            this.chkUsePTL.Text = "Use Pick to light";
            this.chkUsePTL.UseVisualStyleBackColor = true;
            // 
            // chkUseHIM
            // 
            this.chkUseHIM.AutoSize = true;
            this.chkUseHIM.Checked = true;
            this.chkUseHIM.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUseHIM.Location = new System.Drawing.Point(6, 19);
            this.chkUseHIM.Name = "chkUseHIM";
            this.chkUseHIM.Size = new System.Drawing.Size(68, 17);
            this.chkUseHIM.TabIndex = 0;
            this.chkUseHIM.Text = "Use HIM";
            this.chkUseHIM.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtStatusHim);
            this.groupBox2.Controls.Add(this.txtConnectionAP);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtStatusADS);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtConnectionPT);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtConnectionVWV);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(1142, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(250, 147);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Connection status";
            // 
            // txtConnectionAP
            // 
            this.txtConnectionAP.BackColor = System.Drawing.Color.IndianRed;
            this.txtConnectionAP.Location = new System.Drawing.Point(82, 71);
            this.txtConnectionAP.Name = "txtConnectionAP";
            this.txtConnectionAP.ReadOnly = true;
            this.txtConnectionAP.Size = new System.Drawing.Size(160, 20);
            this.txtConnectionAP.TabIndex = 7;
            this.txtConnectionAP.Text = "Not connected";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Afwerkpost:";
            // 
            // txtStatusADS
            // 
            this.txtStatusADS.BackColor = System.Drawing.Color.IndianRed;
            this.txtStatusADS.Location = new System.Drawing.Point(82, 97);
            this.txtStatusADS.Name = "txtStatusADS";
            this.txtStatusADS.ReadOnly = true;
            this.txtStatusADS.Size = new System.Drawing.Size(160, 20);
            this.txtStatusADS.TabIndex = 5;
            this.txtStatusADS.Text = "Not connected";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "PLC : ";
            // 
            // txtConnectionPT
            // 
            this.txtConnectionPT.BackColor = System.Drawing.Color.IndianRed;
            this.txtConnectionPT.Location = new System.Drawing.Point(82, 45);
            this.txtConnectionPT.Name = "txtConnectionPT";
            this.txtConnectionPT.ReadOnly = true;
            this.txtConnectionPT.Size = new System.Drawing.Size(160, 20);
            this.txtConnectionPT.TabIndex = 3;
            this.txtConnectionPT.Text = "Not connected";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Perstafel:";
            // 
            // txtConnectionVWV
            // 
            this.txtConnectionVWV.BackColor = System.Drawing.Color.IndianRed;
            this.txtConnectionVWV.Location = new System.Drawing.Point(82, 19);
            this.txtConnectionVWV.Name = "txtConnectionVWV";
            this.txtConnectionVWV.ReadOnly = true;
            this.txtConnectionVWV.Size = new System.Drawing.Size(160, 20);
            this.txtConnectionVWV.TabIndex = 1;
            this.txtConnectionVWV.Text = "Not connected";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vrijwerkvlak: ";
            // 
            // btnPreviousInstruction
            // 
            this.btnPreviousInstruction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreviousInstruction.Location = new System.Drawing.Point(6, 632);
            this.btnPreviousInstruction.Name = "btnPreviousInstruction";
            this.btnPreviousInstruction.Size = new System.Drawing.Size(250, 50);
            this.btnPreviousInstruction.TabIndex = 9;
            this.btnPreviousInstruction.Text = "<";
            this.btnPreviousInstruction.UseVisualStyleBackColor = true;
            // 
            // gpbVerbinding
            // 
            this.gpbVerbinding.Controls.Add(this.txtAMSaddress);
            this.gpbVerbinding.Controls.Add(this.txtADSPort);
            this.gpbVerbinding.Controls.Add(this.btnADSdisconnect);
            this.gpbVerbinding.Controls.Add(this.btnADSconnect);
            this.gpbVerbinding.Controls.Add(this.Label8);
            this.gpbVerbinding.Controls.Add(this.Label10);
            this.gpbVerbinding.Location = new System.Drawing.Point(1142, 211);
            this.gpbVerbinding.Name = "gpbVerbinding";
            this.gpbVerbinding.Size = new System.Drawing.Size(250, 155);
            this.gpbVerbinding.TabIndex = 24;
            this.gpbVerbinding.TabStop = false;
            this.gpbVerbinding.Text = "ADS connection";
            // 
            // txtAMSaddress
            // 
            this.txtAMSaddress.Location = new System.Drawing.Point(94, 25);
            this.txtAMSaddress.Name = "txtAMSaddress";
            this.txtAMSaddress.Size = new System.Drawing.Size(127, 20);
            this.txtAMSaddress.TabIndex = 25;
            // 
            // txtADSPort
            // 
            this.txtADSPort.Location = new System.Drawing.Point(93, 52);
            this.txtADSPort.Name = "txtADSPort";
            this.txtADSPort.Size = new System.Drawing.Size(128, 20);
            this.txtADSPort.TabIndex = 4;
            // 
            // btnADSdisconnect
            // 
            this.btnADSdisconnect.Enabled = false;
            this.btnADSdisconnect.Location = new System.Drawing.Point(23, 115);
            this.btnADSdisconnect.Name = "btnADSdisconnect";
            this.btnADSdisconnect.Size = new System.Drawing.Size(198, 23);
            this.btnADSdisconnect.TabIndex = 23;
            this.btnADSdisconnect.Text = "Disconnect";
            this.btnADSdisconnect.UseVisualStyleBackColor = true;
            this.btnADSdisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnADSconnect
            // 
            this.btnADSconnect.Location = new System.Drawing.Point(23, 86);
            this.btnADSconnect.Name = "btnADSconnect";
            this.btnADSconnect.Size = new System.Drawing.Size(198, 23);
            this.btnADSconnect.TabIndex = 3;
            this.btnADSconnect.Text = "Connect";
            this.btnADSconnect.UseVisualStyleBackColor = true;
            this.btnADSconnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(54, 55);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(32, 13);
            this.Label8.TabIndex = 2;
            this.Label8.Text = "Port :";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(21, 28);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(66, 13);
            this.Label10.TabIndex = 1;
            this.Label10.Text = "AMS Adres :";
            // 
            // gbxExecutor
            // 
            this.gbxExecutor.Controls.Add(this.btnExcecutorStop);
            this.gbxExecutor.Controls.Add(this.tvExecuter);
            this.gbxExecutor.Controls.Add(this.btnNextInstruction);
            this.gbxExecutor.Controls.Add(this.btnPreviousInstruction);
            this.gbxExecutor.Location = new System.Drawing.Point(12, 12);
            this.gbxExecutor.MinimumSize = new System.Drawing.Size(512, 500);
            this.gbxExecutor.Name = "gbxExecutor";
            this.gbxExecutor.Size = new System.Drawing.Size(512, 715);
            this.gbxExecutor.TabIndex = 26;
            this.gbxExecutor.TabStop = false;
            this.gbxExecutor.Text = "Executor";
            // 
            // btnExcecutorStop
            // 
            this.btnExcecutorStop.BackColor = System.Drawing.Color.Red;
            this.btnExcecutorStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExcecutorStop.ForeColor = System.Drawing.Color.White;
            this.btnExcecutorStop.Location = new System.Drawing.Point(6, 686);
            this.btnExcecutorStop.Name = "btnExcecutorStop";
            this.btnExcecutorStop.Size = new System.Drawing.Size(500, 23);
            this.btnExcecutorStop.TabIndex = 26;
            this.btnExcecutorStop.Text = "STOP";
            this.btnExcecutorStop.UseVisualStyleBackColor = false;
            this.btnExcecutorStop.Click += new System.EventHandler(this.btnExcecutorStop_Click);
            // 
            // gbxScheduler
            // 
            this.gbxScheduler.Controls.Add(this.gbxScheduleInfo);
            this.gbxScheduler.Controls.Add(this.btnExecute);
            this.gbxScheduler.Controls.Add(this.btnRemove);
            this.gbxScheduler.Controls.Add(this.tvSchedule);
            this.gbxScheduler.Controls.Add(this.groupBox1);
            this.gbxScheduler.Location = new System.Drawing.Point(530, 12);
            this.gbxScheduler.Name = "gbxScheduler";
            this.gbxScheduler.Size = new System.Drawing.Size(300, 715);
            this.gbxScheduler.TabIndex = 27;
            this.gbxScheduler.TabStop = false;
            this.gbxScheduler.Text = "Scheduler";
            // 
            // gbxScheduleInfo
            // 
            this.gbxScheduleInfo.Controls.Add(this.lblScheduleState);
            this.gbxScheduleInfo.Controls.Add(this.cbxScheduleCobot);
            this.gbxScheduleInfo.Controls.Add(this.cbxSchedulePick);
            this.gbxScheduleInfo.Controls.Add(this.cbxScheduleHim);
            this.gbxScheduleInfo.Controls.Add(this.lblSchedulePublishedDate);
            this.gbxScheduleInfo.Location = new System.Drawing.Point(12, 208);
            this.gbxScheduleInfo.Name = "gbxScheduleInfo";
            this.gbxScheduleInfo.Size = new System.Drawing.Size(276, 146);
            this.gbxScheduleInfo.TabIndex = 11;
            this.gbxScheduleInfo.TabStop = false;
            this.gbxScheduleInfo.Text = "Info";
            // 
            // lblScheduleState
            // 
            this.lblScheduleState.AutoSize = true;
            this.lblScheduleState.Location = new System.Drawing.Point(6, 41);
            this.lblScheduleState.Name = "lblScheduleState";
            this.lblScheduleState.Size = new System.Drawing.Size(35, 13);
            this.lblScheduleState.TabIndex = 4;
            this.lblScheduleState.Text = "State:";
            // 
            // cbxScheduleCobot
            // 
            this.cbxScheduleCobot.AutoSize = true;
            this.cbxScheduleCobot.Enabled = false;
            this.cbxScheduleCobot.Location = new System.Drawing.Point(9, 123);
            this.cbxScheduleCobot.Name = "cbxScheduleCobot";
            this.cbxScheduleCobot.Size = new System.Drawing.Size(92, 17);
            this.cbxScheduleCobot.TabIndex = 3;
            this.cbxScheduleCobot.Text = "Cobot support";
            this.cbxScheduleCobot.UseVisualStyleBackColor = true;
            // 
            // cbxSchedulePick
            // 
            this.cbxSchedulePick.AutoSize = true;
            this.cbxSchedulePick.Enabled = false;
            this.cbxSchedulePick.Location = new System.Drawing.Point(9, 100);
            this.cbxSchedulePick.Name = "cbxSchedulePick";
            this.cbxSchedulePick.Size = new System.Drawing.Size(99, 17);
            this.cbxSchedulePick.TabIndex = 2;
            this.cbxSchedulePick.Text = "Picking support";
            this.cbxSchedulePick.UseVisualStyleBackColor = true;
            // 
            // cbxScheduleHim
            // 
            this.cbxScheduleHim.AutoSize = true;
            this.cbxScheduleHim.Enabled = false;
            this.cbxScheduleHim.Location = new System.Drawing.Point(9, 77);
            this.cbxScheduleHim.Name = "cbxScheduleHim";
            this.cbxScheduleHim.Size = new System.Drawing.Size(84, 17);
            this.cbxScheduleHim.TabIndex = 1;
            this.cbxScheduleHim.Text = "HIM support";
            this.cbxScheduleHim.UseVisualStyleBackColor = true;
            // 
            // lblSchedulePublishedDate
            // 
            this.lblSchedulePublishedDate.AutoSize = true;
            this.lblSchedulePublishedDate.Location = new System.Drawing.Point(6, 18);
            this.lblSchedulePublishedDate.Name = "lblSchedulePublishedDate";
            this.lblSchedulePublishedDate.Size = new System.Drawing.Size(80, 13);
            this.lblSchedulePublishedDate.TabIndex = 0;
            this.lblSchedulePublishedDate.Text = "Published date:";
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(12, 360);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(276, 23);
            this.btnExecute.TabIndex = 10;
            this.btnExecute.Text = "Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(12, 389);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(276, 23);
            this.btnRemove.TabIndex = 9;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // tvSchedule
            // 
            this.tvSchedule.FullRowSelect = true;
            this.tvSchedule.HideSelection = false;
            this.tvSchedule.Location = new System.Drawing.Point(12, 22);
            this.tvSchedule.Name = "tvSchedule";
            this.tvSchedule.Size = new System.Drawing.Size(276, 180);
            this.tvSchedule.TabIndex = 8;
            this.tvSchedule.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvSchedule_AfterSelect);
            // 
            // gbxPerformance
            // 
            this.gbxPerformance.Controls.Add(this.gbxPerformanceInfo);
            this.gbxPerformance.Controls.Add(this.tvPerformance);
            this.gbxPerformance.Location = new System.Drawing.Point(836, 12);
            this.gbxPerformance.Name = "gbxPerformance";
            this.gbxPerformance.Size = new System.Drawing.Size(300, 715);
            this.gbxPerformance.TabIndex = 28;
            this.gbxPerformance.TabStop = false;
            this.gbxPerformance.Text = "Performance";
            // 
            // gbxPerformanceInfo
            // 
            this.gbxPerformanceInfo.Controls.Add(this.lblPerformanceStopTime);
            this.gbxPerformanceInfo.Controls.Add(this.lblPerformanceStartTime);
            this.gbxPerformanceInfo.Controls.Add(this.lblPerformanceConsultations);
            this.gbxPerformanceInfo.Controls.Add(this.lblPerformanceDuration);
            this.gbxPerformanceInfo.Location = new System.Drawing.Point(6, 22);
            this.gbxPerformanceInfo.Name = "gbxPerformanceInfo";
            this.gbxPerformanceInfo.Size = new System.Drawing.Size(288, 95);
            this.gbxPerformanceInfo.TabIndex = 12;
            this.gbxPerformanceInfo.TabStop = false;
            this.gbxPerformanceInfo.Text = "Info";
            // 
            // lblPerformanceStopTime
            // 
            this.lblPerformanceStopTime.AutoSize = true;
            this.lblPerformanceStopTime.Location = new System.Drawing.Point(6, 39);
            this.lblPerformanceStopTime.Name = "lblPerformanceStopTime";
            this.lblPerformanceStopTime.Size = new System.Drawing.Size(57, 13);
            this.lblPerformanceStopTime.TabIndex = 6;
            this.lblPerformanceStopTime.Text = "Stop time: ";
            // 
            // lblPerformanceStartTime
            // 
            this.lblPerformanceStartTime.AutoSize = true;
            this.lblPerformanceStartTime.Location = new System.Drawing.Point(6, 23);
            this.lblPerformanceStartTime.Name = "lblPerformanceStartTime";
            this.lblPerformanceStartTime.Size = new System.Drawing.Size(57, 13);
            this.lblPerformanceStartTime.TabIndex = 5;
            this.lblPerformanceStartTime.Text = "Start time: ";
            // 
            // lblPerformanceConsultations
            // 
            this.lblPerformanceConsultations.AutoSize = true;
            this.lblPerformanceConsultations.Location = new System.Drawing.Point(6, 56);
            this.lblPerformanceConsultations.Name = "lblPerformanceConsultations";
            this.lblPerformanceConsultations.Size = new System.Drawing.Size(73, 13);
            this.lblPerformanceConsultations.TabIndex = 4;
            this.lblPerformanceConsultations.Text = "Consultations:";
            // 
            // lblPerformanceDuration
            // 
            this.lblPerformanceDuration.AutoSize = true;
            this.lblPerformanceDuration.Location = new System.Drawing.Point(6, 75);
            this.lblPerformanceDuration.Name = "lblPerformanceDuration";
            this.lblPerformanceDuration.Size = new System.Drawing.Size(50, 13);
            this.lblPerformanceDuration.TabIndex = 3;
            this.lblPerformanceDuration.Text = "Duration:";
            // 
            // tvPerformance
            // 
            this.tvPerformance.HideSelection = false;
            this.tvPerformance.Location = new System.Drawing.Point(6, 123);
            this.tvPerformance.Name = "tvPerformance";
            this.tvPerformance.Size = new System.Drawing.Size(288, 586);
            this.tvPerformance.TabIndex = 11;
            this.tvPerformance.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvPerformance_AfterSelect);
            // 
            // txtStatusHim
            // 
            this.txtStatusHim.BackColor = System.Drawing.Color.IndianRed;
            this.txtStatusHim.Location = new System.Drawing.Point(82, 121);
            this.txtStatusHim.Name = "txtStatusHim";
            this.txtStatusHim.ReadOnly = true;
            this.txtStatusHim.Size = new System.Drawing.Size(160, 20);
            this.txtStatusHim.TabIndex = 8;
            this.txtStatusHim.Text = "Not connected";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "HIM:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1438, 739);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gbxPerformance);
            this.Controls.Add(this.gbxScheduler);
            this.Controls.Add(this.gbxExecutor);
            this.Controls.Add(this.gpbVerbinding);
            this.Name = "frmMain";
            this.Text = "FlexAs Executer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gpbVerbinding.ResumeLayout(false);
            this.gpbVerbinding.PerformLayout();
            this.gbxExecutor.ResumeLayout(false);
            this.gbxScheduler.ResumeLayout(false);
            this.gbxScheduleInfo.ResumeLayout(false);
            this.gbxScheduleInfo.PerformLayout();
            this.gbxPerformance.ResumeLayout(false);
            this.gbxPerformanceInfo.ResumeLayout(false);
            this.gbxPerformanceInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox lstTasks;
        private System.Windows.Forms.TreeView tvExecuter;
        private System.Windows.Forms.Button btnScheduleTask;
        private System.Windows.Forms.Button btnNextInstruction;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtConnectionPT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtConnectionVWV;
        private System.Windows.Forms.Button btnPreviousInstruction;
        internal System.Windows.Forms.GroupBox gpbVerbinding;
        private System.Windows.Forms.TextBox txtAMSaddress;
        internal System.Windows.Forms.TextBox txtADSPort;
        internal System.Windows.Forms.Button btnADSdisconnect;
        internal System.Windows.Forms.Button btnADSconnect;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label10;
        private System.Windows.Forms.TextBox txtStatusADS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtConnectionAP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkUseHIM;
        private System.Windows.Forms.CheckBox chkUseRobot;
        private System.Windows.Forms.CheckBox chkUsePTL;
        private System.Windows.Forms.GroupBox gbxExecutor;
        private System.Windows.Forms.GroupBox gbxScheduler;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.TreeView tvSchedule;
        private System.Windows.Forms.GroupBox gbxPerformance;
        private System.Windows.Forms.GroupBox gbxPerformanceInfo;
        private System.Windows.Forms.TreeView tvPerformance;
        private System.Windows.Forms.GroupBox gbxScheduleInfo;
        private System.Windows.Forms.Label lblSchedulePublishedDate;
        private System.Windows.Forms.CheckBox cbxScheduleCobot;
        private System.Windows.Forms.CheckBox cbxSchedulePick;
        private System.Windows.Forms.CheckBox cbxScheduleHim;
        private System.Windows.Forms.Label lblScheduleState;
        private System.Windows.Forms.Button btnExcecutorStop;
        private System.Windows.Forms.Label lblPerformanceConsultations;
        private System.Windows.Forms.Label lblPerformanceDuration;
        private System.Windows.Forms.Label lblPerformanceStopTime;
        private System.Windows.Forms.Label lblPerformanceStartTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStatusHim;
    }
}

