﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using ISyE.AssemblyInformation.MongoDB;
using ISyE.AssemblyInformation.ISA95;
using ISyE.AssemblyInformation.LogObjects;
using HIM_Communication;

namespace FlexAs_Scheduler
{
    public partial class frmMain : Form
    {
        //Communicatie
        private TCPS.clsTCPServer _oTcpServer;
        private MongoDAL _oMongoDal = new MongoDAL("mongodb://FlexAs:FlexAs@172.16.220.101:27017", "FlexAs");
        //private MongoDAL _oMongoDal = new MongoDAL("mongodb://FlexAs:FlexAs@127.0.0.1:27017", "FlexAs");
        private clsHimCommunication _oHim = new clsHimCommunication();
        //Executor
        private int _iCurrentStepIndex = -1;
        private int _iCurrentInstructionIndex = -1;
        private List<JobResponse> _oCurrentJobResponseList = null;
        private bool _xTaskRunning = false;
        //Work stations
        private clsWorkstation _oVrijWerkVlak;
        private clsWorkstation _oPerstafel;
        private clsWorkstation _oAfwerkpost;

        private bool _xDisableButtons = true;



        private List<MaterialDefinition> _oMaterialDefinitionList;

        // ADS interface
        private clsADSInterface _oADSInterface = new clsADSInterface();

        public frmMain()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //ADS visual aid
            GatherInfo();
            _oADSInterface.ConnectionActionDone += CatchConnectionEvent;
            txtAMSaddress.KeyPress += CheckIPbox;
            txtADSPort.KeyPress += CheckPortbox;

            btnScheduleTask.Click += btnScheduleTask_Click;
            btnNextInstruction.Click += btnNextInstruction_Click;
            btnPreviousInstruction.Click += btnPreviousInstruction_Click;
            _oADSInterface.NextStep += oAdsInterface_NextInstruction;
            _oADSInterface.PreviousStep += oAdsInterface_PreviousInstruction;
            _oADSInterface.PickingLightChange += oAdsInterface_PickingLightChange;
            _oADSInterface.KlemStatusChange += oAdsInterface_KlemStatus;
            _oADSInterface.MeasurementIsDone += oAdsInterface_MeasurementIsDone;
            _oADSInterface.DubbeleHandbedieningActive += oAdsInterface_DubbeleHandbedieningActive;
            _oADSInterface.DubbeleHandbedieningOK += oAdsInterface_DubbeleHandbedieningOK;
            _oADSInterface.NextStepComming += oAdsInterface_NextStepComming;

            //Add tcp server
            _oTcpServer = new TCPS.clsTCPServer("0.0.0.0", 100, 0);
            _oTcpServer.ClientDisconnected += _oTcpServer_Disconnected;
            _oTcpServer.NewClient += _oTcpServer_NewClient;
            _oTcpServer.NewMessage += _oTcpServer_NewMessage;

            //Add HIM communication
            _oHim.PickToLightRequest += HimPickRequest;
            _oHim.ProcessFinished += HimProcessFinished;
            _oHim.StepChanged += HimStepChanged;

            if (_oHim.Available)
            {
                txtStatusHim.Text = "Connected";
                txtStatusHim.BackColor = Color.LightGreen;
            }

            //Load material definitions
            _oMaterialDefinitionList = _oMongoDal.GetMaterialDefinitions();

            UpdateWorkMasterListbox();
            LoadScheduledTasks();
            LoadPerformanceTasks();

            //Controls
            SetExecutorControls(false);

        }

        #region Schedule
        private TreeNode ScheduleTask(WorkMaster oTask, bool xHimSupport = true, bool xPickingSupport = true, bool xCobotSupport = false, string sJobOrder = null)
        {
            if (sJobOrder == null) sJobOrder = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            var oTaskDirective = oTask.GetWorkDirective(sJobOrder);
            oTaskDirective.HimSupport = xHimSupport;
            oTaskDirective.PickingSupport = xPickingSupport;
            oTaskDirective.CobotSupport = xCobotSupport;
            var oStepDirectiveList = GetStepDirectives(oTaskDirective,true);
            var oTaskJobResponse = oTaskDirective.GetJobResponse();
            oTaskJobResponse.JobResponseId = new List<string>();
            var oStepJobResponseList = new List<JobResponse>();
            var oInstructionJobResponseList = new List<JobResponse>();
            TreeNode oTNTask = new TreeNode(oTaskDirective.WorkMasterId);
            oTNTask.Tag = oTaskDirective;
            //go over all the steps of the given task
            foreach (var oStepDirective in oStepDirectiveList)
            {
                oStepDirective.HimSupport = xHimSupport;
                oStepDirective.PickingSupport = xPickingSupport;
                oStepDirective.CobotSupport = xCobotSupport;
                var oStepResponse = oStepDirective.GetJobResponse();
                oStepResponse.JobResponseId = new List<string>();
                oStepJobResponseList.Add(oStepResponse);
                oTaskJobResponse.JobResponseId.Add(oStepResponse.Id);
                TreeNode oTNStep = new TreeNode(oStepDirective.WorkMasterId);
                oTNStep.Tag = oStepDirective;
                oTNTask.Nodes.Add(oTNStep);
                //go over all the nodes of the given step
                var oInstructionList = GetInstructions(oStepDirective);
                if (oInstructionList == null) continue;
                foreach (var oNode in oInstructionList)
                {
                    var oInstructionResponse = new JobResponse()
                    {
                        Id = oStepDirective.Id + "_" + oNode.Id,
                        JobOrderId = sJobOrder,
                        WorkDirectiveId = oStepDirective.Id,
                        WorkMasterId = oStepDirective.WorkMasterId,
                        WorkflowSpecificationId = oStepDirective.WorkflowSpecification?.First().Id,
                        NodeId = oNode.Id,
                        JobState = WorkState.Ready,
                        ResponseType = "INSTRUCTION"
                    };
                    oInstructionJobResponseList.Add(oInstructionResponse);
                    oStepResponse.JobResponseId.Add(oInstructionResponse.Id);
                    TreeNode oTNNode = new TreeNode(oNode.Id) { Tag = oNode };
                    oTNStep.Nodes.Add(oTNNode);
                    //read the instruction text
                    if (!string.IsNullOrEmpty(oNode.TextInstruction))
                    {
                        oTNNode.Nodes.Add(new TreeNode(oNode.TextInstruction) { Tag = oNode.TextInstruction });
                    }
                    //see if there are picking actions
                    List<PickingAction> oPickingActions = oNode.PickingActions;
                    if (oPickingActions != null && oPickingActions.Count > 0)
                    {
                        foreach (PickingAction oPickingAction in oPickingActions)
                        {
                            TreeNode oTNPickintAction = new TreeNode("Picking action") { Tag = oPickingAction };
                            oTNNode.Nodes.Add(oTNPickintAction);
                            if (!string.IsNullOrEmpty(oPickingAction.MaterialDefinitionId))
                            {
                                oTNPickintAction.Nodes.Add(new TreeNode(oPickingAction.MaterialDefinitionId) { Tag = oPickingAction.MaterialDefinitionId });
                                oTNPickintAction.Nodes.Add(new TreeNode(oPickingAction.Quantity.ToString()) { Tag = oPickingAction.Quantity });
                            }
                        }
                    }
                }
            }

            //Write to MongoDB
            _oMongoDal.InsertWorkDirective(oTaskDirective);
            _oMongoDal.InsertWorkDirective(oStepDirectiveList.ToList());
            _oMongoDal.InsertJobResponse(oTaskJobResponse);
            _oMongoDal.InsertJobResponse(oStepJobResponseList);
            _oMongoDal.InsertJobResponse(oInstructionJobResponseList);
       
            return oTNTask;
        }

        private WorkDirective[] GetStepDirectives(WorkDirective oTaskDirective, bool xGenerate = false)
        {
            var oWorkDirectiveList = new List<WorkDirective>();
            var oNodes = oTaskDirective.WorkflowSpecification?.First().Node;
            if (oNodes == null) return oWorkDirectiveList.ToArray();
            foreach (var oNode in oNodes.Where(n => n.NodeType.Equals("STEP")))
            {
                //Generate work directive
                if (xGenerate && !string.IsNullOrEmpty(oNode.WorkMasterId))
                {
                    oWorkDirectiveList.Add(_oMongoDal.GetWorkMaster(oNode.WorkMasterId).GetWorkDirective(oTaskDirective.JobOrderId));
                    oNode.WorkDirectiveId = oWorkDirectiveList.Last().Id;
                }
                //Load work directive
                if (!xGenerate && !string.IsNullOrEmpty(oNode.WorkDirectiveId))
                {
                    var oWD = _oMongoDal.GetWorkDirective(oNode.WorkDirectiveId);
                    if (oWD == null) oWD = _oMongoDal.GetWorkMaster(oNode.WorkMasterId).GetWorkDirective(oTaskDirective.JobOrderId);
                    oWorkDirectiveList.Add(oWD);
               }
            }
            return oWorkDirectiveList.ToArray();
        }

        private Node[] GetInstructions(WorkDirective oStepDirective)
        {
            return oStepDirective.WorkflowSpecification?.First().Node?.Where(n => n.NodeType.Equals("INSTRUCTION")).ToArray();
        }

        private void ClearScheduleInfo()
        {
            lblSchedulePublishedDate.Text = "Published date: ";
            lblScheduleState.Text = "State: ";
            cbxScheduleHim.Checked = false;
            cbxSchedulePick.Checked = false;
            cbxScheduleCobot.Checked = false;
        }
        private void UpdateScheduleInfo(WorkDirective oWorkDirective)
        {
            ClearScheduleInfo();
            if (oWorkDirective == null) return;
            var oJobResponse = _oMongoDal.GetTaskJobResponse(oWorkDirective);
            lblSchedulePublishedDate.Text += oWorkDirective.PublishedDate.ToLocalTime();
            lblScheduleState.Text += oJobResponse?.OrderByDescending(jr => jr.StartTime)?.First().JobState;
            cbxScheduleHim.Checked = oWorkDirective.HimSupport;
            cbxSchedulePick.Checked = oWorkDirective.PickingSupport;
            cbxScheduleCobot.Checked = oWorkDirective.CobotSupport;
        }
        private void LoadScheduledTasks()
        {
            tvSchedule.Nodes.Clear();
            var oReadyJobResponses = _oMongoDal.GetJobResponses(WorkState.Ready)?.Where(jr => jr.ResponseType == "TASK")?.ToList();
            var oRunningJobResponses = _oMongoDal.GetJobResponses(WorkState.Running)?.Where(jr => jr.ResponseType == "TASK")?.ToList();
            var oJobResponses = new List<JobResponse>();
            if (oRunningJobResponses != null) oJobResponses.AddRange(oRunningJobResponses);
            if (oReadyJobResponses != null) oJobResponses.AddRange(oReadyJobResponses);
            if (oJobResponses.Count == 0) return;
            var oWorkDirectives = new List<WorkDirective>();
            foreach (var oJR in oJobResponses.GroupBy(jr => jr.WorkDirectiveId).Select(g => g.First()))
            {
                var oWD = _oMongoDal.GetWorkDirective(oJR.WorkDirectiveId);
                if (oWD != null) oWorkDirectives.Add(oWD);
            }
            oWorkDirectives = oWorkDirectives?.OrderBy(wd => wd.PublishedDate).ToList();
            foreach (var oWD in oWorkDirectives)
            {
                tvSchedule.Nodes.Add(GetWorkDirectiveTreenode(oWD));
            }
        }
        private TreeNode GetWorkDirectiveTreenode(WorkDirective oTaskDirective)
        {
            TreeNode oTask = new TreeNode(oTaskDirective.WorkMasterId);
            oTask.Tag = oTaskDirective;
            //go over all the steps of the given task
            foreach (var oStepDirective in GetStepDirectives(oTaskDirective))
            {
                TreeNode oStep = new TreeNode(oStepDirective.WorkMasterId);
                oStep.Tag = oStepDirective;
                oTask.Nodes.Add(oStep);
                //go over all the nodes of the given step
                var oInstructionList = GetInstructions(oStepDirective);
                if (oInstructionList == null) continue;
                foreach (var oNode in oInstructionList)
                {
                    TreeNode oInstruction = new TreeNode(oNode.Id) { Tag = oNode };
                    oStep.Nodes.Add(oInstruction);
                    //read the instruction text
                    if (!string.IsNullOrEmpty(oNode.TextInstruction))
                    {
                        oInstruction.Nodes.Add(new TreeNode(oNode.TextInstruction) { Tag = oNode.TextInstruction });
                    }
                    //see if there are picking actions
                    List<PickingAction> oPickingActions = oNode.PickingActions;
                    if (oPickingActions != null && oPickingActions.Count > 0)
                    {
                        foreach (PickingAction oPickingAction in oPickingActions)
                        {
                            TreeNode oTNPickintAction = new TreeNode("Picking action") { Tag = oPickingAction };
                            oInstruction.Nodes.Add(oTNPickintAction);
                            if (!string.IsNullOrEmpty(oPickingAction.MaterialDefinitionId))
                            {
                                oTNPickintAction.Nodes.Add(new TreeNode(oPickingAction.MaterialDefinitionId) { Tag = oPickingAction.MaterialDefinitionId });
                                oTNPickintAction.Nodes.Add(new TreeNode(oPickingAction.Quantity.ToString()) { Tag = oPickingAction.Quantity });
                            }
                        }
                    }
                }
            }
            return oTask;
        }

        private void RemoveScheduledTask(TreeNode oTask)
        {
            //Get root work directive
            while (oTask.Parent != null)
            {
                oTask = oTask.Parent;
            }
            //Remove from excecuter
            if (tvExecuter.Nodes.Count > 0 && TASK != null)
            {
                WorkDirective oRemovedTask = (WorkDirective)oTask.Tag;
                if (TASK_DIRECTIVE == oRemovedTask) StopCurrentTask();
            }
            //Remove task work directive & job response
            _oMongoDal.DeleteWorkDirective((WorkDirective)oTask.Tag);
            _oMongoDal.DeleteJobResponse((WorkDirective)oTask.Tag);

            //Remove step work directives & job responses
            foreach (TreeNode oStep in oTask.Nodes)
            {
                _oMongoDal.DeleteWorkDirective((WorkDirective)oStep.Tag);
                _oMongoDal.DeleteJobResponse((WorkDirective)oStep.Tag);
            }
            LoadScheduledTasks();
        }
        private void UpdateWorkMasterListbox()
        {
            lstTasks.Items.Clear();
            List<WorkMaster> oWorkMasters = _oMongoDal.GetWorkMasters();
            if (oWorkMasters == null) { return; }
            lstTasks.DataSource = _oMongoDal.GetWorkMasters().Where(wm => wm.WorkDefinitionType == "TASK").ToList();
        }

        #endregion

        #region Performane
        private void LoadPerformanceTasks()
        {
            tvPerformance.Nodes.Clear();
            var oJobResponses = _oMongoDal.GetJobResponses()?
                .Where(jr => jr.ResponseType == "TASK")?
                .OrderByDescending(jr => jr.StartTime)
                .GroupBy(jr => jr.WorkDirectiveId)
                .ToList();
            if (oJobResponses == null) return;
            if (oJobResponses.Count == 0) return;
            var oWorkDirectives = new List<WorkDirective>();
            foreach (var oWDG in oJobResponses)
            {
                if (oWDG.Where(jr => jr.JobState != WorkState.Completed).Count()>0) continue;
                var oWD = _oMongoDal.GetWorkDirective(oWDG.Key);
                if (oWD != null) oWorkDirectives.Add(oWD);
            }
            oWorkDirectives = oWorkDirectives?.OrderBy(wd => wd.PublishedDate).ToList();
            foreach (var oWD in oWorkDirectives)
            {
                tvPerformance.Nodes.Add(GetWorkDirectiveTreenode(oWD));
            }
        }

        private void ClearPerformanceInfo()
        {
            lblPerformanceStartTime.Text = "Start time: ";
            lblPerformanceStopTime.Text = "Stop time: ";
            lblPerformanceConsultations.Text = "Consultations: ";
            lblPerformanceDuration.Text = "Duration: ";
        }

        private void UpdatePerformanceInfo(TreeNode oNode)
        {
            ClearPerformanceInfo();
            if (oNode.Level == 0)
            {
                var oTaskDirective = (WorkDirective) oNode.Tag;
                var oJobResponses = _oMongoDal.GetTaskJobResponse(oTaskDirective);
                if (oJobResponses == null) return;
                if (oJobResponses.Count == 0) return;
                oJobResponses = oJobResponses.OrderBy(jr => jr.StartTime).ToList();
                lblPerformanceStartTime.Text += oJobResponses.First().StartTime.ToLocalTime();
                lblPerformanceStopTime.Text += oJobResponses.Last().EndTime.ToLocalTime();
                lblPerformanceConsultations.Text += oJobResponses.Count;
                double dDuration = 0.0;
                foreach (var oJR in oJobResponses)
                {
                    dDuration += (oJR.EndTime - oJR.StartTime).TotalSeconds;
                }
                lblPerformanceDuration.Text += dDuration + " s";
                return;
            }
            if (oNode.Level == 1)
            {
                var oStepDirective = (WorkDirective)oNode.Tag;
                var oJobResponses = _oMongoDal.GetStepJobResponse(oStepDirective);
                if (oJobResponses == null) return;
                if (oJobResponses.Count == 0) return;
                oJobResponses = oJobResponses.OrderBy(jr => jr.StartTime).ToList();
                lblPerformanceStartTime.Text += oJobResponses.First().StartTime.ToLocalTime();
                lblPerformanceStopTime.Text += oJobResponses.Last().EndTime.ToLocalTime();
                lblPerformanceConsultations.Text += oJobResponses.Count;
                double dDuration = 0.0;
                foreach (var oJR in oJobResponses)
                {
                    dDuration += (oJR.EndTime - oJR.StartTime).TotalSeconds;
                }
                lblPerformanceDuration.Text += dDuration + " s";
                return;
            }
            if (oNode.Level == 2)
            {
                var oStepDirective = (WorkDirective)oNode.Parent.Tag;
                var oNodePerformance = (Node)oNode.Tag;
                var oJobResponses = _oMongoDal.GetInstructionJobResponses(oStepDirective, oNodePerformance);
                if (oJobResponses == null) return;
                if (oJobResponses.Count == 0) return;
                oJobResponses = oJobResponses.OrderBy(jr => jr.StartTime).ToList();
                lblPerformanceStartTime.Text += oJobResponses.First().StartTime.ToLocalTime();
                lblPerformanceStopTime.Text += oJobResponses.Last().EndTime.ToLocalTime();
                lblPerformanceConsultations.Text += oJobResponses.Count;
                double dDuration = 0.0;
                foreach (var oJR in oJobResponses)
                {
                    dDuration += (oJR.EndTime - oJR.StartTime).TotalSeconds;
                }
                lblPerformanceDuration.Text += dDuration + " s";
                return;
            }
        }
        #endregion

        #region Executor
        #region Executor properties
        /// <summary>
        /// Current executed task treenode.
        /// </summary>
        private TreeNode TASK
        {
            get
            {
                if (tvExecuter.Nodes.Count == 0) return null;
                return tvExecuter.Nodes[0];
            }
        }

        /// <summary>
        /// Current executed task work directive.
        /// </summary>
        private WorkDirective TASK_DIRECTIVE
        {
            get
            {
                if (TASK == null) return null;
                if (TASK.Tag == null) return null;
                return (WorkDirective)TASK.Tag;
            }
        }

        /// <summary>
        /// Get last task job response.
        /// </summary>
        private JobResponse TASK_RESPONSE
        {
            get
            {
                if (TASK == null) return null;
                return _oCurrentJobResponseList
                    .Where(jr => jr.ResponseType == "TASK")
                    .Where(jr => jr.WorkDirectiveId == TASK_DIRECTIVE.Id)
                    .OrderByDescending(jr => jr.StartTime)
                    .FirstOrDefault();
            }
        }

        /// <summary>
        /// Maximum step index.
        /// </summary>
        private int MAX_STEP_INDEX
        {
            get
            {
                if (TASK == null) return -1;
                if (TASK.Nodes == null) return -1;
                return TASK.Nodes.Count - 1;
            }
        }

        /// <summary>
        /// Current executed step treenode.
        /// </summary>
        private TreeNode STEP
        {
            get
            {
                if (TASK == null) return null;
                if (TASK.Nodes == null) return null;
                if (_iCurrentStepIndex <= -1) return null;
                if (_iCurrentStepIndex > MAX_STEP_INDEX) return null;
                return TASK.Nodes[_iCurrentStepIndex];
            }
        }

        /// <summary>
        /// Current executed step work directive.
        /// </summary>
        private WorkDirective STEP_DIRECTIVE
        {
            get
            {
                if (STEP == null) return null;
                if (STEP.Tag == null) return null;
                return (WorkDirective)STEP.Tag;
            }
        }

        /// <summary>
        /// Get last step job response.
        /// </summary>
        private JobResponse STEP_RESPONSE
        {
            get
            {
                if (STEP == null) return null;
                return _oCurrentJobResponseList
                    .Where(jr => jr.ResponseType == "STEP")
                    .Where(jr => jr.WorkDirectiveId == STEP_DIRECTIVE.Id)
                    .OrderByDescending(jr => jr.StartTime)
                    .FirstOrDefault();
            }
        }

        /// <summary>
        /// Maximum instruction index.
        /// </summary>
        private int MAX_INSTRUCTION_INDEX
        {
            get
            {
                if (STEP == null) return -1;
                if (STEP.Nodes == null) return -1;
                return STEP.Nodes.Count - 1;
            }
        }

        /// <summary>
        /// Current executed instruction treenode.
        /// </summary>
        private TreeNode INSTRUCTION
        {
            get
            {
                if (STEP == null) return null;
                if (_iCurrentInstructionIndex <= -1) return null;
                if (_iCurrentInstructionIndex > MAX_INSTRUCTION_INDEX) return null;
                return STEP.Nodes[_iCurrentInstructionIndex];
            }
        }

        /// <summary>
        /// Current executed instruction workflow node.
        /// </summary>
        private Node INSTRUCTION_NODE
        {
            get
            {
                if (INSTRUCTION == null) return null;
                if (INSTRUCTION.Tag == null) return null;
                return (Node)INSTRUCTION.Tag;
            }
        }

        /// <summary>
        /// Get last instruction job response.
        /// </summary>
        private JobResponse INSTRUCTION_RESPONSE
        {
            get
            {
                if (STEP == null) return null;
                if (INSTRUCTION == null) return null;
                return _oCurrentJobResponseList
                    .Where(jr => jr.ResponseType == "INSTRUCTION")
                    .Where(jr => jr.WorkDirectiveId == STEP_DIRECTIVE.Id)
                    .Where(jr => jr.NodeId == INSTRUCTION_NODE.Id)
                    .OrderByDescending(jr => jr.StartTime)
                    .FirstOrDefault();
            }
        }
        /// <summary>
        /// Get current step workstation.
        /// </summary>
        private clsWorkstation CURRENT_WORKSTATION
        {
            get
            {
                if (STEP == null) return null;
                switch (STEP_DIRECTIVE?.HierarchyScope)
                {
                    case "VrijWerkvlak": return _oVrijWerkVlak;
                    case "Perstafel": return _oPerstafel;
                    case "Afwerkpost": return _oAfwerkpost;
                    default: return null;
                }
            }
        }

        #endregion

        private void SetExecutorControls(bool Enable)
        {
            this.Invoke((MethodInvoker)delegate
            {
                _xDisableButtons = !Enable;
                btnNextInstruction.Enabled = Enable;
                btnPreviousInstruction.Enabled = Enable;
                btnExcecutorStop.Enabled = Enable;
                btnExecute.Enabled = _xTaskRunning ? false : !Enable;
            });
        }

        #region Browse instructions
        /// <summary>
        /// Get next instruction from task.
        /// </summary>
        private void NextInstruction()
        {
            //Exit if there are nodes in executor
            if (TASK == null) return;
            //Check task response status
            if (!_xTaskRunning)
            {
                if (TASK_RESPONSE.JobState == WorkState.Running)
                {
                    StartJobResponse(TASK_RESPONSE);
                    StartJobResponse(STEP_RESPONSE);
                    StartJobResponse(INSTRUCTION_RESPONSE);
                    _xTaskRunning = true;
                    return;
                }
                else
                {
                    StartJobResponse(TASK_RESPONSE);
                    _xTaskRunning = true;
                }

            }
            //Update job response (store end time in case of a crash)
            else
            {
                UpdateJobResponse(TASK_RESPONSE);
                UpdateJobResponse(STEP_RESPONSE);
            }
            //Disable buttons
            SetExecutorControls(false);
            //Reset execute treeview highlight
            HighlightNode(Color.White);
            //Load instruction
            if (INSTRUCTION == null)
            {
                //Get next step
                GetStep();
                if (STEP != null) GetInstruction();
                else
                {
                    //END TASK
                    StopCurrentTask();
                    return;
                }
            }
            //Load next instruction
            else
            {
                GetInstruction();
                if (INSTRUCTION == null)
                {
                    //Repeat for next step
                    NextInstruction();
                }
            }
            HighlightNode(Color.LightBlue);
            //Enable buttons
            SetExecutorControls(true);

        }
        /// <summary>
        /// Get previous instruction from task.
        /// </summary>
        private void PreviousInstruction()
        {
            // alle picking lights disablen
            _oADSInterface.ResetLights();

            //Exit if there are no nodes in executor
            if (TASK == null) return;
            if (_iCurrentStepIndex == 0 && _iCurrentInstructionIndex == 0) return;
            //Disable buttons
            SetExecutorControls(false);
            //Reset execute treeview highlight
            HighlightNode(Color.White);
            //Update job response (store end time in case of a crash)
            UpdateJobResponse(TASK_RESPONSE);
            UpdateJobResponse(STEP_RESPONSE);
            //Load previous instruction
            GetInstruction(true);
            if (INSTRUCTION == null)
            {
                //Load previous step
                GetStep(true);
                if (STEP != null) GetInstruction(true);
            }
            HighlightNode(Color.LightBlue);

            // twijfelachtig of dit werkt
            if (TASK_DIRECTIVE.PickingSupport)
            {
                List<int> oPickToLights = new List<int>();
                if (INSTRUCTION_NODE?.PickingActions?.Count > 0)
                {
                    foreach (PickingAction oPickingAction in INSTRUCTION_NODE.PickingActions)
                    {
                        int iPickingLocation = _oMaterialDefinitionList.Where(md => md.Id == oPickingAction.MaterialDefinitionId).Select(md => md.PickingLocation).FirstOrDefault();
                        if (iPickingLocation > 0) { oPickToLights.Add(iPickingLocation); }
                    }
                }
                _oADSInterface.SetPickToLight(oPickToLights.ToArray());
            }


            //Enable buttons
            SetExecutorControls(true);
        }
        /// <summary>
        /// Get next or previous step from task.
        /// </summary>
        /// <param name="xPrevious">Previous direction.</param>
        private void GetStep(bool xPrevious = false)
        {
            //Stop running step job response
            StopJobResponse(STEP_RESPONSE);
            //Check if task is loaded in executor
            if (TASK == null) return;
            //Store current workstation
            var oPreviousWorkstation = CURRENT_WORKSTATION;

            if (_iCurrentStepIndex == -1)
            {
                if (!xPrevious) _iCurrentStepIndex = 0;
                else _iCurrentStepIndex = MAX_STEP_INDEX;
            }
            else
            {
                if (!xPrevious) _iCurrentStepIndex += 1;
                else _iCurrentStepIndex -= 1;
            }

            //If no step is selected, reset step index.
            if (STEP == null) _iCurrentStepIndex = -1;

            //Start new step job response
            StartJobResponse(STEP_RESPONSE);

            //Check if previous workstation needs to be cleared
            if (oPreviousWorkstation != null && oPreviousWorkstation != CURRENT_WORKSTATION) ClearClient(oPreviousWorkstation);
        }
        /// <summary>
        /// Get next or previous instruction from step.
        /// </summary>
        /// <param name="xPrevious">Previous direction.</param>
        private void GetInstruction(bool xPrevious = false)
        {
            //Close current instruction response
            if (INSTRUCTION_RESPONSE != null) StopJobResponse(INSTRUCTION_RESPONSE);
            if (_iCurrentInstructionIndex == -1)
            {
                if (!xPrevious) _iCurrentInstructionIndex = 0;
                else _iCurrentInstructionIndex = MAX_INSTRUCTION_INDEX;
            }
            else
            {
                if (!xPrevious)
                {
                    _iCurrentInstructionIndex += 1;
                }
                else
                {
                    _iCurrentInstructionIndex -= 1;
                }
            }

            //If no instruction is selected, reset instruction index.
            if (INSTRUCTION == null) _iCurrentInstructionIndex = -1;

            //Start new job response
            StartJobResponse(INSTRUCTION_RESPONSE);

            //Update client
            UpdateClient();

        }
        #endregion

        /// <summary>
        /// Execute a scheduled task.
        /// </summary>
        /// <param name="oTask">The scheduled task.</param>
        private void ExecuteTask(WorkDirective oTask)
        {
            //Check if no task is running
            if (TASK != null) 
            {
                MessageBox.Show("A task is already running.", "Execute task", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return; 
            }
            //Check if HIM is available
            if (oTask.HimSupport && !_oHim.Available)
            {
                MessageBox.Show("The HIM is not accessible!", "HIM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            //Reset HIM
            if (oTask.HimSupport && _oHim.Available)
            {
                _oHim.StartProcess("RESET");
            }
            //Get new treenode from task
            var oTaskTreeNode = GetWorkDirectiveTreenode(oTask);
            //Add the task treenode to the treeview
            tvExecuter.Nodes.Add(oTaskTreeNode);
            //Check if task is loaded
            if (TASK == null) return;
            //Expand task nodes
            TASK.Expand();
            //Expand step nodes
            foreach (TreeNode oNode in TASK.Nodes) oNode.Expand();
            //Reset indexes
            _iCurrentStepIndex = -1;
            _iCurrentInstructionIndex = -1;
            //Load all job responses for scheduled task
            _oCurrentJobResponseList = _oMongoDal.GetJobResponses(TASK_DIRECTIVE.JobOrderId);
            //Iterate over all steps
            foreach (TreeNode oStepNode in oTaskTreeNode.Nodes)
            {
                //Check if step is running
                var oCurrentStepDirective = (WorkDirective)oStepNode.Tag;
                var oCurrentStepResponse = _oCurrentJobResponseList
                    .Where(jr => jr.ResponseType == "STEP")
                    .Where(jr => jr.WorkDirectiveId == oCurrentStepDirective.Id)
                    .OrderByDescending(jr => jr.StartTime)
                    .FirstOrDefault();
                if (oCurrentStepResponse.JobState == WorkState.Running) _iCurrentStepIndex = oTaskTreeNode.Nodes.IndexOf(oStepNode);
                //Iterate over all instructions
                foreach (TreeNode oInstructionNode in oStepNode.Nodes)
                {
                    //Check if instruction is running
                    var oCurrentInstruction = (Node)oInstructionNode.Tag;
                    var oCurrentNodeResponse = _oCurrentJobResponseList
                        .Where(jr => jr.ResponseType == "INSTRUCTION")
                        .Where(jr => jr.WorkDirectiveId == oCurrentStepDirective.Id)
                        .Where(jr => jr.NodeId == oCurrentInstruction.Id)
                        .OrderByDescending(jr => jr.StartTime)
                        .FirstOrDefault();
                    if (oCurrentNodeResponse.JobState == WorkState.Running) _iCurrentInstructionIndex = oStepNode.Nodes.IndexOf(oInstructionNode);
                }
            }
            SetExecutorControls(true);
            HighlightNode(Color.LightBlue);
        }
        /// <summary>
        /// Stop the current running task.
        /// </summary>
        private void StopCurrentTask(bool xForce = false)
        {

            if (!xForce) StopJobResponse(TASK_RESPONSE);
            _iCurrentStepIndex = -1;
            _iCurrentInstructionIndex = -1;
            if (_oHim.Available) _oHim.StartProcess("RESET");
            _oCurrentJobResponseList = null;
            LoadScheduledTasks();
            LoadPerformanceTasks();
            _xTaskRunning = false;
            SetExecutorControls(false);
            if (TASK == null) return;
            tvExecuter.Nodes.RemoveAt(0);
        }

        #region Update job response
        /// <summary>
        /// Start a job response.
        /// </summary>
        /// <param name="oJobResponse">The job response.</param>
        private void StartJobResponse(JobResponse oJobResponse)
        {
            if (oJobResponse == null) return;
            //Check state
            switch (oJobResponse.JobState)
            {
                case WorkState.Ready:
                    oJobResponse.StartTime = DateTime.Now;
                    oJobResponse.JobState = WorkState.Running;
                    _oMongoDal.UpdateJobResponse(oJobResponse);
                    break;
                case WorkState.Running:
                    //Change state to completed (task wasn't closed correctly)
                    if (oJobResponse.EndTime.Ticks > 0)
                    {
                        oJobResponse.JobState = WorkState.Completed;
                        _oMongoDal.UpdateJobResponse(oJobResponse);
                    }
                    else
                    {
                        oJobResponse.JobState = WorkState.Ready;
                    }
                    StartJobResponse(oJobResponse);
                    //Start job response
                    break;
                case WorkState.Completed:
                    //Find root job response
                    var oJobResponseList = _oCurrentJobResponseList
                        .Where(jr => jr.ResponseType == oJobResponse.ResponseType)
                        .Where(jr => jr.WorkDirectiveId == oJobResponse.WorkDirectiveId)
                        .Where(jr => jr.NodeId == oJobResponse.NodeId).
                        OrderByDescending(jr => jr.StartTime);
                    //Generate new ID
                    string ID = oJobResponseList.First().Id;
                    ID += "_" + oJobResponseList.Count();
                    var oNewJobResponse = new JobResponse()
                    {
                        Id = ID,
                        StartTime = DateTime.Now,
                        JobState = WorkState.Running,
                        ResponseType = oJobResponse.ResponseType,
                        WorkDirectiveId = oJobResponse.WorkDirectiveId,
                        WorkMasterId = oJobResponse.WorkMasterId,
                        JobOrderId = oJobResponse.JobOrderId,
                        JobResponseId = oJobResponse.JobResponseId,
                        NodeId = oJobResponse.NodeId
                    };
                    _oMongoDal.UpdateJobResponse(oNewJobResponse);
                    _oCurrentJobResponseList.Add(oNewJobResponse);
                    break;
            }
        }
        /// <summary>
        /// Stop a job response.
        /// </summary>
        /// <param name="oJobResponse">The job response</param>
        private void StopJobResponse(JobResponse oJobResponse)
        {
            if (oJobResponse == null) return;
            oJobResponse.EndTime = DateTime.Now;
            oJobResponse.JobState = WorkState.Completed;
            _oMongoDal.UpdateJobResponse(oJobResponse);
        }
        /// <summary>
        /// Update the stop time of a job response (in order to resume if not stopped correctly)
        /// </summary>
        /// <param name="oJobResponse"></param>
        private void UpdateJobResponse(JobResponse oJobResponse)
        {
            if (oJobResponse == null) return;
            oJobResponse.EndTime = DateTime.Now;
            oJobResponse.JobState = WorkState.Running;
            _oMongoDal.UpdateJobResponse(oJobResponse);
        }

        #endregion

        /// <summary>
        /// Highlight the node of the active task, step or instruction.
        /// </summary>
        /// <param name="oColor">The highlight color.</param>
        private void HighlightNode(Color oColor)
        {
            if (TASK == null) return;
            TASK.BackColor = oColor;
            if (STEP == null) return;
            STEP.BackColor = oColor;
            if (INSTRUCTION == null) return;
            INSTRUCTION.BackColor = oColor;
        }

        /// <summary>
        /// Send the new instruction to the correct client.
        /// </summary>
        private void UpdateClient()
        {
            if (INSTRUCTION_NODE == null || CURRENT_WORKSTATION == null) return;
            SendInstructionToWorkstation(TASK_DIRECTIVE, STEP_DIRECTIVE, INSTRUCTION_NODE, CURRENT_WORKSTATION);
        }

        #endregion

        #region ControlEvents

        private void btnScheduleTask_Click(object sender, EventArgs e) {
            if (lstTasks.SelectedIndex == -1) return;
            //get the task
            WorkMaster oTask = (WorkMaster) lstTasks.SelectedItem;
            //tvSchedule.Nodes.Add(ScheduleTask(_oMongoDal.GetWorkMaster(oTask.Id, oTask.Version),chkUseHIM.Checked,chkUsePTL.Checked,chkUseRobot.Checked));
            var oNewNode = ScheduleTask(oTask, chkUseHIM.Checked, chkUsePTL.Checked, chkUseRobot.Checked);
            tvSchedule.Nodes.Add(oNewNode);
            tvSchedule.SelectedNode = oNewNode;
        }

        private void btnNextInstruction_Click(object sender, EventArgs e)
        {
            // alle picking lights disablen
            _oADSInterface.ResetLights();
            //Buttons disabled?
            if (_xDisableButtons) return;
            //Load next instruction from workflow (except when HIM is running)
            if (!_oHim.ProcessRunning) NextInstruction();
            if (TASK == null) return;
            //Picking support
            //TODO: Send picking instruction step-by-step
            if (TASK_DIRECTIVE.PickingSupport) {
                List<int> oPickToLights = new List<int>();
                if (INSTRUCTION_NODE?.PickingActions?.Count > 0)
                {
                    foreach (PickingAction oPickingAction in INSTRUCTION_NODE.PickingActions)
                    {
                        int iPickingLocation = _oMaterialDefinitionList.Where(md => md.Id == oPickingAction.MaterialDefinitionId).Select(md => md.PickingLocation).FirstOrDefault();
                        if (iPickingLocation > 0) { oPickToLights.Add(iPickingLocation); }
                    }
                }
                _oADSInterface.SetPickToLight(oPickToLights.ToArray());
            }

            //HIM support
            if (TASK_DIRECTIVE.HimSupport)
            {
                if (STEP_DIRECTIVE?.HierarchyScope == "Afwerkpost")
                {
                    if (!_oHim.ProcessRunning) _oHim.StartProcess(STEP_DIRECTIVE.WorkMasterId,_iCurrentInstructionIndex);
                    else _oHim.NextStep();
                }
            }

            //COBOT support
            if (TASK_DIRECTIVE.CobotSupport) {
                string sRobotInstruction = INSTRUCTION_NODE?.CobotCommand;
                if (sRobotInstruction != null && sRobotInstruction != "") {
                    //TODO: Handle cobot command
                }
            }
        }

        private void btnPreviousInstruction_Click(object sender, EventArgs e)
        {
            //Buttons disabled?
            if (_xDisableButtons) return;
            if (TASK == null) return;
            if (STEP == null) return;
            if (INSTRUCTION == null) return;
            if (!_oHim.ProcessRunning) PreviousInstruction();
            else
            {
                if (int.Parse(_oHim.CurrentStep) > 0) _oHim.PreviousStep();
                else
                {
                    _oHim.StopProcess();
                    PreviousInstruction();
                }
                    
                    
            }
        }


        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (tvSchedule.SelectedNode == null) return;
            RemoveScheduledTask(tvSchedule.SelectedNode);
            ClearScheduleInfo();
        }

        private void tvSchedule_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (tvSchedule.SelectedNode == null) return;
            var oNode = tvSchedule.SelectedNode;
            while (oNode.Parent != null) oNode = oNode.Parent;
            var oWorkDirective = (WorkDirective)oNode.Tag;
            UpdateScheduleInfo(oWorkDirective);
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            if (tvSchedule.SelectedNode == null) return;
            var oNode = tvSchedule.SelectedNode;
            while (oNode.Parent != null) oNode = oNode.Parent;
            ExecuteTask((WorkDirective)oNode.Tag);
            this.Select();
        }

        private void btnExcecutorStop_Click(object sender, EventArgs e)
        {
            //Reset current tasks
            StopCurrentTask(true);
            LoadScheduledTasks();
        }

        private void tvPerformance_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (tvPerformance.SelectedNode == null) return;
            UpdatePerformanceInfo(tvPerformance.SelectedNode);
        }
        #endregion

        #region Tcp

        private void _oTcpServer_Disconnected(object sender, TCPS.MessageEventargs e) {
            if (e.oTCPClient == _oVrijWerkVlak?.oTcpClient)
            {
                _oVrijWerkVlak = null;
                txtConnectionVWV.Invoke((MethodInvoker)delegate { txtConnectionVWV.Text = "Not connected"; txtConnectionVWV.BackColor = Color.IndianRed; });
            }
            else if (e.oTCPClient == _oPerstafel?.oTcpClient)
            {
                _oPerstafel = null;
                txtConnectionPT.Invoke((MethodInvoker)delegate { txtConnectionPT.Text = "Not connected"; txtConnectionPT.BackColor = Color.IndianRed; });
            }
            else if (e.oTCPClient == _oAfwerkpost?.oTcpClient)
            {
                _oAfwerkpost = null;
                txtConnectionAP.Invoke((MethodInvoker)delegate { txtConnectionAP.Text = "Not connected"; txtConnectionAP.BackColor = Color.IndianRed; });
            }
        }

        private void _oTcpServer_NewClient(object sender, TCPS.MessageEventargs e) {
            TcpClient oTcpClient = e.oTCPClient;
            SendMessage("Who are you?", oTcpClient);
        }

        private void _oTcpServer_NewMessage(object sender, TCPS.MessageEventargs e) {
            string[] arrsData = e.sMessage.Split('|');
            switch (arrsData[0]) {
                case "I am":
                    switch (arrsData[1]) {
                        case "Vrijwerkvlak":
                            _oVrijWerkVlak = new clsWorkstation(e.oTCPClient);
                            txtConnectionVWV.Invoke((MethodInvoker)delegate { txtConnectionVWV.Text = "Connected"; txtConnectionVWV.BackColor = Color.LightGreen; });

                            if (TASK != null & STEP != null & INSTRUCTION != null)
                            {
                                if (STEP_DIRECTIVE.HierarchyScope == "VrijWerkvlak")
                                {
                                    SendInstructionToWorkstation(TASK_DIRECTIVE, STEP_DIRECTIVE, INSTRUCTION_NODE, _oVrijWerkVlak);
                                }
                            }
                            break;
                        case "Perstafel":
                            _oPerstafel = new clsWorkstation(e.oTCPClient);
                            txtConnectionPT.Invoke((MethodInvoker)delegate { txtConnectionPT.Text = "Connected"; txtConnectionPT.BackColor = Color.LightGreen; });

                            if (TASK != null & STEP != null & INSTRUCTION != null)
                            {
                                if (STEP_DIRECTIVE.HierarchyScope == "Perstafel")
                                {
                                    SendInstructionToWorkstation(TASK_DIRECTIVE, STEP_DIRECTIVE, INSTRUCTION_NODE, _oPerstafel);
                                }
                            }
                            // onmiddellijk doorsturen wat de huidige status is
                            if (_oADSInterface.IsADSConnected())
                            {
                                oAdsInterface_KlemStatus(_oADSInterface.KlemStatus());
                            }
                            
                            break;
                        case "Afwerkpost":
                            _oAfwerkpost = new clsWorkstation(e.oTCPClient);
                            txtConnectionAP.Invoke((MethodInvoker)delegate { txtConnectionAP.Text = "Connected"; txtConnectionAP.BackColor = Color.LightGreen; });

                            if (TASK != null & STEP != null & INSTRUCTION != null)
                            {
                                if (STEP_DIRECTIVE.HierarchyScope == "Afwerkpost")
                                {
                                    SendInstructionToWorkstation(TASK_DIRECTIVE, STEP_DIRECTIVE, INSTRUCTION_NODE, _oAfwerkpost);
                                }
                            }
                            break;
                        default:
                            e.oTCPClient.Close();
                            break;
                    }
                    break;
                case "ACK":
                    clsWorkstation oWorkStation = GetWorkstationFromTcpClient(e.oTCPClient);
                    if (oWorkStation == null) { break; }
                    oWorkStation.xWaitingForReply = false;
                    break;
            }
        }

        private void SendInstructionToWorkstation(WorkDirective oTask, WorkDirective oStep, Node oInstruction, clsWorkstation oWorkstation) {
            if (oTask == null | oStep == null | oInstruction == null | oWorkstation == null) { return; }
            if (!_oTcpServer.CheckConnected(oWorkstation.oTcpClient)) { return; }
            if (oWorkstation.xWaitingForReply) { return; }

            List<String> oData = new List<string>();
            oData.Add("Data");
            oData.Add(oTask.Id);
            oData.Add(oStep.Id);
            oData.Add(oInstruction.Id);
            oData.Add(oInstruction.TextInstruction);
            oData.Add(oInstruction.ImageInstruction);
            oData.Add("Clamp"); //todo, determine the mode of the workstation (universal, clamp or hydraulic), comes from PLC

            string sPickingData = "";
            if (!(oInstruction.PickingActions == null) && oInstruction.PickingActions.Count > 0) {
                List<string> oPickingData = new List<string>();
                foreach (PickingAction oPickingAction in oInstruction.PickingActions) {
                    oPickingData.Add(oPickingAction.MaterialDefinitionId);
                    oPickingData.Add(oPickingAction.Quantity.ToString());
                }
                sPickingData = string.Join("§", oPickingData);
            }
            oData.Add(sPickingData);

            string sMessage = string.Join("|", oData);
            //Message structure: Data|TaskId|StepId|InstructionId|Text|ImagePath|WorkstationMode|Material1§Quantity1§Material2§Quantity2
            SendMessage(sMessage, oWorkstation.oTcpClient);//if you want to send the message to all workstations(tcp clients), use the .SendToAll method instead
        }

        private void ClearClient(clsWorkstation oWorkstation)
        {
            if (oWorkstation == null) { return; }
            if (!_oTcpServer.CheckConnected(oWorkstation.oTcpClient)) { return; }
            if (oWorkstation.xWaitingForReply) { return; }
            List<String> oData = new List<string>();
            oData.Add("Data");
            oData.Add("-1");//task id
            oData.Add("");// step id
            oData.Add("");// instruction id
            oData.Add("");  //
            oData.Add("");
            oData.Add(""); 
            oData.Add("");
            string sMessage = string.Join("|", oData);
            //Message structure: Data|TaskId|StepId|InstructionId|Text|ImagePath|WorkstationMode|Material1§Quantity1§Material2§Quantity2
            SendMessage(sMessage, oWorkstation.oTcpClient);//if you want to send the message to all workstations(tcp clients), use the .SendToAll method instead
        }


        private clsWorkstation GetWorkstationFromTcpClient(TcpClient oTcpClient) {
            if (oTcpClient == null) { return null; }
            if (_oVrijWerkVlak?.oTcpClient == oTcpClient) { return _oVrijWerkVlak; }
            if (_oPerstafel?.oTcpClient == oTcpClient) { return _oPerstafel; }
            if (_oAfwerkpost?.oTcpClient == oTcpClient) { return _oAfwerkpost; }
            return null;
        }

        #endregion

        #region "ADS"

        #region "ADS form stuff"

        private void GatherInfo()
        {
            txtAMSaddress.Text = Properties.Settings.Default.sAMSaddress;
            txtADSPort.Text = Properties.Settings.Default.sADSPort;
        }
        private void StoreInfo()
        {
            Properties.Settings.Default.sAMSaddress = txtAMSaddress.Text;
            Properties.Settings.Default.sADSPort = txtADSPort.Text;
        }
        private void Vis_Textbox(TextBox o, Color c, string t)
        {
            o.Invoke((Action)delegate
            {
                o.BackColor = c;
                o.Text = t;
                o.Update();
            });

        }
        private void Vis_ADS(bool x, bool xOverwrite = true)
        {
            this.Invoke((Action)delegate
            {
                btnADSconnect.Enabled = !x & xOverwrite;
                btnADSdisconnect.Enabled = x & xOverwrite;
                txtADSPort.Enabled = !x & xOverwrite;
                txtAMSaddress.Enabled = !x & xOverwrite;
            });
        }
        private void btnConnect_Click(object sender, EventArgs e)
        {
            Vis_ADS(true, false);
            Vis_Textbox(txtStatusADS, Color.Yellow, "Connecting (loading all vars)");
            _oADSInterface.StartConnecting(txtAMSaddress.Text, int.Parse(txtADSPort.Text));
        }
        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            Vis_Textbox(txtStatusADS, Color.Yellow, "Disconnecting (Cleaning up)");
            _oADSInterface.StartDisconnecting();
        }

        private void CheckIPbox(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void CheckPortbox(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
            {
                e.Handled = true;
            }
        }
        #endregion

        private void SendMessage(string s , TcpClient o)
        {
            s += "$";
            if (_oTcpServer != null & o != null)
            {
                SendMessage(s, o);
            }
            
        }


        #region ADS events interface

        private void CatchConnectionEvent(bool x)
        {
            if (x)
            {
                Vis_Textbox(txtStatusADS, Color.LightGreen, "Connected");
            }
            else
            {
                Vis_Textbox(txtStatusADS, Color.IndianRed, "Not Connected");
            }
            Vis_ADS(x);
        }
        private void oAdsInterface_NextInstruction()
        {
            btnNextInstruction_Click(null, null);
        }
        private void oAdsInterface_PreviousInstruction()
        {
            btnPreviousInstruction_Click(null, null);
        }
        private void oAdsInterface_PickingLightChange(bool xColorWasGreen, int iLightNumber) {
            //Log the picking action to the database
            PickingLog oPickingLog = new PickingLog();
            oPickingLog.LightActivated = xColorWasGreen;
            oPickingLog.PickingLight = iLightNumber;
            oPickingLog.Timestamp = DateTime.Now;
            string sMatId;
            if (_oMaterialDefinitionList == null) {
                sMatId = "No Material Definition Available";
            }
            else
            {
                sMatId = _oMaterialDefinitionList.Where(md => md.PickingLocation == iLightNumber).FirstOrDefault()?.Id ; 
            }
        
            oPickingLog.MaterialDefinitionId = sMatId;
            _oMongoDal.InsertPickingLog(oPickingLog);
            //todo, test PTL event logging
        }

        private void oAdsInterface_KlemStatus(bool x)
        {
            string s = "Klem|"+ x.ToString();

            if (_oPerstafel != null)
            {
                SendMessage(s, _oPerstafel?.oTcpClient);
            }
            
        }

        private void oAdsInterface_MeasurementIsDone(bool x)
        {
            string s = "Measurement|" + x.ToString();

            if (_oPerstafel != null)
            {
                SendMessage(s, _oPerstafel?.oTcpClient);
            }
        }

        private void oAdsInterface_DubbeleHandbedieningActive()
        {
            string s = "KlemmenBusy|" ;
            if (_oPerstafel != null)
            {
                SendMessage(s, _oPerstafel?.oTcpClient);
            }
        }

        private void oAdsInterface_DubbeleHandbedieningOK()
        {
            string s = "KlemmenOK|";
            if (_oPerstafel != null)
            {
                SendMessage(s, _oPerstafel?.oTcpClient);
            }
        }

        private void oAdsInterface_NextStepComming(bool x)
        {
            string s = "NextStepIncomming|";
            if (x)
            {
                s += "1";
                if (_oVrijWerkVlak?.oTcpClient != null)
                {
                    SendMessage(s, _oVrijWerkVlak?.oTcpClient);
                }
                if (_oPerstafel?.oTcpClient != null)
                {
                    SendMessage(s, _oPerstafel?.oTcpClient);
                }
                if (_oAfwerkpost?.oTcpClient != null)
                {
                    SendMessage(s, _oAfwerkpost?.oTcpClient);
                }
            }
            else
            {
                s += "2";
            }
     
           
            
        }
        #endregion

        #endregion

        #region HIM

        private void HimStepChanged()
        {
            this.Invoke((MethodInvoker)delegate
            {
                if (TASK == null) return;
                if (STEP == null) return;
                //Disable buttons
                _xDisableButtons = true;
                HighlightNode(Color.White);
                var sStep = _oHim.CurrentStep;
                if (sStep != "")
                {
                    int iStep = int.Parse(sStep);
                    while (iStep > _iCurrentInstructionIndex && _iCurrentInstructionIndex > -1) GetInstruction();
                    while (iStep < _iCurrentInstructionIndex && _iCurrentInstructionIndex > -1) GetInstruction(true);
                }
                HighlightNode(Color.LightBlue);
                _oHim.SetVariable("FlexAs_HimAck", "FALSE");
                //Enable buttons
                _xDisableButtons = false;
            });
        }

        private void HimPickRequest()
        {

        }

        private void HimProcessFinished()
        {
            this.Invoke((MethodInvoker)delegate
            {
                btnNextInstruction_Click(null, null);
            });
        }

        #endregion

    }
}
