﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ADScom;

namespace FlexAs_Scheduler
{
    public delegate void ConnectionActionHandler(bool xResult);
    public delegate void NextStepHandler();
    public delegate void PreviousStepHandler();
    public delegate void PickLightChangeHandler(bool xWasGreen, int iNumber);
    public delegate void KlemStatusChangeHandler(bool xIsKlem);
    public delegate void MeasurementIsDoneHandler(bool xMeasurementSuccessfull);
    public delegate void RobotActionDonehandler();
    public delegate void DubbeleHandbedieningHandler();
    public delegate void DubbeleHandbedieningOKhandler();
    public delegate void NextStepCommingHandler(bool x);

    public class clsADSInterface
    {
        //Beckhoff communication
        private clsADScommunicatie _oADScom = new clsADScommunicatie();

        // interessante variabelen
        private clsPLCvariable _oNextStep;
        private clsPLCvariable _oPreviousStep;
        private clsPLCvariable _oPickingLights;
        private clsPLCvariable _oKlemActive;
        private clsPLCvariable _oIsMeasurementDone;
        private clsPLCvariable _oMeasurementResult;
        private clsPLCvariable _oBezigMetKlemmen;
        private clsPLCvariable _oKlemmenIsKlaar;
        private clsPLCvariable _oResetLights;
        private clsPLCvariable _oNextStepComming;
        


        //events
        public event ConnectionActionHandler ConnectionActionDone;
        public event NextStepHandler NextStep;
        public event PreviousStepHandler PreviousStep;
        public event PickLightChangeHandler PickingLightChange;
        public event KlemStatusChangeHandler KlemStatusChange;
        public event MeasurementIsDoneHandler MeasurementIsDone;
        public event RobotActionDonehandler RobotActionIsDone;
        public event DubbeleHandbedieningHandler DubbeleHandbedieningActive;
        public event DubbeleHandbedieningOKhandler DubbeleHandbedieningOK;
        public event NextStepCommingHandler NextStepComming;

        //hulp var
        private bool xCyclusOK = false;
        private object _oLock = new object();
        

        public clsADSInterface()
        {
            _oADScom.CyclusDone += ProcessCyclusResult;
            this.ConnectionActionDone += SetPLCvars;
        }

        #region "Connect / Disconnect"
        public void StartConnecting(string sAMSaddress , int iADSport)
        {
            RunInNewThread(new Action[] { () => _oADScom.Connect(sAMSaddress, iADSport, false, new string[] { "GVL" }), () => ConnectionActionDone?.Invoke(_oADScom.CheckConnection), () => KlemStatusChange?.Invoke(KlemStatus()) });
        }

        public void StartDisconnecting()
        {
            RunInNewThread(new Action[] { () => xCyclusOK = false, () => _oADScom.Disconnect(), () => ConnectionActionDone?.Invoke(false) }) ;
        }

        private void SetPLCvars(bool x)
        {
            if (x)
            {
                _oNextStep = _oADScom.GetVarObject("GVL.xNextStep");
                _oPreviousStep = _oADScom.GetVarObject("GVL.xPreviousStep");
                _oPickingLights = _oADScom.GetVarObject("GVL.arrPickingLights");
                _oKlemActive = _oADScom.GetVarObject("GVL.i_xKlemActive");
                _oIsMeasurementDone = _oADScom.GetVarObject("GVL.xMeasureDone");
                _oMeasurementResult = _oADScom.GetVarObject("GVL.iMeasurementSTATUS");
                _oBezigMetKlemmen = _oADScom.GetVarObject("GVL.xManualOK");
                _oKlemmenIsKlaar = _oADScom.GetVarObject("GVL.xManualReady");
                _oResetLights = _oADScom.GetVarObject("GVL.xResetAllPL");
                _oNextStepComming = _oADScom.GetVarObject("GVL.xNextStepComming");
                xCyclusOK = true;
            }
            else
            {
                lock (_oLock)
                {
                    _oNextStep = null;
                    _oPreviousStep = null;
                    _oPickingLights = null;
                    _oKlemActive = null;
                    _oIsMeasurementDone = null;
                    _oMeasurementResult = null;
                    _oBezigMetKlemmen = null;
                    _oKlemmenIsKlaar = null;
                    _oResetLights = null;
                    _oNextStepComming = null;
                }             
            }
            
        }

        #endregion

        #region "PLC Logic"

        public void ProcessCyclusResult()
        {
            lock(_oLock)
            {
                // er wordt eerder een event uitgestuurd dan dat deze variabelen worden gevuld in setplcvar
                // eerst daarop wachten
                if (!xCyclusOK) { return; }
                try
                {
                    bool xSendKLem = false;
                    // next 
                    if (_oNextStep.CurrentValue.ToString() == "True")
                    {                      
                        _oADScom.SingleWrite(_oNextStep, false);
                        Console.WriteLine("PLC var : next step");
                         RunInNewThread(new Action[] { () => NextStep?.Invoke(), () => _oADScom.SingleWrite(_oNextStep, false) });
                    }
                    //previous
                    if (_oPreviousStep.CurrentValue.ToString() == "True")
                    {                     
                        _oADScom.SingleWrite(_oPreviousStep, false);
                        Console.WriteLine("PLC var : previous step");
                        RunInNewThread(new Action[] { () => PreviousStep?.Invoke(), () => _oADScom.SingleWrite(_oPreviousStep, false) });
                    }
                   
                    // measurement done
                    if (_oIsMeasurementDone.CurrentValue.ToString() == "True")
                    {
                        Console.WriteLine("PLC var : measurement done");
                        RunInNewThread(new Action[] { () => MeasurementIsDone?.Invoke(_oMeasurementResult.CurrentValue.ToString() == "2"), () => _oADScom.SingleWrite(_oIsMeasurementDone, false)});
                                        
                    }
                    // manuele bediening
                    if (_oBezigMetKlemmen.HasChanged)
                    {
                        Console.WriteLine("PLC var : klem has changed");
                        if (_oBezigMetKlemmen.CurrentValue.ToString() == "True")
                        {
                            // manuele bediening is actief, we willen dat tonen op het scherm 
                            RunInNewThread(new Action[] { () => DubbeleHandbedieningActive?.Invoke( )});
                        }
                        else
                        { // manuele bediening is niet meer actief, dus tonen we klemstatus terug
                            xSendKLem = true;
                        }        
                    }
                    if (_oKlemmenIsKlaar.HasChanged)
                    {
                        Console.WriteLine("PLC var : klem is klaar");
                        if (_oKlemmenIsKlaar.CurrentValue.ToString() == "True")
                        { // de klem is klaar, deze boodschap willen we op het scherm tonen
                            RunInNewThread(new Action[] { () => DubbeleHandbedieningOK?.Invoke() });
                        }
                        else
                        {   // terug naar af ;)
                            xSendKLem = true;
                        }

                    }
                    if (_oNextStepComming.HasChanged)
                    {
                        Console.WriteLine("PLC var : next step incomming");
                        RunInNewThread(new Action[] { () => NextStepComming?.Invoke(_oNextStepComming.CurrentValue.ToString() == "True") });
                    }

                    //klem status
                    if (_oKlemActive.HasChanged || xSendKLem)
                    {
                        Console.WriteLine("PLC var : klemactive change");
                        RunInNewThread(new Action[] { () => KlemStatusChange?.Invoke(KlemStatus()) });
                    }


                    // check all picking lights
                    for (int i = 0; i < _oPickingLights.SubVars.Count(); i++)
                    {
                        if (_oPickingLights.SubVars[i].GetSubVarByName("xInputDetected").CurrentValue.ToString() == "True")
                        {
                            // er is een variabele gedetecteerd
                            bool x = _oPickingLights.SubVars[i].GetSubVarByName("q_xLight").CurrentValue.ToString() == "True";
                            int i2 = i ; // due to multithreading, better use a temporary var then the one from the for-loop
                            RunInNewThread(new Action[] { () =>PickingLightChange?.Invoke(x, i2+1) , () => _oADScom.SingleWrite(_oPickingLights.SubVars[i2].GetSubVarByName("xLogged"), true) }) ;
                        }
                    }
                }
                catch 
                {
                    
                }
            
            }
           
        }

        public bool SetPickToLight(int i)
        {
            if (_oPickingLights != null)
            {
               return _oADScom.SingleWrite(_oPickingLights.SubVars[i-1].GetSubVarByName("q_xLight"), true);
            } else { return false; }
        }

        public bool SetPickToLight(int[] arri)
        {
            bool x = true;
            foreach( int i in arri)
            {
                x = x & SetPickToLight(i);
            }
            return x;
        }
     
        public void ResetLights()
        {
            _oADScom?.SingleWrite(_oResetLights, true); 
        }


        #endregion

        #region "Auxiliary"
        private void RunInNewThread(Action[] arrActions)
        {
            Thread t = new Thread(() => {
                foreach (Action oAction in arrActions)
                {
                    oAction();
                }
            });
            t.Start();
        }
        #endregion

        public bool KlemStatus()
        {
           return ( _oKlemActive?.CurrentValue.ToString() == "True");
        }

        public bool IsADSConnected()
        {
            return _oADScom.CheckConnection;
        }

      
    }
}
