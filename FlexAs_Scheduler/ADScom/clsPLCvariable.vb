﻿Imports TwinCAT.Ads
Imports System.IO

Public Class clsPLCvariable

    'hoofdvariabele
    Private _tcAdsSymbolInfo As TcAdsSymbolInfo
    Private _oCurrentValue As Object = Nothing

    'ondersteunende variabelen
    Private _iVariableHandle As Integer
    Private _xHandleReady As Boolean = False
    Private _xError As Boolean = False
    Private _bData() As Byte
    Private _xIsSubItem As Boolean
    Private _xIncludeInReadAll As Boolean = True
    Private _xVarChange As Boolean = False

    'subvariabelen
    Private _oSubVariables As New List(Of clsPLCvariable)

    'hulpvariabele ENKEL gebruikt om handle te vinden van subvariabelen
    'in theorie zou hiermee ook een write-actie kunnen uitgevoerd worden in de clsplcvariabele, maar alles blijft geconcentreerd in clsadscommunciate
    Private _otcClient As TcAdsClient

    'events
    Private _xEventCoupled As Boolean = False
    Private _iEventHandler As Integer = 0



    Public Sub New(tcAdsSingleVar As TcAdsSymbolInfo, ByRef otcClient As TcAdsClient, Optional xIsSubItem As Boolean = False)
        _tcAdsSymbolInfo = tcAdsSingleVar
        _otcClient = otcClient
        Dim iThread As System.Threading.Thread = New System.Threading.Thread(AddressOf CreateVariableHandleThread)
        iThread.Start()
        _xIsSubItem = xIsSubItem


        For Each oSubVar As TcAdsSymbolInfo In tcAdsSingleVar.SubSymbols
            If oSubVar.IndexOffset <> 0 Then
                AddSubVar(oSubVar, _otcClient)
            End If
        Next

    End Sub

    Private Sub CreateVariableHandleThread()
        _iVariableHandle = _otcClient.CreateVariableHandle(_tcAdsSymbolInfo.Name)
        _xHandleReady = True
    End Sub


#Region "SubVariabelen"

    ''' <summary>
    ''' Toevoegen subvariabelen
    ''' </summary>
    ''' <param name="tcAdsSubVar"></param>
    ''' <param name="otcClient"></param>
    Public Sub AddSubVar(tcAdsSubVar As TcAdsSymbolInfo, ByRef otcClient As TcAdsClient)
        _oSubVariables.Add(New clsPLCvariable(tcAdsSubVar, otcClient, True))
    End Sub

    ''' <summary>
    ''' Verzamelen van alle variabelen. Dit is dus de variabele van dit object en elk object van alle subvariabelen
    ''' </summary>
    ''' <returns></returns>
    Public Function CollectAll() As List(Of clsPLCvariable)
        Dim oReturn As New List(Of clsPLCvariable)
        oReturn.Add(Me)
        If HasSubItems Then
            For Each oSubItem As clsPLCvariable In _oSubVariables
                oReturn.AddRange(oSubItem.CollectAll())
            Next
        End If
        Return oReturn
    End Function

#End Region


#Region "UpdateValue"

    ''' <summary>
    ''' Schrijf databytes naar variabelen. Deze databytes mogen nooit opgevraagd worden. Dit omdat ze niet noodzakelijk actueel zijn.
    ''' Wanneer niet actueel : Variabele geüpdatet dmv readany. In dat geval wordt onmiddellijk een object meegegeven, geen byte().
    ''' </summary>
    Public WriteOnly Property DataBytes(Optional xIsWriteAction As Boolean = False) As Byte()
        Set(value() As Byte)
            If value Is Nothing Then
                _xError = True
                Exit Property
            End If
            ConvertDataBytes(value)
            If xIsWriteAction Then
                _xVarChange = False
            End If
        End Set
    End Property



    ''' <summary>
    ''' Converteren byte() naar het juiste datatype
    ''' </summary>
    ''' <param name="bData"></param>
    Private Sub ConvertDataBytes(bData() As Byte)
        _xError = False

        _xVarChange = False
        If _bData IsNot Nothing Then


            For i As Integer = 0 To Math.Min(_bData.Count() - 1, bData.Count() - 1)
                If _bData(i) <> bData(i) Then
                    _xVarChange = True
                    Exit For
                End If
            Next

        End If

        _bData = bData.Clone()
        Dim oDatastream As BinaryReader = New BinaryReader(New MemoryStream(bData))
        Try

            If IsArray Then
                _oCurrentValue = BigTypeSupport(_bData)
                Exit Sub
            End If

            Select Case _tcAdsSymbolInfo.DataTypeId
                Case AdsDatatypeId.ADST_BIT
                    _oCurrentValue = oDatastream.ReadBoolean
                Case AdsDatatypeId.ADST_INT16
                    _oCurrentValue = oDatastream.ReadInt16
                Case AdsDatatypeId.ADST_INT32
                    _oCurrentValue = oDatastream.ReadInt32
                Case AdsDatatypeId.ADST_INT64
                    _oCurrentValue = oDatastream.ReadInt64
                Case AdsDatatypeId.ADST_WSTRING
                    _oCurrentValue = System.Text.Encoding.Unicode.GetString(_bData)
                    _oCurrentValue = _oCurrentValue.replace(vbNullChar, "")
                Case AdsDatatypeId.ADST_BIGTYPE
                    _oCurrentValue = BigTypeSupport(_bData)
                Case AdsDatatypeId.ADST_INT8
                    _oCurrentValue = oDatastream.ReadSByte
                Case AdsDatatypeId.ADST_UINT8
                    _oCurrentValue = oDatastream.ReadByte
                Case AdsDatatypeId.ADST_UINT16
                    _oCurrentValue = oDatastream.ReadUInt16
                Case AdsDatatypeId.ADST_UINT32
                    _oCurrentValue = oDatastream.ReadUInt32
                Case AdsDatatypeId.ADST_REAL32
                    _oCurrentValue = oDatastream.ReadSingle
                Case AdsDatatypeId.ADST_REAL64
                    _oCurrentValue = oDatastream.ReadDouble
                Case AdsDatatypeId.ADST_STRING
                    _oCurrentValue = System.Text.Encoding.ASCII.GetString(_bData)
                    _oCurrentValue = _oCurrentValue.replace(vbNullChar, "")
                Case Else
                    If _bData IsNot Nothing Then
                        _oCurrentValue = "Not Supported"
                    Else
                        _oCurrentValue = "Not Supported (nothing)"
                    End If
            End Select

        Catch ex As Exception
            MsgBox("fout bij conversie van datastream uit plc naar vb.net" & vbCrLf & vbCrLf & _tcAdsSymbolInfo.Name & vbCrLf & vbCrLf & ex.Message & vbCrLf & vbCrLf & ex.StackTrace)
        End Try
    End Sub

#Region "BigType Region"


    ''' <summary>
    ''' Ondersteuning voor Bigtype. Dit is meestal een variabele met subvariabelen (eventueel een array). 
    ''' </summary>
    ''' <param name="bData"></param>
    ''' <returns></returns>
    Private Function BigTypeSupport(bData() As Byte)
        Try
            Dim sOutput As String = "["
            If HasSubItems Then
                If bData.Count >= VarDatalength Then
                    'bigtype met onderliggende datastructuur
                    sOutput += DistributeBytesToSubSymbols(bData)
                Else
                    'bigtype waar de onderliggende datastructuur groter is dat de toegewezen size (waarschijnlijk een word waarin enkele booleans verstopt zitten)
                    Dim xFinalLine As Boolean = True
                    For i As Integer = 0 To _oSubVariables.Count() - 1
                        xFinalLine = xFinalLine And Not _oSubVariables(i).HasSubItems
                    Next
                    If xFinalLine Then
                        'alleen nog maar variabelen uit dat woord gehaald
                        sOutput += DistributeBitsToSubSymbols(bData)
                    Else
                        sOutput += DistributeBytesToSubSymbols(bData)
                    End If

                End If
            Else
                'bigtype zonder onderliggende datastructuur
                sOutput += SpecialBigDataSupport(bData)
            End If


            'de laatste ; verwijderen
            If sOutput.Contains(";") Then
                sOutput = sOutput.Remove(sOutput.LastIndexOf(";"))
            End If
            ' afsluiten
            sOutput += "]"

            Return sOutput
        Catch ex As Exception
            _xError = True
            Return "Dim:" & bData.Count.ToString
        End Try


    End Function


    Private Function DistributeBytesToSubSymbols(bData() As Byte) As String
        Dim sOutput As String = ""

        ' Blijkbaar is beginbyte niet altijd 0!
        Dim iByteOffset As Integer = _tcAdsSymbolInfo.IndexOffset
        Dim iByteIndex As Integer = _oSubVariables(0).GetIndexOffset - iByteOffset

        If iByteIndex > _tcAdsSymbolInfo.ByteSize Then
            sOutput += DistributeBitsToSubSymbols(bData)
        Else
            For i As Integer = 0 To _oSubVariables.Count() - 1

                'buffer vullen voor 1 variabele
                Dim bBuffer(_oSubVariables(i).DataStreamLength() - 1) As Byte
                For j As Integer = 0 To _oSubVariables(i).DataStreamLength() - 1
                    bBuffer(j) = bData(iByteIndex)
                    iByteIndex += 1
                Next

                'data doorgeven aan die variabelen
                _oSubVariables(i).DataBytes = bBuffer
                sOutput += _oSubVariables(i).ToString + " ; "

                'compenseren voor eventuele paddingbytes
                If i <= _oSubVariables.Count() - 2 Then
                    iByteIndex = _oSubVariables(i + 1).GetIndexOffset - iByteOffset
                End If
            Next
        End If

        Return sOutput
    End Function

    Private Function DistributeBitsToSubSymbols(bData() As Byte) As String
        Dim sOutput As String = ""
        Dim oBits As BitArray = New BitArray(bData)

        Dim iOffset As Integer = _oSubVariables(0).GetIndexOffset

        For i As Integer = 0 To _oSubVariables.Count() - 1
            Dim oNewData(0) As Byte

            oNewData(0) = Convert.ToByte(oBits(_oSubVariables(i).GetIndexOffset - iOffset))

            _oSubVariables(i).DataBytes = oNewData
            sOutput += _oSubVariables(i).CurrentValue.ToString + " ; "
        Next
        Return sOutput
    End Function

    Private Function SpecialBigDataSupport(bData() As Byte) As String
        Dim sOutput As String = ""
        Dim oDatastream As AdsBinaryReader = New AdsBinaryReader(New AdsStream(bData))
        If _tcAdsSymbolInfo.TypeName = "TIME" Then
            _oCurrentValue = oDatastream.ReadPlcTIME
        ElseIf _tcAdsSymbolInfo.TypeName = "DATE" Then
            _oCurrentValue = oDatastream.ReadPlcDATE
        ElseIf _tcAdsSymbolInfo.TypeName = "DT" Or _tcAdsSymbolInfo.TypeName = "DATE_AND_TIME" Then
            _oCurrentValue = oDatastream.ReadUInt32
        ElseIf _tcAdsSymbolInfo.TypeName = "TOD" Or _tcAdsSymbolInfo.TypeName = "TIME_OF_DAY" Then
            _oCurrentValue = oDatastream.ReadUInt32
        End If
        Return sOutput
    End Function
#End Region


#End Region

#Region "Events"

    Public Sub CoupleEvent(iHandler As Integer)
        If _xEventCoupled Then
            Throw New Exception("Variable already coupled to an event.")
        End If
        _iEventHandler = iHandler
        _xEventCoupled = True
    End Sub

    Public Function RemoveEvent()
        _xEventCoupled = False
        Dim oReturn As Integer = _iEventHandler
        _iEventHandler = -1
        Return oReturn
    End Function

#End Region


#Region "Readonly Properties"

#Region "status"
    Public ReadOnly Property IsArray()
        Get
            Return _tcAdsSymbolInfo.ArrayDimensions <> 0
        End Get
    End Property

    Public ReadOnly Property IsEventCoupled
        Get
            Return _xEventCoupled
        End Get
    End Property

    Public Property ErrorState As Boolean
        Set(value As Boolean)
            _xError = value
        End Set
        Get
            Return _xError
        End Get
    End Property

    Public ReadOnly Property HasSubItems As Boolean
        Get
            Return _oSubVariables.Count() <> 0
        End Get
    End Property

    Public ReadOnly Property IsSubItem()
        Get
            Return _xIsSubItem
        End Get
    End Property

    Public ReadOnly Property HasChanged() As Boolean
        Get
            Return _xVarChange
        End Get
    End Property

    Public Property IncludeInReadAll() As Boolean
        Get
            Return _xIncludeInReadAll
        End Get
        Set(value As Boolean)
            _xIncludeInReadAll = value
        End Set
    End Property

    Public ReadOnly Property VarDatalength() As Integer
        Get
            Dim iReturn As Integer = 0
            If _oSubVariables.Count() <> 0 Then
                For i As Integer = 0 To _oSubVariables.Count() - 1
                    iReturn += _oSubVariables(i).DataStreamLength
                Next
            Else
                iReturn = _tcAdsSymbolInfo.ByteSize
            End If

            Return iReturn
        End Get
    End Property

#End Region

#Region "GetData"

    Public ReadOnly Property EventHandler
        Get
            Return _iEventHandler
        End Get
    End Property

    Public ReadOnly Property GetSystemType()
        Get
            Select Case _tcAdsSymbolInfo.DataTypeId
                Case AdsDatatypeId.ADST_BIT
                    Return GetType(Boolean)
                Case AdsDatatypeId.ADST_INT16 'int
                    Return GetType(Int16)
                Case AdsDatatypeId.ADST_INT32 'Dint
                    Return GetType(Int32)
                Case AdsDatatypeId.ADST_INT64 'Lint
                    Return GetType(Int64)
                ' Case AdsDatatypeId.ADST_WSTRING
                ' Return GetType(String) 'Let op: encoding volgens utf 16!!!! dus 2 bytes per character

                'Case AdsDatatypeId.ADST_BIGTYPE 'datatype dat gebruikt wordt voor structs, moet op een andere manier opgevangen worden
                ' Return GetType(Byte())
                Case AdsDatatypeId.ADST_UINT16
                    Return GetType(UInt16)
                Case AdsDatatypeId.ADST_UINT32
                    Return GetType(UInt32)
                Case AdsDatatypeId.ADST_REAL64 'LREAL
                    Return GetType(Double)
                Case AdsDatatypeId.ADST_REAL32 'REAL
                    Return GetType(Single)
                Case AdsDatatypeId.ADST_INT8
                    Return GetType(Byte)
                Case AdsDatatypeId.ADST_STRING
                    Return GetType(String)
                Case Else
                    Return GetType(Object)
            End Select
        End Get
    End Property

    Public ReadOnly Property GetAdsType As AdsDatatypeId
        Get
            Return _tcAdsSymbolInfo.DataTypeId
        End Get
    End Property



    Public ReadOnly Property DataStreamLength() As Integer
        Get
            Return _tcAdsSymbolInfo.ByteSize
        End Get
    End Property

    Public ReadOnly Property SubVars() As List(Of clsPLCvariable)
        Get
            Return _oSubVariables
        End Get
    End Property

    Public Function ToArray() As Object()
        If _oSubVariables Is Nothing OrElse _oSubVariables.Count = 0 Then Return Nothing
        Dim oReturn(_oSubVariables.Count - 1) As Object
        For i As Integer = 0 To _oSubVariables.Count - 1
            oReturn(i) = _oSubVariables(i).CurrentValue
        Next
        Return oReturn
    End Function

    Public Function ToList() As List(Of Object)
        Dim oReturn As New List(Of Object)
        If _oSubVariables Is Nothing OrElse _oSubVariables.Count = 0 Then Return Nothing
        For i As Integer = 0 To _oSubVariables.Count - 1
            oReturn.Add(_oSubVariables(i).CurrentValue)
        Next
        Return oReturn
    End Function

    Public Function GetSubVarByName(sName As String) As clsPLCvariable
        Dim i As Integer = 0
        Dim o As clsPLCvariable = Nothing
        For Each oSubvar As clsPLCvariable In _oSubVariables
            If oSubvar.Name.Split(".").Last.Equals(sName) Then
                o = oSubvar
                i = i + 1
            End If
        Next
        If i = 1 Then
            Return o
        Else
            Return Nothing
        End If
    End Function

#End Region

#Region "Main"

    Public Property CurrentValue As Object
        Set(oValue As Object)
            _oCurrentValue = oValue
        End Set
        Get
            Dim sOutput As String = _oCurrentValue
            If _oCurrentValue Is Nothing Then
                sOutput = "NULL"
            End If

            If _xError Then
                Return "[ERROR]  " + sOutput
            Else
                Return sOutput
            End If
        End Get
    End Property

    Public ReadOnly Property Name As String
        Get
            Return _tcAdsSymbolInfo.Name
        End Get
    End Property

    Public ReadOnly Property VarHandle As Integer
        Get
            Return _iVariableHandle
        End Get
    End Property

    Public ReadOnly Property GetIndexGroup As Long
        Get
            Return _tcAdsSymbolInfo.IndexGroup
        End Get
    End Property
    Public ReadOnly Property IsHandleReady As Boolean
        Get
            Return _xHandleReady
        End Get
    End Property
    Public ReadOnly Property GetIndexOffset As Long
        Get
            Return _tcAdsSymbolInfo.IndexOffset
        End Get
    End Property
#End Region
#End Region

    Public Overrides Function ToString() As String
        Return Name
    End Function


End Class
