﻿Imports TwinCAT.Ads
Imports System.IO

Public Class clsADScommunicatie

    ' Variabelen meegegeven in constructor
    Private _sAMSAddress As String
    Private _iPort As Integer
    Private _xTimer As Boolean
    Private _arrFilterForVar() As String

    'main object
    Private WithEvents _otcClient As TcAdsClient

    'Data objecten
    Private _oParentVars As New List(Of clsPLCvariable)
    Private _oVarsByName As New Dictionary(Of String, clsPLCvariable)
    Private _xOmitSystemVars As Boolean

    'echo (voor debug)
    Private _xEcho As Boolean

    'timer
    Private WithEvents _oTimer As New System.Timers.Timer
    Private _oLockObject As New Object
    Private _xTimerAllowed As Boolean

    'event
    Public Event ChangedVarADSevent(oPLCvar As clsPLCvariable)
    Private _oEventDataStream As New AdsStream
    Public Event UpdatedVarList(oListVar As List(Of clsPLCvariable))
    Public Event CyclusDone()



    ''' <summary>
    ''' Initialize ADS communication class. 
    ''' </summary>
    ''' <param name="iInterval">Timer Interval. If not needed : set -1.</param>
    Public Sub New(Optional iInterval As Integer = 100, Optional xEnableEcho As Boolean = False)

        _xTimer = (-1 = iInterval)
        _oTimer.Interval = iInterval
        _xEcho = xEnableEcho

    End Sub

#Region "Connect/disconnect & startup"

    ''' <summary>
    ''' Connecteren met PLC ; laden van alle variabelen ; een eerste keer alle waarden uitlezen. Eventueel timer aanzetten
    ''' </summary>
    Public Sub Connect(ByVal sAMSaddress As String, ByVal iPort As Integer, Optional xOmitSystemVars As Boolean = False, Optional arrFilterVars() As String = Nothing)
        Try
            _sAMSAddress = sAMSaddress
            _iPort = iPort
            _arrFilterForVar = arrFilterVars
            _otcClient = New TcAdsClient()
            _xOmitSystemVars = xOmitSystemVars
            _otcClient.Connect(_sAMSAddress, _iPort)
            If CheckConnection() Then
                LoadVarList()
                ReadAll()
                If _oTimer.Interval > 0 Then
                    _oTimer.Start()
                    _xTimerAllowed = True
                End If
            Else
                MsgBox("Connection Failed" & vbCrLf & vbCrLf & GetPLCState)
            End If
        Catch ex As Exception
            EchoError("Kan niet connecteren met de PLC via " & _sAMSAddress & "   op port " & _iPort, ex)
        End Try
    End Sub

    ''' <summary>
    ''' Disconnecten met PLC ; alle variablehandlers verwijderen
    ''' </summary>
    Public Sub Disconnect()
        _oTimer.Stop()
        If _otcClient IsNot Nothing Then
            Try
                _xTimerAllowed = False
                SyncLock _oLockObject
                    If _otcClient.IsConnected Then
                        For Each oElement As clsPLCvariable In _oVarsByName.Values
                            _otcClient.DeleteVariableHandle(oElement.VarHandle)
                        Next
                    End If
                End SyncLock
            Catch ex As Exception
                EchoError("Kan niet disconnecten", ex)
            Finally
                _otcClient.Dispose()
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Lijst inladen van alle variabelen in de PLC. Als er gefilterd moet worden, moet dat hier gebeuren
    ''' </summary>
    Private Sub LoadVarList()
        If CheckConnection() Then
            SyncLock _oLockObject
                Try
                    ' downloaden van alle info van de PLC
                    Dim tcPLCInfoLoader As TcAdsSymbolInfoLoader = _otcClient.CreateSymbolInfoLoader()
                    ' filteren van alle info naar een lijst van alle variabelen
                    Dim tcPLCSymbolCollection As TcAdsSymbolInfoCollection = tcPLCInfoLoader.GetSymbols(True)
                    'het aantal variabelen bijhouden
                    Dim iSymbolCount As Integer = tcPLCInfoLoader.GetSymbolCount(False)

                    'lijst van variabelen opbouwen
                    _oVarsByName.Clear()
                    _oParentVars.Clear()

                    ' lijsten opvullen, maar er mogen geen dubbele entries inzitten

                    Dim oTijdelijkSymbol As TcAdsSymbolInfo
                    oTijdelijkSymbol = tcPLCInfoLoader.GetFirstSymbol(True)
                    While (Not oTijdelijkSymbol Is Nothing)
                        'filtering toepassen
                        If ApplyFilter(oTijdelijkSymbol.Name) And (_xOmitSystemVars And oTijdelijkSymbol.IndexGroup <= 61440 Or Not _xOmitSystemVars) Then
                            'controle als de variabele nog niet gekend is of niet gebruikt mag worden
                            If Not _oVarsByName.ContainsKey(oTijdelijkSymbol.Name) And oTijdelijkSymbol.IndexOffset <> 0 Then
                                'opbouwen van parent variabele
                                Dim oVar As clsPLCvariable = New clsPLCvariable(oTijdelijkSymbol, _otcClient)
                                _oParentVars.Add(oVar)
                                'opvragen alle children van de parentvariabele
                                Dim oListPLCvar As List(Of clsPLCvariable) = oVar.CollectAll()
                                For Each o As clsPLCvariable In oListPLCvar
                                    If Not _oVarsByName.ContainsKey(o.Name) Then
                                        _oVarsByName.Add(o.Name, o)
                                    End If
                                Next
                            End If
                        End If
                        oTijdelijkSymbol = oTijdelijkSymbol.NextSymbol
                    End While
                Catch ex As Exception
                    EchoError("Fout bij het opbouwen van de LoadVarList", ex)
                End Try
            End SyncLock
        End If

    End Sub

    Private Function ApplyFilter(sSubSymbolName As String) As Boolean
        Dim xAllGoodToGo As Boolean = True
        If _arrFilterForVar IsNot Nothing Then
            Dim arrTemp() As String = Array.FindAll(Of String)(_arrFilterForVar, Function(s) sSubSymbolName.Contains(s))
            xAllGoodToGo = arrTemp.Count() <> 0
        End If
        Return xAllGoodToGo
    End Function
#End Region

#Region "IO"

#Region "Read"

    ''' <summary>
    ''' Uitlezen van alle variabelen dmv 1 commando.
    ''' </summary>
    Private Sub ReadAll()
        SyncLock _oLockObject
            Try

                ' zie lengte buffers : https://infosys.beckhoff.com/english.php?content=../content/1033/tcsample_net/html/twincat.ads.sample09.htm&id=5699182335088012610
                Dim rdLength As Integer = 0
                Dim oWriter As New AdsBinaryWriter(New AdsStream())
                Dim oTemporaryVarList As New List(Of clsPLCvariable)
                'maken van het data element waarop de plc z'n gegevens zal schrijven
                For Each oElement As clsPLCvariable In _oVarsByName.Values
                    If Not oElement.IsSubItem And oElement.IncludeInReadAll And oElement.IsHandleReady Then
                        ' ik lees enkel items zonder subitem, anders wordt er dubbel uitgelezen
                        oWriter.Write(AdsReservedIndexGroups.SymbolValueByHandle)
                        oWriter.Write(oElement.VarHandle)
                        oWriter.Write(oElement.DataStreamLength)
                        rdLength += oElement.DataStreamLength + 4
                        oTemporaryVarList.Add(oElement)
                    End If
                Next
                If oTemporaryVarList.Count <> 0 Then
                    Dim rdStream As AdsStream = New AdsStream(rdLength)
                    Dim oOmzetting As AdsStream = oWriter.BaseStream

                    'het commando om uit de plc meerdere variabelen in 1 keer op te halen
                    _otcClient.ReadWrite(AdsReservedIndexGroups.SumCommandRead, oTemporaryVarList.Count(), rdStream, oOmzetting)

                    Dim oReader As AdsBinaryReader = New AdsBinaryReader(rdStream)

                    'uitlezen error codes
                    For i As Integer = 0 To oTemporaryVarList.Count() - 1
                        oTemporaryVarList(i).ErrorState = (AdsErrorCode.NoError <> oReader.ReadInt32)
                    Next

                    'uitlezen variabelen
                    For i As Integer = 0 To oTemporaryVarList.Count() - 1
                        Dim bBuffer(oTemporaryVarList(i).DataStreamLength - 1) As Byte
                        oReader.Read(bBuffer, 0, oTemporaryVarList(i).DataStreamLength)
                        ' oReader.Read()
                        oTemporaryVarList(i).DataBytes = bBuffer
                    Next

                    Dim oChangedVarList As List(Of clsPLCvariable) = oTemporaryVarList.Where(Function(x) x.HasChanged).ToList
                    If oChangedVarList.Count <> 0 Then
                        Dim t As New Threading.Thread(Sub() RaiseEvent UpdatedVarList(oChangedVarList))
                        t.Start()
                    End If
                    Dim t2 As New Threading.Thread(Sub() RaiseEvent CyclusDone())
                    t2.Start()

                End If

            Catch ex As Exception
                EchoError("Fout bij het uitlezen van alle variabelen" & _sAMSAddress & "   op port " & _iPort, ex)
            End Try

        End SyncLock

    End Sub

    ''' <summary>
    ''' Uitlezen van één variabele dmv een adsstream
    ''' </summary>
    ''' <param name="sVarnaam"></param>
    ''' <returns></returns>
    Public Function SingleRead(sVarnaam As String) As Object
        Try
            ' plc variabele vinden
            Dim oPLCvar As clsPLCvariable = _oVarsByName(sVarnaam)
            ' buffer waarin data gelezen wordt
            Dim oBinBuffer As New AdsStream(oPLCvar.DataStreamLength)
            ' commando naar plc sturen
            _otcClient.Read(oPLCvar.VarHandle, oBinBuffer)
            ' buffer in een binary reader schrijven
            Dim oReader As AdsBinaryReader = New AdsBinaryReader(oBinBuffer)
            ' binary reader naar byte() converteren en naar de plc variabele doorspelen (daar wordt een conversie gedaan naar een datatype)
            Dim bBuffer(oPLCvar.DataStreamLength) As Byte
            oReader.Read(bBuffer, 0, oPLCvar.DataStreamLength)
            oPLCvar.DataBytes = bBuffer
            Return oPLCvar.CurrentValue
        Catch ex As Exception
            EchoError("Fout bij het uitlezen van één variabele [SingleRead]" & _sAMSAddress & "   op port " & _iPort, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Uitlezen één variabele dmv de variable handle.
    ''' </summary>
    ''' <param name="sVarnaam"></param>
    ''' <returns></returns>
    Public Function SingleReadAny(sVarnaam As String) As Object
        Try

            Dim oPLCvar As clsPLCvariable = _oVarsByName(sVarnaam)
            Dim oValue As Object 'tijdelijk object om de data in te plaatsen
            Dim arguments(0) As Integer
            Dim otype As System.Type = oPLCvar.GetSystemType 'hulpfunctie voor de conversie van ads datatypes naar systeem types
            If oPLCvar.GetAdsType <> AdsDatatypeId.ADST_STRING And oPLCvar.GetAdsType <> AdsDatatypeId.ADST_WSTRING Then
                oValue = _otcClient.ReadAny(oPLCvar.VarHandle, otype)
            Else
                arguments(0) = oPLCvar.DataStreamLength - 1
                oValue = _otcClient.ReadAny(oPLCvar.VarHandle, otype, arguments)
            End If
            oPLCvar.CurrentValue = oValue
            Return oValue
        Catch ex As Exception
            EchoError("Fout bij het uitlezen van één variabele [SingleReadAny]" & _sAMSAddress & "   op port " & _iPort, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Uitlezen één variabele dmv indexgroup en indexoffset. Source : Thibaut 
    ''' </summary>
    ''' <param name="sVarnaam"></param>
    ''' <returns></returns>
    Public Function SingleReadAnyIndex(sVarnaam As String) As Object
        Try
            Dim oPLCvar As clsPLCvariable = _oVarsByName(sVarnaam)

            Dim oValue As Object
            Dim arguments(0) As Integer
            Dim otype As System.Type = oPLCvar.GetSystemType 'hulpfunctie voor de conversie van ads datatypes naar systeem types
            If oPLCvar.GetAdsType <> AdsDatatypeId.ADST_STRING Then
                oValue = _otcClient.ReadAny(oPLCvar.GetIndexGroup, oPLCvar.GetIndexOffset, otype)
            Else
                arguments(0) = oPLCvar.DataStreamLength - 1
                oValue = _otcClient.ReadAny(oPLCvar.GetIndexGroup, oPLCvar.GetIndexOffset, otype, arguments)
            End If
            Return oValue
        Catch ex As Exception
            EchoError("Fout bij het uitlezen van één variabele [SingleReadAnyIndex]" & _sAMSAddress & "   op port " & _iPort, ex)
            Return Nothing
        End Try

    End Function

#End Region

#Region "Write"

    ''' <summary>
    ''' Schrijven van één variabele, dmv adsstream
    ''' </summary>
    ''' <param name="sVarnaam"></param>
    ''' <param name="oValue"></param>
    ''' <returns></returns>
    Public Function SingleWrite(sVarnaam As String, oValue As Object) As Boolean
        Try
            Dim oPLCvar As clsPLCvariable = _oVarsByName(sVarnaam)
            Return SingleWrite(oPLCvar, oValue)
        Catch ex As Exception
            EchoError("Fout bij het schrijven van één variabele [SingleWrite]" & _sAMSAddress & "   op port " & _iPort, ex)
            Return False
        End Try
    End Function


    Public Function SingleWrite(oPLCvar As clsPLCvariable, oValue As Object) As Boolean
        Try
            If oPLCvar Is Nothing Then Return False
            ' werkt voor alle datatypes
            _otcClient.Write(oPLCvar.VarHandle, Convert2Stream(oPLCvar, oValue))
            oPLCvar.CurrentValue = oValue
            Return True
        Catch ex As Exception
            EchoError("Fout bij het schrijven van één variabele [SingleWrite]" & _sAMSAddress & "   op port " & _iPort, ex)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Schrijven van één variabele, dmv variabelehandler
    ''' </summary>
    ''' <param name="sVarnaam"></param>
    ''' <param name="oValue"></param>
    ''' <returns></returns>
    Public Function SingleWriteAny(sVarnaam As String, oValue As Object) As Boolean
        Try
            Dim oPLCvar As clsPLCvariable = _oVarsByName(sVarnaam)
            Dim typePLCvar As System.Type = oPLCvar.GetSystemType
            Dim typeValue As System.Type = oValue.GetType
            If _oVarsByName.ContainsKey(sVarnaam) And typePLCvar.ToString = typeValue.ToString Then
                ' schrijven van de variabele naar de PLC
                Dim arguments(0) As Integer
                Convert2Stream(oPLCvar, oValue)
                If oPLCvar.GetAdsType <> AdsDatatypeId.ADST_STRING Then
                    _otcClient.WriteAny(oPLCvar.VarHandle, oValue)
                Else 'indien het een string is wordt de lengte van de te schrijven string meegegeven
                    Dim tempstring As String = oValue.ToString
                    arguments(0) = tempstring.Length
                    _otcClient.WriteAny(oPLCvar.VarHandle, oValue, arguments)
                End If
                _oVarsByName(sVarnaam).CurrentValue = oValue
                Return True
            End If
            Return False
        Catch ex As Exception
            EchoError("Fout bij het schrijven van één variabele [SingleWriteAny]" & _sAMSAddress & "   op port " & _iPort, ex)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Uitlezen van alle variabelen in één keer. Maakt gebruik van adsstream
    ''' </summary>
    ''' <param name="sListPLCvar"></param>
    ''' <param name="oListvarValue"></param>
    ''' <returns></returns>
    Public Function WriteAll(sListPLCvar As List(Of String), oListvarValue As List(Of Object))

        'opbouwen errorlijst
        Dim oReturn As New List(Of Boolean)
        For i As Integer = 0 To sListPLCvar.Count() - 1
            oReturn.Add(False)
        Next

        SyncLock _oLockObject
            Try
                'zie https://infosys.beckhoff.com/english.php?content=../content/1033/tcsample_net/html/twincat.ads.sample09.htm&id=5699182335088012610

                'lijst met strings omzetten naar lijst met PLCvariabelen
                ' rekening houden dat er incorrecte strings in kunnen zitten
                Dim oListPLCvar As New List(Of clsPLCvariable)
                Dim oListActualValue As New List(Of Object)

                For i As Integer = 0 To sListPLCvar.Count() - 1
                    If _oVarsByName.ContainsKey(sListPLCvar(i)) Then
                        oListPLCvar.Add(_oVarsByName(sListPLCvar(i)))
                        oListActualValue.Add(oListvarValue(i))
                        oListPLCvar.Last.CurrentValue = oListActualValue.Last
                    End If
                Next
                If oListPLCvar.Count <> 0 Then

                    ' binarywriter die de communicatieboodschap opzet (zie de hyperlink hierboven)
                    Dim oWriter As New BinaryWriter(New AdsStream())

                    'schrijven van dataheader
                    For i As Integer = 0 To oListPLCvar.Count() - 1
                        oWriter.Write(AdsReservedIndexGroups.SymbolValueByHandle)
                        oWriter.Write(oListPLCvar(i).VarHandle)
                        oWriter.Write(oListPLCvar(i).DataStreamLength)
                    Next

                    'schrijven van variabelen 
                    For i As Integer = 0 To oListPLCvar.Count() - 1
                        ' variabele naar datastream => naar byte()
                        Dim oMemStream As MemoryStream = Convert2Stream(oListPLCvar(i), oListvarValue(i))
                        ' byte() in datastream
                        oWriter.Write(oMemStream.GetBuffer())
                    Next

                    'antwoord buffer opbouwen
                    Dim rdStream As AdsStream = New AdsStream(4 * oListPLCvar.Count())

                    'converteren van binarystream
                    Dim oCommands As AdsStream = oWriter.BaseStream

                    'het commando waar alles rond draait
                    _otcClient.ReadWrite(AdsReservedIndexGroups.SumCommandWrite, oListPLCvar.Count, rdStream, oCommands)

                    ' foutenbuffer uitlezen
                    Dim oErrorReader As AdsBinaryReader = New AdsBinaryReader(rdStream)

                    For i As Integer = 0 To oReturn.Count() - 1
                        oReturn(i) = (AdsErrorCode.NoError = oErrorReader.ReadInt32)
                    Next

                End If


            Catch ex As Exception
                EchoError("Fout bij het schrijven van meerdere variabelen [WriteAll]" & _sAMSAddress & "   op port " & _iPort, ex)
            End Try

        End SyncLock
        Return oReturn
    End Function

    Public Function WriteAll(sListPLCvar As List(Of clsPLCvariable), oListvarValue As List(Of Object))
        Dim oNewList As New List(Of String)
        For i As Integer = 0 To sListPLCvar.Count - 1
            oNewList.Add(sListPLCvar(i).Name)
        Next
        Return WriteAll(oNewList, oListvarValue)
    End Function


    'TODO : schrijven van een variabele met subvariabelen

#End Region

#Region "Events"

    ''' <summary>
    ''' Toevoegen van variabele-event
    ''' </summary>
    ''' <param name="oPLCvar"></param>
    Public Sub AddVariableEvent(oPLCvar As clsPLCvariable)
        If oPLCvar IsNot Nothing Then

            Dim iHandler As Integer = _otcClient.AddDeviceNotification(oPLCvar.Name, _oEventDataStream, 0, oPLCvar.DataStreamLength, AdsTransMode.OnChange, 100, 0, oPLCvar)
            oPLCvar.CoupleEvent(iHandler)
        End If

    End Sub

    ''' <summary>
    ''' Verwijderen van één variabele-event
    ''' </summary>
    ''' <param name="oPLCvar"></param>
    Public Sub RemoveVarEvent(oPLCvar As clsPLCvariable)
        If oPLCvar IsNot Nothing AndAlso oPLCvar.IsEventCoupled Then
            _otcClient.DeleteDeviceNotification(oPLCvar.RemoveEvent)
        End If

    End Sub

    ''' <summary>
    ''' Opvangen van variabele-event
    ''' ADVIES : geen intensieve functies triggeren met dit event in dezelfde thread.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub OnNotification(ByVal sender As Object, ByVal e As AdsNotificationEventArgs) Handles _otcClient.AdsNotification
        Dim bBuffer(e.UserData.DataStreamLength) As Byte
        Dim oReader As AdsBinaryReader = New AdsBinaryReader(e.DataStream)
        oReader.Read(bBuffer, 0, e.UserData.DataStreamLength)
        e.UserData.DataBytes = bBuffer
        RaiseEvent ChangedVarADSevent(e.UserData)
    End Sub

#End Region

#End Region

#Region "Ondersteunende functies"

    ''' <summary>
    ''' Converteren van een variabele naar een adsstream
    ''' </summary>
    ''' <param name="oPLCvar"></param>
    ''' <param name="oValue"></param>
    ''' <returns></returns>
    Private Function Convert2Stream(oPLCvar As clsPLCvariable, oValue As Object) As AdsStream

        Dim oDatastream As AdsStream
        Dim oWriter As BinaryWriter
        'verschil tussen string (ASCII of UNICODE) en een gewone variabele
        If oPLCvar.GetAdsType = AdsDatatypeId.ADST_STRING Or oPLCvar.GetAdsType = AdsDatatypeId.ADST_WSTRING Then

            oDatastream = New AdsStream(oPLCvar.DataStreamLength)
            If oPLCvar.GetAdsType = AdsDatatypeId.ADST_STRING Then
                oWriter = New BinaryWriter(oDatastream, System.Text.Encoding.ASCII)
            Else
                oWriter = New BinaryWriter(oDatastream, System.Text.Encoding.Unicode)
            End If

            Dim stemp As String = oValue
            oWriter.Write(stemp.ToCharArray())
            oWriter.Write(Chr(0))

            Dim arrByte(oWriter.BaseStream.Length - 1) As Byte
            Dim oTemp As BinaryReader = New BinaryReader(oWriter.BaseStream)
            oTemp.BaseStream.Position = 0
            oTemp.Read(arrByte, 0, oWriter.BaseStream.Length - 1)

            oPLCvar.DataBytes(True) = arrByte
        Else
            'Alles wat hier binnenkomt (toch via de GUI) is een string
            'hier wordt alles omgezet naar het correcte datatype
            Dim oNewVal As Object
            oNewVal = Convert.ChangeType(oValue, oPLCvar.GetSystemType)

            'aanmaken van AdsStream en writer voor die stream
            oDatastream = New AdsStream(oPLCvar.DataStreamLength)
            oWriter = New BinaryWriter(oDatastream)

            'kopieren van bytes in het geheugen die bij de inputvariabele horen naar de array
            Dim sSize As Integer = Runtime.InteropServices.Marshal.SizeOf(oNewVal)
            Dim bBuffer(sSize - 1) As Byte
            Dim ptr As Integer = Runtime.InteropServices.Marshal.AllocHGlobal(sSize)
            Runtime.InteropServices.Marshal.StructureToPtr(oNewVal, ptr, False)
            Runtime.InteropServices.Marshal.Copy(ptr, bBuffer, 0, sSize)
            Runtime.InteropServices.Marshal.FreeHGlobal(ptr)
            'dimensie van buffer verkleinen tot corrcte lengte
            'boolean is bv te groot in vb voor ads, dus nutteloze data achteraan verwijderen
            ' potentieel gevaar dat er hier data verloren gaat, maar normaal niet het geval
            ReDim Preserve bBuffer(oPLCvar.DataStreamLength - 1)
            ' buffer schrijven naar stream
            oPLCvar.DataBytes(True) = bBuffer.Clone
            oWriter.Write(bBuffer)
        End If

        Return oDatastream
    End Function

    ''' <summary>
    ''' Uitlezen van variabelen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub TimerLogic(sender As System.Object, e As EventArgs) Handles _oTimer.Elapsed
        _oTimer.Stop()
        ReadAll()
        _oTimer.Enabled = _xTimerAllowed
    End Sub

#End Region

#Region "Get Data"

    Public ReadOnly Property CheckConnection() As Boolean
        Get
            Try
                If _otcClient IsNot Nothing AndAlso _otcClient.IsConnected AndAlso _otcClient.ReadState.AdsState = AdsState.Run Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        End Get
    End Property

    Public ReadOnly Property GetPLCState As String
        Get
            Dim sOutput As String
            Try
                'status van de plc uitlezen
                sOutput = "Devicestate: " & _otcClient.ReadState.DeviceState.ToString & vbCrLf & "ADSstate: " & _otcClient.ReadState.AdsState.ToString

            Catch ex As Exception
                sOutput = "Geen Verbinding"
            End Try
            Return sOutput
        End Get
    End Property

    Public ReadOnly Property GetAllPLCvar() As List(Of clsPLCvariable)
        Get
            Return _oParentVars
        End Get
    End Property

    Public Function GetVarObject(sName As String) As clsPLCvariable
        If _oVarsByName.ContainsKey(sName) Then
            Return _oVarsByName(sName)
        Else
            Return Nothing
        End If
    End Function

    Public Function ReadValue(ByVal sVarNaam As String) As Object
        If CheckConnection() And _oVarsByName.ContainsKey(sVarNaam) Then
            Return _oVarsByName(sVarNaam).CurrentValue
        Else
            Return Nothing
        End If
    End Function

    Public Function GetAllPLCvarSingleLevel(xAllowItemsWithSubItems As Boolean)
        Dim oReturn As New List(Of clsPLCvariable)
        For Each oPLCvar As clsPLCvariable In _oVarsByName.Values()
            If xAllowItemsWithSubItems And oPLCvar.HasSubItems Then
                oReturn.Add(oPLCvar)
            ElseIf Not oPLCvar.HasSubItems Then
                oReturn.Add(oPLCvar)
            End If
        Next
        Return oReturn
    End Function

#End Region

#Region "Debug"
    Private Sub EchoError(sMessage As String, ex As Exception)
        If _xEcho Then
            Dim sTotalMessage As String = sMessage & vbCrLf & vbCrLf & ex.Message & vbCrLf & vbCrLf & ex.StackTrace

            MsgBox(sTotalMessage, MsgBoxStyle.Critical, "Error")
            '  MessageBox.Show(sTotalMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
        End If
    End Sub
#End Region


End Class
