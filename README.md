# README #

# FlexAs

## Content
* Client 
* Executor
* GigE Recorder
* Masterdata
* HIM Communication


### Client

Open Client by clicking on the shortcut in the root of the github or : 
..\2020-flexas-opstelling\Client\Client\Client\bin\Debug or Release

No prerequisites.

### Executor

Server for clients. Remember to allow incomming TCP/IP connections.


### GigE Recorder

Prerequisites
* Opencv 4.3.0 (installed in : C:\opencv\opencv4.3.0)
* Vimba 4.0.0 (default installation path)
* Qt 5.15.0




